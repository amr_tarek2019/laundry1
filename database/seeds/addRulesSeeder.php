<?php

use Illuminate\Database\Seeder;

class addRulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'show_news',
            'add_news',
            'edit_news',
            'delete_news',
            'show_testimonials',
            'add_testimonials',
            'edit_testimonials',
            'delete_testimonials',
           
        ];
        foreach ($permissions as $permission) {
            \Spatie\Permission\Models\Permission::create([
                'name' => $permission,
                'guard_name' => 'admin'
            ]);
        }
        \App\Admin::first()->givePermissionTo(\Spatie\Permission\Models\Permission::all());
    }
}
