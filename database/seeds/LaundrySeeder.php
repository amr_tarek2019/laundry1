<?php

use Illuminate\Database\Seeder;

class LaundrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\LaundryOwner::class,20)->create();
        factory(\App\Laundry::class,20)->create();
        factory(\App\LaundryServices::class,20)->create();
        factory(\App\LaundryDoc::class,20)->create();
        factory(\App\City::class,20)->create();
    }
}
