<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaundryDocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laundry_docs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('laundry_id');
            $table->foreign('laundry_id')->references('id')->on('laundries')->onDelete('cascade');
            $table->string('doc');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laundry_docs');
    }
}
