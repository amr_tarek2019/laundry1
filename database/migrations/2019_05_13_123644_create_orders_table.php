<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('laundry_id');
            $table->text('services_id');
            $table->text('services_name');
            $table->text('services_type');
            $table->text('services_count');
            $table->text('services_price');
            $table->integer('delegate_id')->nullable()->default(null);
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('order_status')->default(0)->comment('0 new , 1 on way to laundry , 2 laundry received , 3 delegate received , 4 done , 5 cancelled');
            $table->string('reason')->nullable();
            $table->string('city');
            $table->string('delivery');
            $table->string('address');
            $table->string('lat');
            $table->string('lng');
            $table->string('payment_type')->nullable();
            $table->string('is_paid')->default(0);
            $table->string('is_collected')->default(0);
            $table->bollean('order_type');
            $table->text('description');
            $table->softDeletes();
            $table->timestamps();
        });

        DB::update("ALTER TABLE orders AUTO_INCREMENT = 1000;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
