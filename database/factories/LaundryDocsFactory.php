<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\LaundryDoc::class, function (Faker $faker) {
    return [
        'laundry_id' => $faker->randomElement(\App\LaundryOwner::pluck('id')->toArray()),
        'doc' => 'test no file '
    ];
});
