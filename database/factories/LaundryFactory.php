<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(\App\Laundry::class, function (Faker $faker) {
    return [
        'owner_id' => $faker->randomElement(\App\LaundryOwner::pluck('id')->toArray()),
        'section_id' => [1,2],
        'name' => $faker->company,
        'description' => $faker->name,
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
        'address' => $faker->address,
        'has_offers' => $faker->randomElement([0, 1]),
        'phone' => $faker->phoneNumber,
        'email' => $faker->safeEmail,
        'is_active' => $faker->randomElement([0, 1]),
        'expire_date' => \Carbon\Carbon::today()->addDay($faker->numberBetween(1,9)),

    ];
});
