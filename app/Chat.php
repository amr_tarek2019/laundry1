<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
     protected $table='chats';
    protected $fillable = [

        'sender_id',
        'sender_type',
        'target_id',
        'order_id',
    ];

    public function messages()
    {
        return $this->hasMany(ChatMessages::class);
    }
}
