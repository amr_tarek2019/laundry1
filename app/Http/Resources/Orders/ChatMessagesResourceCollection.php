<?php

namespace App\Http\Resources\Orders;

use App\ChatMessages;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ChatMessagesResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function (ChatMessages $chatMessages) {
            return [
                'id'=>$chatMessages->id,
                'sent_from' => $chatMessages->sent_from,
                'content' => $chatMessages->content,
                'image'=> $chatMessages->image,
            ];
        })->toArray();
   
    //  dd( $this->collection->transform(function (ChatMessages $chatMessages) {
    //         return [
    //             'id'=>$chatMessages->id,
    //             'sent_from' => $chatMessages->sent_from,
    //             'content' => $chatMessages->content,
    //             'image'=> $chatMessages->image,
    //         ];
    //     })->sortByDesc('id')->toArray());
    }
}



