<?php

namespace App\Http\Resources\Orders;

use Illuminate\Http\Resources\Json\JsonResource;

class LaundryDetails extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => app()->isLocale('ar') ? $this->name : $this->name_en,
            'img' => $this->img,
            'description' => app()->isLocale('ar') ? $this->description : $this->description_en,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'address' => $this->address,
            'phone' => $this->phone,
            'email' => $this->email,
        ];
    }
}
