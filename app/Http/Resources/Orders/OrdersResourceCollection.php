<?php

namespace App\Http\Resources\Orders;

use App\Http\Resources\Delegate\User\DelegateResource;
use App\Http\Resources\User\UserResource;
use App\Order;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OrdersResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Order $order) use ($request) {
            $result = [
                'id' => $order->id,
                'laundry_id' => $order->laundry_id,
                'delegate_id' => $order->delegate_id,
                'order_status' => $order->order_status,
                'reason' => $order->reason,
                'description' => $order->description,
                'order_type' => $order->order_type,
                'city' => $order->city,
                'delivery' => $order->delivery,
                'address' => $order->address,
                'user' => new UserResource($order->user),
                'laundry' => new LaundryDetails($order->laundry),
                'delegate' => $order->delegate ? new DelegateResource($order->delegate) : '',
                'services' => $order->getOrderServices(),
                'chat' => new ChatResource($order->chat),
                'created_at' => $order->created_at,
                'estimate_time'=>$order->estimate_time,

            ];
            if ($user = getUserByJwt($request)) {
                $result['rate'] = $user->orderRate($order->laundry_id);
            }
            return $result;
        });

        return $this->collection->toArray();
    }
}
