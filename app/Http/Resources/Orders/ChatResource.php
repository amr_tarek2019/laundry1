<?php

namespace App\Http\Resources\Orders;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;

class ChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        //           $image = null;
        // if($this->sender_type == "user"){
        //     $user = User::where('id',$this->sender_id)->first();
        //      $image = $user->img ;
            
        //       return [
        // 'sender_id' => $this->sender_id,
        //  'sender_type' => $this->sender_type,
        //  'target_id' => $this->target_id,
        //  'order_id' => $this->order_id,
        //  'messages' => new ChatMessagesResourceCollection($this->messages),
        //   'image'=>$image,
        // ];
        
        // }else{
        //     $delegate = Delegate::where('id',$this->sender_id)->first();
        //     $image = $delegate->img ;
        //              return [
        // 'sender_id' => $this->sender_id,
        //  'sender_type' => $this->sender_type,
        //  'target_id' => $this->target_id,
        //  'order_id' => $this->order_id,
        //  'messages' => new ChatMessagesResourceCollection($this->messages),
        //  'image'=>$image,
        // ];
        // }
        
        
        
        
        
        
        //new 
        
         $image = null;
         if($this->sender_type == "user"){
            $user = User::where('id',$this->sender_id)->first();
             $image = $user->img ;
         }else{
            $delegate = Delegate::where('id',$this->sender_id)->first();
            $image = $delegate->img ;
         }
         return [
        'sender_id' => $this->sender_id,
         'sender_type' => $this->sender_type,
         'target_id' => $this->target_id,
         'order_id' => $this->order_id,
         'messages' => new ChatMessagesResourceCollection($this->messages),
         'image'=>$image,
        ];
        
        
        // return [
        // 'sender_id' => $this->sender_id,
        //  'sender_type' => $this->sender_type,
        //  'target_id' => $this->target_id,
        //  'order_id' => $this->order_id,
        //  'messages' => new ChatMessagesResourceCollection($this->messages),
        //  'image'=>$this->image,
        // ];
        
    
    }
}
