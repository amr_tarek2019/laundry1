<?php

namespace App\Http\Resources\User;

use App\Address;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AddressResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Address $address) {
            return [
                'id' => $address->id,
                'lat' => $address->lat,
                'lng' => $address->lng,
                'address' => $address->address,
            ];
        });
        return $this->collection->toArray();
    }
}
