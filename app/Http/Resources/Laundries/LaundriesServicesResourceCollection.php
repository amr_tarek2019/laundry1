<?php

namespace App\Http\Resources\Laundries;

use App\LaundryServices;
use Illuminate\Http\Resources\Json\ResourceCollection;

class LaundriesServicesResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (LaundryServices $laundryServices) {
            return [
                'id' => $laundryServices->id,
                'section_id' => $laundryServices->section_id,
                'name' => app()->isLocale('ar') ? $laundryServices->name : $laundryServices->name_en,
                'wash' => $laundryServices->wash,
                'ironing' => $laundryServices->ironing,
                'wash_ironing' => $laundryServices->wash_ironing,
                'img'=>$laundryServices->img
            ];
        });

        return $this->collection->toArray();
    }
}
