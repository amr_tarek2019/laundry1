<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class ApiUserJWT
{

    public function handle($request, Closure $next)
    {
        $token = $request->header('jwt'); // getting the token from the request header
        if (!$token) {
            return response(apiResponse(405, 'please enter jwt')); // checking if the token exists in the header
        } elseif ($user = User::whereJwt($token)->first()) {  // checking if the token is correct
            if ($user->is_active == 0) {
                return response(apiResponse(405, 'تم تعليق الحساب من قبل الادمن')); // checking if the user is active
            } elseif ($user->activated == 0) {
                return response(apiResponse(405, 'الحساب غير مفعل')); // checking if the user didnt verify
            } else
                return $next($request);
        }
        return response(apiResponse(405, 'please enter a valid jwt'));
    }
}
