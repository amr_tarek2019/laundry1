<?php

namespace App\Http\Middleware;

use Closure;

class isActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user('web');
        if ($user) {
            if (!$user->is_active) {
                auth('web')->logout();
                return redirect()->route('site.auth.index')->withErrors('تم تعليق الحساب من قبل الادمن');
            }
        }
        return $next($request);
    }
}
