<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiteLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|max:50|exists:users,phone',
            'password' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'phone.required' => 'برجاء إدخال رقم الهاتف',
            'password.required' => 'برجاء إدخال كلمه المرور',

        ];
    }
}
