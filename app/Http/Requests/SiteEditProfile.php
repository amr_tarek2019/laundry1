<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiteEditProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50',
            'email' => 'required|max:50|unique:users,email,'.auth('web')->id(),
            'phone' => 'required|max:50|unique:users,phone,' . auth('web')->id(),
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'برجاء إدخال الإسم',
            'email.required' => 'برجاء إدخال البريد الإلكترونى',
            'email.unique' => ' البريد الإلكترونى مسجل بالفعل',
            'phone.required' => 'برجاء إدخال رقم الهاتف',
            'phone.unique' => ' رقم الهاتف مسجل بالفعل',

        ];
    }
}
