<?php

namespace App\Http\Controllers\Laundry;

use App\Laundry;
use App\LaundryOwner;
use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class LaundriesController extends Controller
{
    /*   function function __construct()
    {
        
    }()
       {
           $this->middleware('permission:show_laundry_owner');
           $this->middleware('permission:show_laundry', ['only' => ['userDetails']]);
           $this->middleware('permission:edit_laundry', ['only' => ['editLaundry', 'feesReset']]);
           $this->middleware('permission:add_laundry_owner', ['only' => ['add']]);
           $this->middleware('permission:edit_laundry_owner', ['only' => ['edit']]);
           $this->middleware('permission:delete_laundry_owner', ['only' => ['delete']]);
       }*/

    /*  public function index()
      {
          $users = auth('laundry')->user()->toArray();
          return view('laundry.pages.laundries.laundry-owners', compact('users'));
      }*/

    /* public function add(Request $request)
     {
         $this->validate($request, [
             'name' => 'required|max:191',
             'email' => 'required|max:191|unique:laundry_owners,email',
             'phone' => 'required|max:191',
             'password' => 'required|max:191',
         ], [
             'name.required' => 'برجاء إدخال  الإسم',
             'email.required' => 'برجاء إدخال البريد الإلكترونى',
             'email.unique' => ' إدخال البريد الإلكترونى مسجل بالفعل',
             'phone.required' => 'برجاء إدخال الهاتف',
             'password.required' => 'برجاء إدخال كلمه المرور',
         ]);
         $owner = LaundryOwner::create($request->all());
         if ($owner) {
             $owner->defaultLaundry();
         }
         return back()->withSuccess('تم الإضافه بنجاح');
     }*/

    public function edit(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|max:191',
            'email' => 'required|max:191',
            'phone' => 'required|max:191',
        ], [
            'name.required' => 'برجاء إدخال  الإسم',
            'email.required' => 'برجاء إدخال البريد الإلكترونى',
            'phone.required' => 'برجاء إدخال الهاتف',
        ]);
        LaundryOwner::findOrFail($request->id)->update($request->all());
        return back()->withSuccess('تم التعديل بنجاح');
    }

    /*  public function delete($id)
      {
          $user = LaundryOwner::findOrFail($id);
          $destination = public_path('uploads/LaundryOwner');
          delete_file($user, 'img', $destination);
          $user->delete();
          return 1;
      }*/
    public function addService(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:191',
            'wash' => 'required|max:191',
            'img' => 'required',
        ], [
            'name.required' => 'برجاء إدخال  الإسم',
            'img.required' => 'برجاء إختيار صوره الخدمه',
            'wash.required' => 'برجاء إدخال  سعر الغسيل',

        ]);
        Laundry::findOrFail($request->id)->services()->create($request->all());
        return back()->withSuccess('تم الإضافه بنجاح');
    }

    public function editService(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:191',
            'wash' => 'required|max:191',
        ], [
            'name.required' => 'برجاء إدخال  الإسم',
            'wash.required' => 'برجاء إدخال  سعر الغسيل',

        ]);
        LaundryServices::findOrFail($request->id)->update($request->all());
        return back()->withSuccess('تم الإضافه بنجاح');
    }

    public function deleteService($id)
    {
        $user = LaundryServices::findOrFail($id);
        @unlink(public_path('uploads/' . class_basename($user) . '/' . $user->getAttributes()['img']));
        $user->delete();
        return 1;
    }
    public function userDetails($id)
    {
        if (auth('laundry')->id() != $id) {
            return back();
        }
        $user = LaundryOwner::findOrFail($id);
        return view('laundry.pages.laundries.details', ['user' => $user]);
    }

    public function downloadInvoiceDetails(Order $order)
    {

        $invoice = [];
        $invoice['name'] = 'فاتوره رقم # ' . $order->id;
        $invoice['date'] = Carbon::parse($order->created_at)->toDateString();
        $invoice['number'] = $order->id;
        $invoice['delegate_detail'] = $order->delegate_id ? $order->delegate : null;
        $invoice['user_detail'] = $order->user;
        $invoice['items'] = $order->getOrderServices();
        $invoice['servicePrice'] = $order->servicePrice();
        $invoice['total'] = $order->totalPrice();
        $invoice['delivery'] = $order->delivery;
        $invoice['address'] = $order->address;
        $pdf = PDF::loadView('laundry.invoices.default', ['invoice' => $invoice]);
        return $pdf->stream('document.pdf');
    }

    public function editLaundry($id, Request $request)
    {
        Laundry::findOrFail($id)->update($request->all());
        return back()->withSuccess('تم التعديل بنجاح');
    }
    /*
        public function feesReset($id)
        {
            $delegate = LaundryOwner::findOrFail($id);
            $fees = $delegate->laundryFees();
            if ($fees) {
                $delegate->restLaundryFees();
                return back()->withSuccess('تم تصفير المستحقات بنجاح');
            }
            return back()->withErrors('لا يوجد مستحقات');
        }*/
}
