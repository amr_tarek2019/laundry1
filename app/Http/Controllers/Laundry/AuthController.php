<?php

namespace App\Http\Controllers\Laundry;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{


    public function index()
    {
        return view('laundry.pages.auth.index');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ], [
            'email.required' => 'برجاء إدخال البريد الإلكترونى',
            'password.required' => 'برجاء إدخال كلمه المرور'
        ]);
        if (\Auth::guard('laundry')->attempt(['email'=>$request->email,'password'=>$request->password]))

        {
            return redirect()->route('laundry.dashboard.index');

        }
        elseif (\Auth::guard('laundry')->attempt(['name'=>$request->email,'password'=>$request->password]))
        {
            return redirect()->route('laundry.dashboard.index');

        }
        return redirect()->route('laundry.auth.index')->withInput($request->all())->exceptInput($request->password)->withErrors('برجاء التأكد من إسم المستخدم وكلمه المرور');
    }

    public function logout(){
        auth()->guard('laundry')->logout();
        return redirect()->route('laundry.auth.index');
    }
}
