<?php

namespace App\Http\Controllers\Laundry;

use App\Admin;
use App\Events\NewMessageEvent;
use App\Laundry;
use App\LaundryOwner;
use App\Notifications\NewMessageNotification;
use Pusher\Pusher;
use Illuminate\Http\Request;
use BaklySystems\LaravelMessenger\Models\Message;
use BaklySystems\LaravelMessenger\Facades\Messenger;
use BaklySystems\LaravelMessenger\Models\Conversation;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MessageController extends Controller
{

    /**
     * @param $withId
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function laravelMessenger($withId, Request $request)
    {
        if ($request->notification_id) {
            $notification = auth('laundry')->user()->unReadNotifications->find($request->notification_id);
            if ($notification) {
                $notification->markAsRead();
            }
        }
        if (!Admin::find($withId)->hasPermissionTo('chats')) {
            return back()->withException(new NotFoundHttpException());
        }
        Messenger::makeSeen(auth('laundry')->id(), $withId, 'Laundry', 'Admin');
        $withUser = Admin::findOrFail($withId);
        $messages = Messenger::messagesWith(auth('laundry')->id(), $withUser->id, 'Laundry', 'Admin');
//        $threads = Messenger::threads(auth('laundry')->id());
        $admins = Admin::all()->filter(function ($admin) {
            return $admin->hasPermissionTo('chats');
        });
        return view('laundry.messenger.messenger', compact('withUser', 'messages', 'admins'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Pusher\PusherException
     */
    public function store(Request $request)
    {
        $this->validate($request, Message::rules());

        $authId = auth('laundry')->id();
        $withId = $request->withId;
        $conversation = Messenger::getConversation($authId, $withId, 'Laundry', 'Admin');

        if (!$conversation) {
            $conversation = Messenger::newConversation($authId, $withId, 'Laundry', 'Admin');
        }

        $message = Messenger::newMessage($conversation->id, $authId, 'Laundry', $request->message);

        // Pusher
        $pusher = new Pusher(
            config('messenger.pusher.app_key'),
            config('messenger.pusher.app_secret'),
            config('messenger.pusher.app_id'),
            [
                'cluster' => config('messenger.pusher.options.cluster')
            ]
        );
        $pusher->trigger('messenger-channel', 'messenger-event', [
            'message' => $message,
            'senderId' => $authId,
            'withId' => $withId
        ]);
        event(new NewMessageEvent('لديك رساله جديده', auth('laundry')->user()->laundry->name));
        $admins = Admin::all()->filter(function ($admin) {
            return $admin->hasPermissionTo('chats');
        });
        \Notification::send($admins, new NewMessageNotification('لديك رساله جديده', auth('laundry')->user()->laundry->name, auth('laundry')->user()->id));
        return response()->json([
            'success' => true,
            'message' => $message
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function loadThreads(Request $request)
    {
        if ($request->ajax()) {
            $withUser = Admin::findOrFail($request->withId);
            $threads = Messenger::threads(auth('laundry')->id());
            $view = view('laundry.messenger.partials.threads', compact('threads', 'withUser'))->render();
            return response()->json($view, 200);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function moreMessages(Request $request)
    {
        $this->validate($request, ['withId' => 'required|integer']);

        if ($request->ajax()) {
            $messages = Messenger::messagesWith(
                auth('laundry')->id(),
                $request->withId,
                'Laundry',
                'Admin',
                $request->take
            );
            $view = view('laundry.messenger.partials.messages', compact('messages'))->render();

            return response()->json([
                'view' => $view,
                'messagesCount' => $messages->count()
            ], 200);
        }
    }

    /**
     * Make a message seen.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function makeSeen(Request $request)
    {
        Messenger::makeSeen($request->authId, $request->withId, 'Laundry', 'Admin');

        return response()->json(['success' => true], 200);
    }

    /**
     * Delete a message.
     *
     * @param int $id
     * @return Response.
     */
    public function destroy($id)
    {
        $confirm = Messenger::deleteMessage($id, auth('laundry')->id());

        return response()->json(['success' => true], 200);
    }
}
