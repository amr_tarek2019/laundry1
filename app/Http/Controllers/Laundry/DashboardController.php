<?php

namespace App\Http\Controllers\Laundry;

use App\Admin;
use App\Laundry;
use App\Order;
use App\Suggestion;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class DashboardController extends Controller
{
    public function index()
    {
        $order_count = auth('laundry')->user()->orders->count();
        $completed_count = auth('laundry')->user()->orders->where('order_status',4)->count();
        $canceled_count = auth('laundry')->user()->orders->where('order_status',5)->count();
        $orders = auth('laundry')->user()->orders->sortByDesc('id')->take(10);
        $date = new Carbon();
        $date->subWeek();
        $orders_last_7_days = auth('laundry')->user()->orders->where('created_at', '>', $date->toDateTimeString())->count();
        return view('laundry.pages.dashboard.index', compact('order_count','completed_count','canceled_count',
            'orders', 'orders_last_7_days'));
    }

    public function readNotification(Request $request)
    {
        if ($request->type) {
            auth('laundry')->user()->unreadNotifications->where('type', '=', 'App\Notifications\NewMessageNotification')->markAsRead();
            return redirect(route('laundry.dashboard.index'));
        }
        if (!$request->id) {
            $notification = auth('laundry')->user()->unReadNotifications;
        } else {
            $notification = auth('laundry')->user()->unReadNotifications->find($request->id);
        }
        if ($notification) {
            $notification->markAsRead();
        }
        return redirect(route('laundry.laundries.user.details',auth('laundry')->id()));
    }
    public function newNotification()
    {
        $view = view('laundry.layouts.header', laundryNotification())->render(); // rending the view with the new comments
        return ['status' => '1', 'view' => $view];
    }
}
