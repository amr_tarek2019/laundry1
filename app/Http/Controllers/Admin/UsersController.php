<?php

namespace App\Http\Controllers\Admin;

use App\Suggestion;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class  UsersController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:show_users');
        $this->middleware('permission:edit_users', ['only' => [ 'toggleStatus']]);
        $this->middleware('permission:delete_users', ['only' => ['deleteUser','deleteSuggestion']]);
    }

    public function index()
    {
        $users = User::all();
        return view('admin.pages.users.index', ['users' => $users]);
    }

    public function toggleStatus(Request $request)
    {
        $user = User::findOrFail($request->id);
        $user->is_active = $request->status;
        $user->save();
        $check = $user->is_active;
        return response()->json(["status" => "ok", 'check' => $check]);

    }

    public function userDetails($id)
    {
        $user = User::findOrFail($id);
        return view('admin.pages.users.details', ['user' => $user]);
    }

    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        $destination = public_path('uploads/User');
        delete_file($user, 'img', $destination);
        $user->delete();
        return 1;
    }

    public function deleteSuggestion($id)
    {
        Suggestion::destroy($id);
        return 1;
    }
}
