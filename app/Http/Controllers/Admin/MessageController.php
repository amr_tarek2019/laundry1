<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Events\NewMessageEvent;
use App\Laundry;
use App\LaundryOwner;
use App\Notifications\NewMessageNotification;
use Pusher\Pusher;
use Illuminate\Http\Request;
use BaklySystems\LaravelMessenger\Models\Message;
use BaklySystems\LaravelMessenger\Facades\Messenger;
use BaklySystems\LaravelMessenger\Models\Conversation;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:chats');
    }

    /**
     * @param $withId
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function laravelMessenger($withId, Request $request)
    {
        if ($request->notification_id) {
            $notification = auth('admin')->user()->unReadNotifications->find($request->notification_id);
            if ($notification) {
                $notification->markAsRead();
            }
        }

        Messenger::makeSeen(auth()->id(), $withId, 'Admin', 'Laundry');
        $withUser = Laundry::findOrFail($withId);
        $messages = Messenger::messagesWith(auth()->id(), $withUser->id, 'Admin', 'Laundry');
//        $threads = Messenger::threads(auth()->id());
        $laundries = Laundry::all();
        return view('admin.messenger.messenger', compact('withUser', 'messages', 'laundries'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Pusher\PusherException
     */
    public function store(Request $request)
    {
        $this->validate($request, Message::rules());

        $authId = auth()->id();
        $withId = $request->withId;
        $conversation = Messenger::getConversation($authId, $withId, 'Admin', 'Laundry');

        if (!$conversation) {
            $conversation = Messenger::newConversation($authId, $withId, 'Admin', 'Laundry');
        }

        $message = Messenger::newMessage($conversation->id, $authId, 'Admin', $request->message);

        // Pusher
        $pusher = new Pusher(
            config('messenger.pusher.app_key'),
            config('messenger.pusher.app_secret'),
            config('messenger.pusher.app_id'),
            [
                'cluster' => config('messenger.pusher.options.cluster')
            ]
        );
        $pusher->trigger('messenger-channel', 'messenger-event', [
            'message' => $message,
            'senderId' => $authId,
            'withId' => $withId
        ]);
        event(new NewMessageEvent('لديك رساله جديده', auth()->user()->name));
        LaundryOwner::find($withId)->notify(new NewMessageNotification('لديك رساله جديده', auth()->user()->name, auth()->user()->id));
        return response()->json([
            'success' => true,
            'message' => $message
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function loadThreads(Request $request)
    {
        if ($request->ajax()) {
            $withUser = Laundry::findOrFail($request->withId);
            $threads = Messenger::threads(auth()->id());
            $view = view('admin.messenger.partials.threads', compact('threads', 'withUser'))->render();
            return response()->json($view, 200);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function moreMessages(Request $request)
    {
        $this->validate($request, ['withId' => 'required|integer']);

        if ($request->ajax()) {
            $messages = Messenger::messagesWith(
                auth()->id(),
                $request->withId,
                'Admin',
                'Laundry',
                $request->take
            );
            $view = view('admin.messenger.partials.messages', compact('messages'))->render();

            return response()->json([
                'view' => $view,
                'messagesCount' => $messages->count()
            ], 200);
        }
    }

    /**
     * Make a message seen.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function makeSeen(Request $request)
    {
        Messenger::makeSeen($request->authId, $request->withId, 'Admin', 'Laundry');

        return response()->json(['success' => true], 200);
    }

    /**
     * Delete a message.
     *
     * @param int $id
     * @return Response.
     */
    public function destroy($id)
    {
        $confirm = Messenger::deleteMessage($id, auth()->id());

        return response()->json(['success' => true], 200);
    }
}
