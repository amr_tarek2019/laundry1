<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Events\newOrderEvent;
use App\Laundry;
use App\Order;
use App\Suggestion;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $users = User::all()->count();
        $order_count = Order::all()->count();
        $laundries_count = Laundry::all()->count();
        $delegates_count = Laundry::all()->count();
        $suggestions = Suggestion::latest()->take(5)->get();
        $orders = Order::latest()->take(10)->get();
        $date = new Carbon();
        $date->subWeek();
        $users_last_7_days = User::where('created_at', '>', $date->toDateTimeString())->get()->count();
        $orders_last_7_days = Order::where('created_at', '>', $date->toDateTimeString())->get()->count();
        return view('admin.pages.dashboard.index', compact('users', 'order_count', 'laundries_count', 'delegates_count', 'orders', 'users_last_7_days', 'orders_last_7_days', 'suggestions'));
    }

    public function readNotification(Request $request)
    {
        if ($request->type) {
            auth('admin')->user()->unreadNotifications->where('type', '=', 'App\Notifications\NewMessageNotification')->markAsRead();
            return redirect(route('admin.dashboard.index'));
        }
        if (!$request->id) {
            $notification = auth('admin')->user()->unReadNotifications;
        } else {
            $notification = auth('admin')->user()->unReadNotifications->find($request->id);
        }
        if ($notification) {
            $notification->markAsRead();
        }
        return redirect(route('admin.orders.index'));
    }

    public function newNotification()
    {
        $view = view('admin.layouts.header', adminNotification())->render(); // rending the view with the new comments
        return ['status' => '1', 'view' => $view];
    }

}
