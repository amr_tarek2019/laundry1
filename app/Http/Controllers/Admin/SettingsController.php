<?php

namespace App\Http\Controllers\Admin;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:edit_settings');
    }
    public function index()
    {
        $settings = Setting::firstOrFail();
        return view('admin.pages.settings.index', ['settings' => $settings]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'terms' => 'required|max:1500',
            'terms_en' => 'required|max:1500',
            'about' => 'required|max:1500',
            'about_en' => 'required|max:1500',
        ], [
            'terms.required' => 'برجاء إدخال الشروط و الاحكام',
            'about.required' => 'برجاء إدخال عن التطبيق',
        ]);
        Setting::firstOrFail()->update($request->all());
        return back()->with('success', 'تم التعديل بنجاح');
    }
}
