<?php

namespace App\Http\Controllers\Admin;

use App\Delegate;
use App\Suggestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DelegatesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:show_delegates');
        $this->middleware('permission:add_delegates', ['only' => ['addDelegate']]);
        $this->middleware('permission:edit_delegates', ['only' => ['toggleStatus', 'editDelegate','feesReset']]);
        $this->middleware('permission:delete_delegates', ['only' => ['deleteUser', 'deleteSuggestion']]);
    }

    public function index()
    {
        $users = Delegate::all();
        return view('admin.pages.delegates.index', ['users' => $users]);
    }

    public function toggleStatus(Request $request)
    {
        $user = Delegate::findOrFail($request->id);
        $user->is_active = $request->status;
        $user->save();
        $check = $user->is_active;
        return response()->json(["status" => "ok", 'check' => $check]);

    }

    public function userDetails($id)
    {
        $user = Delegate::findOrFail($id);
        return view('admin.pages.delegates.details', ['user' => $user]);
    }

    public function deleteUser($id)
    {
        $user = Delegate::findOrFail($id);
        $destination = public_path('uploads/Delegate');
        delete_file($user, 'img', $destination);
        $user->delete();
        return 1;
    }

    public function deleteSuggestion($id)
    {
        Suggestion::destroy($id);
        return 1;
    }

    public function identityDownload($id)
    {
        $delegate = Delegate::findOrFail($id);
        $file = $delegate->identityDownload();

        if (file_exists($file)) {
            return response()->download($file);
        }
        return back()->withErrors('الملف غير موجود');
    }

    public function addDelegate(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:191',
            'email' => 'required|max:191',
            'phone' => 'required|max:191',
            'password' => 'required|max:191',
            'identity_doc' => 'required',
        ], [
            'name.required' => 'برجاء إدخال  الإسم',
            'email.required' => 'برجاء إدخال البريد الإلكترونى',
            'phone.required' => 'برجاء إدخال الهاتف',
            'password.required' => 'برجاء إدخال كلمه المرور',
            'identity_doc.required' => 'برجاء إدخال إثبات الهويه',
        ]);
        Delegate::create($request->all());
        return back()->withSuccess('تم الإضافه بنجاح');
    }

    public function editDelegate(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|max:191',
            'email' => 'required|max:191',
            'phone' => 'required|max:191',
        ], [
            'name.required' => 'برجاء إدخال  الإسم',
            'email.required' => 'برجاء إدخال البريد الإلكترونى',
            'phone.required' => 'برجاء إدخال الهاتف',
        ]);
        Delegate::findOrFail($request->id)->update($request->all());
        return back()->withSuccess('تم التعديل بنجاح');
    }

    public function feesReset($id)
    {
        $delegate = Delegate::findOrFail($id);
        $fees = $delegate->delegateFees();
        if ($fees) {
            $delegate->restDelegateFees();
            return back()->withSuccess('تم تصفير المستحقات بنجاح');
        }
        return back()->withErrors('لا يوجد مستحقات');
    }
}
