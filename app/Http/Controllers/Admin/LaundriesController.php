<?php

namespace App\Http\Controllers\Admin;

use App\Laundry;
use App\LaundryOwner;
use App\LaundryServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LaundriesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:show_laundry_owner');
        $this->middleware('permission:show_laundry', ['only' => ['userDetails']]);
        $this->middleware('permission:edit_laundry', ['only' => ['editLaundry', 'feesReset', 'laundryToggleStatus']]);
        $this->middleware('permission:add_laundry_owner', ['only' => ['add']]);
        $this->middleware('permission:edit_laundry_owner', ['only' => ['edit', 'toggleStatus']]);
        $this->middleware('permission:delete_laundry_owner', ['only' => ['delete']]);
    }

    public function index()
    {
        $users = LaundryOwner::all();
        return view('admin.pages.laundries.laundry-owners', compact('users'));
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:191',
            'email' => 'required|max:191|unique:laundry_owners,email',
            'phone' => 'required|max:191|unique:laundry_owners,phone',
            'password' => 'required|max:191',
        ], [
            'name.required' => 'برجاء إدخال  الإسم',
            'email.required' => 'برجاء إدخال البريد الإلكترونى',
            'email.unique' => ' إدخال البريد الإلكترونى مسجل بالفعل',
            'phone.required' => 'برجاء إدخال الهاتف',
            'password.required' => 'برجاء إدخال كلمه المرور',
        ]);
        $owner = LaundryOwner::create($request->all());
        if ($owner) {
            $owner->defaultLaundry();
        }
        return back()->withSuccess('تم الإضافه بنجاح');
    }

    public function addService(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:191',
            'wash' => 'required|max:191',
            'img' => 'required',
        ], [
            'name.required' => 'برجاء إدخال  الإسم',
            'wash.required' => 'برجاء إدخال  سعر الغسيل',
            'img.required' => 'برجاء إختيار صوره الخدمه',
        ]);
        Laundry::findOrFail($request->id)->services()->create($request->all());
        return back()->withSuccess('تم الإضافه بنجاح');
    }

    public function editService(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:191',
            'wash' => 'required|max:191',
        ], [
            'name.required' => 'برجاء إدخال  الإسم',
            'wash.required' => 'برجاء إدخال  سعر الغسيل',
        ]);
        LaundryServices::findOrFail($request->id)->update($request->all());
        return back()->withSuccess('تم الإضافه بنجاح');
    }

    public function deleteService($id)
    {
        $user = LaundryServices::findOrFail($id);
        @unlink(public_path('uploads/' . class_basename($user) . '/' . $user->getAttributes()['img']));
        $user->delete();
        return 1;
    }

    public function edit(Request $request)
    {
        $checker = LaundryOwner::findOrFail($request->id);

        $this->validate($request, [
            'name' => 'required|max:191',
            'email' => 'required|max:191|unique:laundry_owners,email,' . $checker->id,
            'phone' => 'required|max:191|unique:laundry_owners,phone,' . $checker->id,
        ], [
            'name.required' => 'برجاء إدخال  الإسم',
            'email.required' => 'برجاء إدخال البريد الإلكترونى',
            'phone.required' => 'برجاء إدخال الهاتف',
        ]);
        $checker->update($request->all());
        return back()->withSuccess('تم التعديل بنجاح');
    }

    public function delete($id)
    {
        $user = LaundryOwner::findOrFail($id);
        $destination = public_path('uploads/LaundryOwner');
        delete_file($user, 'img', $destination);
        $user->laundry()->delete();
        $user->delete();
        return 1;
    }

    public function toggleStatus(Request $request)
    {
        $user = LaundryOwner::findOrFail($request->id);
        $user->is_active = $request->status;
        $user->save();
        $check = $user->is_active;
        return response()->json(["status" => "ok", 'check' => $check]);
    }

    public function laundryToggleStatus(Request $request)
    {
        $user = Laundry::findOrFail($request->id);
        $user->is_active = $request->status;
        $user->save();
        $check = $user->is_active;
        return response()->json(["status" => "ok", 'check' => $check]);
    }

    public function userDetails($id)
    {
        $user = LaundryOwner::findOrFail($id);
        return view('admin.pages.laundries.details', ['user' => $user]);
    }

    public function editLaundry($id, Request $request)
    {
        Laundry::findOrFail($id)->update($request->all());
        return back()->withSuccess('تم التعديل بنجاح');
    }

    public function feesReset($id)
    {
        $delegate = LaundryOwner::findOrFail($id);
        $fees = $delegate->laundryFees();
        if ($fees) {
            $delegate->restLaundryFees();
            return back()->withSuccess('تم تصفير المستحقات بنجاح');
        }
        return back()->withErrors('لا يوجد مستحقات');
    }
}
