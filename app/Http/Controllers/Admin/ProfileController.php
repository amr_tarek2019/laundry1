<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index()
    {
        $admin = auth()->guard('admin')->user();
        return view('admin.pages.profile.index', ['admin' => $admin]);
    }

    public function updateInfo(Request $request)
    {
        $checker = auth()->guard('admin')->user();
        $input = $request->all();
        $this->validate($request, [
            'name' => 'required|unique:admins,name,' . $checker->id,
            'email' => 'required|unique:admins,email,' . $checker->id,
            'phone' => 'required|unique:admins,phone,' . $checker->id,
        ],
            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => 'الإسم مسجل بالفعل',
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
                'email.unique' => 'البريد الإلكترونى مسجل بالفعل',
                'phone.required' => 'برجاء إدخال رقم الهاتف',
                'phone.unique' => 'رقم الهاتف مسجل بالفعل',

            ]);
        if ($file = $request->file('img')) {
            $destination = public_path('uploads/profile');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        if ($checker->update($input)) {
            return redirect()->back()->with('success', 'تم تعديل البيانات بنجاح');
        }
        return back()->withSuccess('تم التعديل بنجاح');
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'new_password' => 'required|min:6|max:15',
            'new_password_confirmation' => 'required|same:new_password',
        ],
            [
                'old_password.required' => 'برجاء إدخال كلمه المرور القديمه',
                'new_password.unique' => 'برجاء إدخال كلمه المرور الجديده',
                'new_password.min' => ' كلمه المرور الجديده لا تقل عن 6 حروف',
                'new_password.max' => ' كلمه المرور الجديده لا تزيد عن 15 حروف',
                'new_password_confirmation.unique' => 'برجاء إدخال تأكيد كلمه المرور الجديده',
                'new_password_confirmation.same' => 'تأكيد كلمه المرور الجديده لا تتطابق',
            ]);
        $user = auth()->guard('admin')->user();
        $input = $request->all();
        $input['new_password'] = bcrypt($request->new_password);
        if (Hash::check($request->old_password, $user->password)) {
            $user->update(['password' => $input['new_password']]);
            return redirect()->back()->with('success', 'تم تحديث كلمه المرور بنجاح');
        }
        return redirect()->back()->withErrors('كلمه المرور القديمه غير صحيحه');
    }
}
