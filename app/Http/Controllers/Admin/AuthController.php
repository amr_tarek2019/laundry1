<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{


    public function index()
    {
        return view('admin.pages.auth.index');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ], [
            'email.required' => 'برجاء إدخال البريد الإلكترونى',
            'password.required' => 'برجاء إدخال كلمه المرور'
        ]);
        if (\Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password]))

        {
            return redirect()->route('admin.dashboard.index');

        }
        elseif (\Auth::guard('admin')->attempt(['name'=>$request->email,'password'=>$request->password]))
        {
            return redirect()->route('admin.dashboard.index');

        }
        return redirect()->route('admin.auth.index')->withInput($request->all())->exceptInput($request->password)->withErrors('برجاء التأكد من إسم المستخدم وكلمه المرور');
    }

    public function logout(){
        auth()->guard('admin')->logout();
        return redirect()->route('admin.auth.index');
    }
}
