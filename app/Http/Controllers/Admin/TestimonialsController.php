<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Testimonial;

class TestimonialsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:show_testimonials');
        $this->middleware('permission:add_testimonials', ['only' => ['add']]);
        $this->middleware('permission:edit_testimonials', ['only' => ['edit']]);
        $this->middleware('permission:delete_testimonials', ['only' => ['delete']]);
    }
    public function index()
    {
        $rows = Testimonial::all();
        return view('admin.pages.testimonials.index', ['rows' => $rows]);
    }

    public function delete($id)
    {
        $row = Testimonial::findOrFail($id);
        $destination = public_path('uploads/Testimonial');
        delete_file($row, 'img', $destination);
        $row->delete();
        return 1;
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:191',
            'img' => 'required|max:191',
            'description' => 'required',
        ]);
        Testimonial::create($request->all());
        return back()->withSuccess('تم الإضافه بنجاح');
    }

    public function edit(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|max:191',
            'description' => 'required',
        ]);
        Testimonial::findOrFail($request->id)->update($request->all());
        return back()->withSuccess('تم التعديل بنجاح');
    }
}
