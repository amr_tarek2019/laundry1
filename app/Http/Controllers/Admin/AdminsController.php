<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AdminsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:show_admins');
        $this->middleware('permission:add_admins', ['only' => ['add','store']]);
        $this->middleware('permission:edit_admins', ['only' => ['edit','togglePermissions','adminDetails']]);
        $this->middleware('permission:delete_admins', ['only' => ['delete']]);
    }
    public function index()
    {
        $admins = Admin::where('id', '!=', auth()->guard('admin')->user()->id)->where('id', '!=', 1)->get();
        return view('admin.pages.admins.index', compact('admins'));
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:admins,email',
            'phone' => 'required|unique:admins,phone',
            'password' => 'required',
        ], [
            'name.required' => 'برجاء إدخال الإسم',
            'email.required' => 'برجاء إدخال البريد الإلكترونى',
            'email.unique' => ' البريد الإلكترونى مسجل بالفعل',
            'phone.required' => 'برجاء إدخال رقم الهاتف',
            'phone.unique' => ' رقم الهاتف مسجل بالفعل',
            'password.required' => 'برجاء إدخال كلمه المرور',
        ]);
        Admin::create($request->all());
        return back()->with('success', 'تم الإضافه بنجاح');
    }

    public function edit(Request $request)
    {
//        dd($request->all());
        $checker = Admin::findOrFail($request->id);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:admins,email,' . $checker->id,
            'phone' => 'required|unique:admins,phone,' . $checker->id,

        ], [
            'name.required' => 'برجاء إدخال الإسم',
            'email.required' => 'برجاء إدخال البريد الإلكترونى',
            'email.unique' => ' البريد الإلكترونى مسجل بالفعل',
            'phone.required' => 'برجاء إدخال رقم الهاتف',
            'phone.unique' => ' رقم الهاتف مسجل بالفعل',
        ]);
        $checker->update($request->all());
        return back()->with('success', 'تم التعديل بنجاح');

    }

    public function delete($id)
    {
        $admin = Admin::findOrFail($id);
        $admin->delete();
        return 1;
    }

    public function adminDetails($id)
    {
        $admin = Admin::findOrFail($id);
        return view('admin.pages.admins.permissions', ['admin' => $admin]);
    }

    public function togglePermissions(Request $request)
    {
        $admin = Admin::findOrFail($request->id);
        if ($admin->hasPermissionTo($request->permission)) {
            $admin->revokePermissionTo($request->permission);
        } else {
            $admin->givePermissionTo($request->permission);
        }
        return 1;
    }

}
