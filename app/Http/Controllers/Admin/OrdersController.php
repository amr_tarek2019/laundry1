<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use PDFAnony\TCPDF\Facades\AnonyPDF;

class OrdersController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:show_orders');
//        $this->middleware('permission:edit_orders', ['only' => ['acceptOrder','cashBackResponse']]);
    }

    public function index()
    {
        $orders = Order::all();
        foreach ($orders as $order) {
            $order['start'] = Carbon::parse($order->created_at)->format('Y-m-d');
            $order['end'] = Carbon::parse($order->duration)->format('Y-m-d');
        }

        return view('admin.pages.orders.index', ['orders' => $orders]);
    }

/*    public function acceptOrder(Order $order)
    {
        $order->update(['request_status' => 1]);
        $order->user->notify(new  acceptOrder($order->transaction_code, ' تم الموافقه على الطلب رقم ' . $order->transaction_code . ' من قبل الأدمن'));
        fcm()
            ->to([$order->user->notification_token])// $recipients must an array
            ->data([
                'title' => 'تم تفعيل الطلب',
                'body' => ' تم الموافقه على الطلب رقم ' . $order->transaction_code . ' من قبل الأدمن',
                'type' => 0
            ])
            ->notification([
                'title' => 'تم تفعيل الطلب',
                'body' => ' تم الموافقه على الطلب رقم ' . $order->transaction_code . ' من قبل الأدمن',
                'type' => 0
            ])
            ->send();
        return back()->with('success', 'تم بنجاح');
    }

    public function cashBackDetails($order)
    {
        $request = Order::findOrFail($order)->cashBack;
        return view('admin.pages.orders.cash-back', compact('request'));
    }

    public function cashBackResponse(Request $request)
    {
        $this->validate($request, [
            'admin_comment' => 'required_if:admin_approval,0',
        ], [
            'admin_comment.required_if' => 'برجاء إدخال السبب',
        ]);
        $cashBack = CashBack::findOrFail($request->id);
        $cashBack->update([
            'admin_approval' => $request->admin_approval,
            'admin_comment' => $request->admin_comment
        ]);
        if ($request->admin_approval == 1) {
            $cashBack->order->update(['cash_back' => 1]);
            $cashBack->order->user->notify(new \App\Notifications\cashBack($cashBack->order->transaction_code, ' تم قبول طلب استرجاع المبلغ للطلب رقم  ' . $cashBack->order->transaction_code . ' من قبل الأدمن '));
            $users = [];
            array_push($users, $cashBack->order->user->notification_token);
            if ($seller = User::where('phone', $cashBack->order->seller_phone)->first()) {
                array_push($users, $seller->notification_token);
            }
            fcm()
                ->to($users)// $recipients must an array
                ->data([
                    'title' => 'طلب إسترجاع',
                    'body' => ' تم قبول طلب استرجاع المبلغ للطلب رقم  ' . $cashBack->order->transaction_code . ' من قبل الأدمن ',
                    'type' => 1,
                    'order_id' => $cashBack->order->id
                ])
                ->notification([
                    'title' => 'طلب إسترجاع',
                    'body' => ' تم قبول طلب استرجاع المبلغ للطلب رقم  ' . $cashBack->order->transaction_code . ' من قبل الأدمن ',
                    'type' => 1,
                    'order_id' => $cashBack->order->id
                ])
                ->send();
        } else {
            $cashBack->order->user->notify(new \App\Notifications\cashBack($cashBack->order->transaction_code, ' تم رفض طلب استرجاع المبلغ للطلب رقم  ' . $cashBack->order->transaction_code . ' من قبل الأدمن '));
            $users = [];
            array_push($users, $cashBack->order->user->notification_token);
            if ($seller = User::where('phone', $cashBack->order->seller_phone)->first()) {
                array_push($users, $seller->notification_token);
            }
            fcm()
                ->to($users)// $recipients must an array
                ->data([
                    'title' => 'طلب إسترجاع',
                    'body' => ' تم رفض طلب استرجاع المبلغ للطلب رقم  ' . $cashBack->order->transaction_code . ' من قبل الأدمن ',
                    'type' => 1,
                    'order_id' => $cashBack->order->id
                ])
                ->notification([
                    'title' => 'طلب إسترجاع',
                    'body' => ' تم رفض طلب استرجاع المبلغ للطلب رقم  ' . $cashBack->order->transaction_code . ' من قبل الأدمن ',
                    'type' => 1,
                    'order_id' => $cashBack->order->id
                ])
                ->send();
        }
        return back()->with('success', 'تم بنجاح');
    }*/

    public function downloadInvoiceDetails(Order $order)
    {

        $invoice = [] ;
        $invoice['name'] = 'فاتوره رقم # ' . $order->id;
        $invoice['date'] = Carbon::parse($order->created_at)->toDateString();
        $invoice['number'] = $order->id;
        $invoice['delegate_detail'] = $order->delegate_id ? $order->delegate : null;
        $invoice['user_detail'] = $order->user;
        $invoice['items'] = $order->getOrderServices();
        $invoice['servicePrice'] = $order->servicePrice();
        $invoice['total'] = $order->totalPrice();
        $invoice['delivery'] = $order->delivery;
        $invoice['address'] = $order->address;
        $pdf = PDF::loadView('admin.invoices.default', ['invoice'=>$invoice]);
        return $pdf->stream('document.pdf');
    }
}
