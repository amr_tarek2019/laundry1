<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Laundry;
use Illuminate\Support\Carbon;

class LaundryController extends Controller
{
    public function index()
    {
        $laundries = Laundry::where('is_active', 1)->where('expire_date', '>=', Carbon::today())->get();
        return view('site.pages.laundry',compact('laundries'));
    }
    public function details(Laundry $laundry)
    {
       return view('site.pages.laundry-details', compact('laundry'));
    }
}
