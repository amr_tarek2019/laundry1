<?php

namespace App\Http\Controllers\Backend\Authentication;

use App\Admin;
use App\Laundry;
use App\LaundryOwner;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
class AuthenticationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ], [
            'email.required' => 'برجاء إدخال البريد الإلكترونى',
            'password.required' => 'برجاء إدخال كلمه المرور'
        ]);
        if (\Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password]))

        {
            return redirect()->route('dashboard');

        }
        elseif (\Auth::guard('admin')->attempt(['name'=>$request->email,'password'=>$request->password]))
        {
            return redirect()->route('dashboard');

        }
        return redirect()->route('login')->withInput($request->all())->exceptInput($request->password)->withErrors('برجاء التأكد من إسم المستخدم وكلمه المرور');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
