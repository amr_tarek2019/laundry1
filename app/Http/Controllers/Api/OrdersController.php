<?php

namespace App\Http\Controllers\Api;

use App\Admin;
use App\Delegate;
use App\Events\newOrderEvent;
use App\Http\Resources\Orders\OrdersResourceCollection;
use App\LaundryServices;
use App\Notifications\newOrderNotification;
use App\Order;
use App\Rate;
use App\traits\PushNotificationTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Laundry;

class OrdersController extends Controller
{
    use PushNotificationTrait;

    public function index(Request $request)
    {
        $orders = getUserByJwt($request)->orders->when(Order::find($request->order_id), function ($order) use ($request) {
            return $order->where('id', $request->order_id);
        });
        return apiResponse(200, 'الطلبات', new OrdersResourceCollection($orders), true, $request->per_page);
    }

    public function store(Request $request)
    {

        $v = validator($request->all(), [
            'services_id' => 'required',
            'laundry_id' => 'required|exists:laundries,id',
            'services_name' => 'required',
            'services_type' => 'required',
            'services_count' => 'required',
            'services_price' => 'required',
            'city' => 'required',
            'payment_type' => 'required',
            'delivery' => 'required',
            'address' => 'required',
            'lng' => 'required',
            'lat' => 'required',
            'estimate_time'=>'required',
        ], [
            'services_id.required' => 'برجاء إختيار الخدمات',
            'payment_type.required' => 'برجاء إختيار وسيله الدفع',
            'laundry_id.required' => 'برجاء إدخال رقم المغسله',
            'services_name.required' => 'برجاء أسماء الخدمات',
            'services_type.required' => 'برجاء إختيار أنواع الخدمات',
            'services_count.required' => 'برجاء إختيار عدد الخدمات',
            'services_price.required' => 'برجاء إدخال أسعار المنتجات',
            'city.required' => 'برجاء إدخال المدينه',
            'delivery.required' => 'برجاء إدخال سعر الشحن',
            'address.required' => 'برجاء إدخال العنوان',
            'lat.required' => ' برجاء إدخال خط الطول',
            'lng.required' => ' برجاء إدخال خط العرض',
            'estimate_time.required'=>'برجاء ادخال وقت التسليم',
        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $services = explode(',', $request->services_id);
        foreach ($services as $service) {
            if (!LaundryServices::find($service)) {
                return apiResponse(404, 'الخدمه رقم ' . $service . ' غير موجوده');
            }
            continue;
        }
        $services_types = explode(',', $request->services_type);
        $services_names = explode(',', $request->services_name);
        $services_counts = explode(',', $request->services_count);
        $services_price = explode(',', $request->services_price);
        $user = getUserByJwt($request);
        $order = Order::create([
            'services_id' => $services,
            'services_type' => $services_types,
            'services_name' => LaundryServices::find($services)->pluck('name')->toArray(),
            'services_name_en' => LaundryServices::find($services)->pluck('name_en')->toArray(),
            'services_count' => $services_counts,
            'services_price' => $services_price,
            'payment_type' => $request->payment_type,
            'user_id' => $user->id,
            'city' => $request->city,
            'laundry_id' => $request->laundry_id,
            'delivery' => $request->delivery,
            'address' => $request->address,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'description'=>0,
            'estimate_time'=> $request->estimate_time,
        ]);
        if ($order) {
            $admins = Admin::all()->filter(function ($admin) {
                return $admin->hasPermissionTo('show_orders');
            });
            $delegates = Delegate::whereNotNull('notification_token')->get();
            \Notification::send($admins, new newOrderNotification('طلب جديد', $order->id));

            $order->laundry->owner->notify(new newOrderNotification('طلب جديد', $order->id)) ;

            \Notification::send($delegates, new newOrderNotification('طلب جديد', $order->id));

            $this->PushNotification('طلب جديد', 'يوجد طلب جديد', $delegates->pluck('notification_token')->toArray(), 'new_order', $order->id);
            event(new newOrderEvent('طلب جديد', $order->id));
            return apiResponse(200, app()->isLocale('ar') ? 'تم تسجيل الطلب' : 'order created successfully');
        }
        return apiResponse(401, 'حدث خطأ , برجاء المحاوله ثانيه');
    }

    public function update(Request $request)
    {
        $v = validator($request->all(), [
            'reason' => 'required',

        ], [
            'reason.required' => 'برجاء إدخال سبب الإلغاء',

        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $order = Order::find($request->order_id);
        if ($order) {
            if (getUserByJwt($request)->ownOrder($request->order_id)) {
                if ($order->order_status == 0) {
                    $order->update([
                        'order_status' => 6,
                        'reason' => $request->reason
                    ]);
                    return apiResponse(200, 'تم إلغاء الطلب بنجاح');
                } else {
                    return apiResponse(405, 'لا يمكن إلغاء الطلب بعد قبوله من المندوب');
                }
            } else {
                return apiResponse(405, 'المستخدم لا يملك الطلب');
            }
        } else {
            return apiResponse(404, 'الطلب غير موجود');
        }
    }

    public function myOrders(Request $request)
    {
        if ($request->order_id) {
            $order = Order::where('id', $request->order_id)->get();
            if ($order->count()) {
                return apiResponse(200, 'تفاصيل الطلب', new OrdersResourceCollection($order), true);
            }
            return apiResponse(404, 'الطلب غير موجود');
        }
        return apiResponse(200, 'طلباتى', new OrdersResourceCollection(
            getUserByJwt($request)->orders()->orderBy('id', 'desc')->get()
        ), true);
    }

    public function rate(Request $request)
    {
        $v = validator($request->all(), [
            'order_id' => 'required',
            'rate' => 'required'

        ], [
            'order_id.required' => 'برجاء إدخال رقم الطلب',
            'rate.required' => 'برجاء إدخال التقييم',
        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $order = Order::find($request->order_id);
        if (!$order) {
            return apiResponse(404, 'الطلب غير موجود');
        }
        if ($order->order_status != 4) {
            return apiResponse(405, 'لا يمكن التقييم قبل انتهاء الطلب');
        }
        $user = getUserByJwt($request);
        if ($user->orderRate($order->id) >= 0) {
            return apiResponse(405, 'تم التقييم من قبل');
        } else {
            Rate::create([
                'user_id' => $user->id,
                'laundry_id' => $order->laundry_id,
                'order_id' => $order->id,
                'rate' => $request->rate
            ]);
            return apiResponse(200, 'تم التقييم بنجاح');
        }
    }
    
    public function fastOrder(Request $request)
    {
            $v = validator($request->all(), [
            'laundry_id' => 'required|exists:laundries,id',
            'city' => 'required',
            'address' => 'required',
            'lng' => 'required',
            'lat' => 'required',
            'description'=>'required',
            'estimate_time'=>'required',
        ], [
            'laundry_id.required' => 'برجاء إدخال رقم المغسله',
            'city.required' => 'برجاء إدخال المدينه',
            'address.required' => 'برجاء إدخال العنوان',
            'lat.required' => ' برجاء إدخال خط الطول',
            'lng.required' => ' برجاء إدخال خط العرض',
            'description.required'=>'برجاء إدخال وصف الطلب',
            'estimate_time.required'=>'برجاء ادخال وقت التسليم'
        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }

        $user = getUserByJwt($request);
        $order = Order::create([
            'payment_type' => 'cash',
            'user_id' => $user->id,
            'city' => $request->city,
            'laundry_id' => $request->laundry_id,
            'address' => $request->address,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'description'=>$request->description,
            'order_type'=>'1',
            'services_id'=>'0',
            'services_type' =>0,
             'services_name' => 0,
            'services_count' => 0,
              'services_price' => 0,
            'payment_type' => 0,
            'delivery'=>'0',
            'estimate_time'=>$request->estimate_time,
    
        ]);
        if ($order) {
            
            $admins = Admin::all()->filter(function ($admin) {
                return $admin->hasPermissionTo('show_orders');
            });
            $delegates = Delegate::whereNotNull('notification_token')->get();
            \Notification::send($admins, new newOrderNotification('طلب جديد', $order->id));

            //$order->laundry->owner->notify(new newOrderNotification('طلب جديد', $order->id));

            \Notification::send($delegates, new newOrderNotification('طلب جديد', $order->id));

            $this->PushNotification('طلب جديد', 'يوجد طلب جديد', $delegates->pluck('notification_token')->toArray(), 'new_order', $order->id);
            event(new newOrderEvent('طلب جديد', $order->id));
            return apiResponse(200, app()->isLocale('ar') ? 'تم تسجيل الطلب' : 'order created successfully');
        }
      if ($order) {
            $admins = Admin::all()->filter(function ($admin) {
                return $admin->hasPermissionTo('show_orders');
            });
            $delegates = Delegate::whereNotNull('notification_token')->get();
            \Notification::send($admins, new newOrderNotification('طلب جديد', $order->id));

            $order->laundry->owner->notify(new newOrderNotification('طلب جديد', $order->id));

            \Notification::send($delegates, new newOrderNotification('طلب جديد', $order->id));

            $this->PushNotification('طلب جديد', 'يوجد طلب جديد', $delegates->pluck('notification_token')->toArray(), 'new_order', $order->id);
            event(new newOrderEvent('طلب جديد', $order->id));
            return apiResponse(200, app()->isLocale('ar') ? 'تم تسجيل الطلب' : 'order created successfully');
        }
        return apiResponse(401, 'حدث خطأ , برجاء المحاوله ثانيه');
        }
        
        public function fastOrderDetails(Request $request)
        {
              $orders = getUserByJwt($request)->orders->when(Order::find($request->order_id), function ($order) use ($request) {
            return $order->where('id', $request->order_id)->where('order_type','1');
        });
        return apiResponse(200, 'الطلبات', new OrdersResourceCollection($orders), true, $request->per_page);
        }
       
        
    

    
}
