<?php

namespace App\Http\Controllers\Api\Auth;


use App\Http\Resources\User\UserResource;
use App\User;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        $v = validator($request->all(), [
            'name' => 'required|max:50',
            'email' => 'required|max:50|unique:users,email',
            'password' => 'required_without:social_token|max:50',
            'phone' => 'required|max:50|unique:users,phone',
            'lat' => 'required|max:50',
            'lng' => 'required|max:50',
            'address' => 'required',
            'notification_token' => 'required',
            'device_id' => 'required',
        ], [
            'name.required' => 'برجاء إدخال الإسم',
            'email.required' => 'برجاء إدخال البريد الإلكترونى',
            'email.unique' => ' البريد الإلكترونى مسجل بالفعل',
            'password.required' => ' برجاء إدخال كلمه المرور',
            'phone.required' => 'برجاء إدخال رقم الهاتف',
            'phone.unique' => ' رقم الهاتف مسجل بالفعل',
            'lat.required' => ' برجاء إدخال خط الطول',
            'lng.required' => ' برجاء إدخال خط العرض',
            'address.required' => ' برجاء إدخال العنوان',

        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $input = $request->all();
        $input['jwt'] = jwtGenerator();
        $user = User::create($input);
        if ($user) {
            $user->addresses()->create([
                'lat' => $request->lat,
                'lng' => $request->lng,
                'address' => $request->address,
                'activated'=>'1'
            ]);
        }
            return apiResponse(200, 'تم التسجيل  بنجاح', new UserResource($user));

    }

    public function codeConfirmation(Request $request)
    {
        $v = validator($request->all(), [
            'phone' => 'required|max:50',
            'code' => 'required',
        ], [
            'phone.required' => 'برجاء إدخال رقم الهاتف',
            'code.required' => 'برجاء إدخال كود التفعيل'
        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $user = User::wherePhone($request->phone)->first();
        if ($user) {
            if ($user->code->code === $request->code) {
                $user->code->update(['code' => generateRandomCode(4, $user->id)]);
                $user->update(['activated' => 1]);
                return apiResponse(200, 'تم التفعيل بنجاح', new UserResource($user));
            } else {
                return apiResponse(401, 'كود غير صحيح');
            }
        } else {
            return apiResponse(404, 'برجاء التأكد من رقم الهاتف');
        }
    }

    public function resetPassword(Request $request)
    {
        $v = validator($request->all(), [
            'phone' => 'required|max:50',
        ], [
            'phone.required' => 'برجاء إدخال رقم الهاتف',
        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $user = User::wherePhone($request->phone)->first();
        if ($user) {
            $code = generateRandomCode(4, $user->id);
            $message = "your confirmation code is $code";
            $client = new Client(['base_uri' => 'https://www.hisms.ws/']);
            $client->request('GET', "api.php?send_sms&username=966564444947&password=Aa@123456&numbers=966$user->phone&sender=laundry Sta&message= $message");
            $user->code->update(['code' => $code]);
            return apiResponse(200, 'تم إرسال الكود بنجاح', (object) array());
        } else {
            return apiResponse(404, 'برجاء التأكد من رقم الهاتف', (object) array());
        }
    }

    public function changePassword(Request $request)
    {
        $v = validator(
            $request->all(),
            [
                'phone' => 'required|max:50',
                'new_password' => 'required|min:6|max:15',
                'new_password_confirmation' => 'required|same:new_password',
            ],
            [
                'phone.required' => 'برجاء إدخال رقم الهاتف',
                'new_password.required' => 'برجاء إدخال كلمه المرور',
                'new_password_confirmation.required' => 'برجاء إدخال تأكيد كلمه المرور',
                'new_password_confirmation.same' => 'كلمه المرور وتأكيدها لا تتطابق',
                'new_password.min' => 'كلمه المرور لا تقل عن 6 حروف',
                'new_password.max' => 'كلمه المرور لا تزيد عن 15 حوف',
            ]
        );
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $user = User::wherePhone($request->phone)->first();
        if ($user) {
            $user->update(['password' => $request->new_password]);
            return apiResponse(200, 'تم تغير كلمه المرور بنجاح', new UserResource($user), true);
        }
        return apiResponse(404, 'برجاء التأكد من رقم الهاتف');
    }

    public function login(Request $request)
    {
        $v = validator(
            $request->all(),
            [
                'phone' => 'required_without:social_token|max:50',
                'password' => 'required_without:social_token',
            ],
            [
                'phone.required' => 'برجاء إدخال رقم الهاتف',
                'password.required' => 'برجاء إدخال كلمه المرور',

            ]
        );
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        if (!$request->social_token) {
            $user = User::wherePhone($request->phone)->first();
            if ($user) {
                if (Hash::check($request->password, $user->password)) {
                    if (!$user->activated) {
                        return apiResponse(403, 'الحساب غير مفعل');
                    } elseif (!$user->is_active) {
                        return apiResponse(405, 'الحساب معلق من قبل الأدمن');
                    }
                    $user->update(['jwt' => jwtGenerator()]);
                    return apiResponse(200, 'تم تسجيل الدخول بنجاح', new UserResource($user), true);
                } else {
                    return apiResponse(405, 'برجاء التأكد من رقم الهاتف وكلمه المرور');
                }
            } else {
                return apiResponse(404, 'برجاء التأكد من رقم الهاتف');
            }
        } else {
            if ($user = User::where('social_token', $request->social_token)->first()) {
                $user->update([
                    'social_img' => $request->social_img
                ]);
                return apiResponse(200, 'تم تسجيل الدخول بنجاح', new UserResource($user), true);
            } else {
                return apiResponse(405, 'المستخدم غير موجود');
            }
        }
    }
}
