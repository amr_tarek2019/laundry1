<?php

namespace App\Http\Controllers\Api\Delegate;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuggestionsController extends Controller
{

    public function add(Request $request)
    {
        $v = validator($request->all(), [
            'title' => 'required|max:50',
            'details' => 'required|max:500',
        ], [
            'title.required' => 'برجاء إدخال العنوان',
            'details.required' => 'برجاء إدخال التفاصيل',
        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $user = getDelegateByJwt($request);
        $user->suggestions()->create($request->all());
        return apiResponse(200, 'تم الإرسال بنجاح');
    }

}
