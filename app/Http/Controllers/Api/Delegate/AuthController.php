<?php

namespace App\Http\Controllers\Api\Delegate;

use App\Delegate;
use App\Http\Resources\Delegate\User\DelegateResource;
use App\Http\Resources\Delegate\User\DelegateResourceCollection;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /*   public function register(Request $request)
       {
           $v = validator($request->all(), [
               'name' => 'required|max:50',
               'email' => 'required|max:50|unique:delegates,email',
               'password' => 'required_without:social_token|max:50',
               'phone' => 'required|max:50|unique:delegates,phone',
               'identity_doc' => 'required',
               'notification_token' => 'required',
               'device_id' => 'required',
           ], [
               'name.required' => 'برجاء إدخال الإسم',
               'email.required' => 'برجاء إدخال البريد الإلكترونى',
               'email.unique' => ' البريد الإلكترونى مسجل بالفعل',
               'password.required' => ' برجاء إدخال كلمه المرور',
               'phone.required' => 'برجاء إدخال رقم الهاتف',
               'phone.unique' => ' رقم الهاتف مسجل بالفعل',
               'identity_doc.required' => ' برجاء إختيار إثبات الهويه',


           ]);
           if ($v->fails()) {
               return apiResponse(405, $v->errors()->first());
           }
           $input = $request->all();
           $input['jwt'] = jwtGenerator();
           $input['activated'] = $request->social_token ? 1 : 0;
           $user = Delegate::create($input);

           // TODO :: send the code by sms to the user and enable it for production
   //            $user->code()->create(['code' => generateRandomCode(4, $user->id)]);
           $user->code()->create(['code' => 1111]);

           return apiResponse(200, 'تم التسجيل وإرسال الكود بنجاح', (object)array());
       }*/

    public function codeConfirmation(Request $request)
    {
        $v = validator($request->all(), [
            'phone' => 'required|max:50',
            'code' => 'required',
        ], [
            'phone.required' => 'برجاء إدخال رقم الهاتف',
            'code.required' => 'برجاء إدخال كود التفعيل'
        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $user = Delegate::wherePhone($request->phone)->first();
        if ($user) {
            if ($user->code->code === $request->code) {
                // TODO :: enable on production
                //   $user->code->update(['code' => generateRandomCode(4, $user->id)]);
                $user->update(['activated' => 1]);
                return apiResponse(200, 'تم التفعيل بنجاح', new DelegateResource($user));
            } else {
                return apiResponse(401, 'كود غير صحيح');
            }
        } else {
            return apiResponse(404, 'برجاء التأكد من رقم الهاتف');
        }
    }

    public function resetPassword(Request $request)
    {
        $v = validator($request->all(), [
            'phone' => 'required|max:50',
        ], [
            'phone.required' => 'برجاء إدخال رقم الهاتف',
        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $user = Delegate::wherePhone($request->phone)->first();
        if ($user) {
            // TODO :: send the code by sms to the user and enable it for production
            // TODO :: enable on production
            //   $user->code->update(['code' => generateRandomCode(4, $user->id)]);
            return apiResponse(200, 'تم إرسال الكود بنجاح', (object)array());
        } else {
            return apiResponse(404, 'برجاء التأكد من رقم الهاتف');
        }
    }

    public function changePassword(Request $request)
    {
        $v = validator($request->all(), [
            'phone' => 'required|max:50',
            'new_password' => 'required|min:6|max:15',
            'new_password_confirmation' => 'required|same:new_password',
        ],
            [
                'phone.required' => 'برجاء إدخال رقم الهاتف',
                'new_password.required' => 'برجاء إدخال كلمه المرور',
                'new_password_confirmation.required' => 'برجاء إدخال تأكيد كلمه المرور',
                'new_password_confirmation.same' => 'كلمه المرور وتأكيدها لا تتطابق',
                'new_password.min' => 'كلمه المرور لا تقل عن 6 حروف',
                'new_password.max' => 'كلمه المرور لا تزيد عن 15 حوف',
            ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $user = Delegate::wherePhone($request->phone)->first();
        if ($user) {
            $user->update(['password' => $request->new_password]);
            return apiResponse(200, 'تم تغير كلمه المرور بنجاح', new DelegateResource($user), true);
        }
        return apiResponse(404, 'برجاء التأكد من رقم الهاتف');
    }

    public function login(Request $request)
    {
        $v = validator($request->all(), [
            'phone' => 'required_without:social_token|max:50',
            'password' => 'required_without:social_token',
            'notification_token' => 'required',
            'device_id' => 'required',
        ],
            [
                'phone.required' => 'برجاء إدخال رقم الهاتف',
                'password.required' => 'برجاء إدخال كلمه المرور',

            ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $user = Delegate::wherePhone($request->phone)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                if (!$user->is_active) {
                    return apiResponse(405, 'الحساب معلق من قبل الأدمن');
                }
                $user->update([
                    'jwt' => jwtGenerator(),
                    'notification_token' => $request->notification_token,
                    'device_id' => $request->device_id,
                ]);
                return apiResponse(200, 'تم تسجيل الدخول بنجاح', new DelegateResource($user), true);
            } else {
                return apiResponse(405, 'برجاء التأكد من رقم الهاتف وكلمه المرور');
            }
        } else {
            return apiResponse(404, 'برجاء التأكد من رقم الهاتف');
        }
    }
}
