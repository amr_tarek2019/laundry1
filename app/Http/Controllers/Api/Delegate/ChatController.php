<?php

namespace App\Http\Controllers\Api\Delegate;

use App\Chat;
use App\ChatMessages;
use App\Delegate;
use App\Http\Resources\Orders\ChatResource;
use App\Order;
use App\traits\PushNotificationTrait;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatController extends Controller
{
    use PushNotificationTrait;
    public function store(Request $request)
    {
        $v = validator($request->all(), [
            'order_id' => 'required|exists:orders,id',
            'message' => 'required_if:message,m',
            'image' => 'required_if:image,i'
        ], [
            'order_id.required' => 'برجاء إدخال رقم الطلب',
            'order_id.exists' => 'رقم الطلب غير صحيح',
            //'message.required' => 'برجاء إدخال المحتوى',
        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $order = Order::find($request->order_id);
        $chat_check = Chat::where('order_id', $order->id)->first();

        if (!$chat_check) {
            $chat = Chat::create([
                'sender_id' => getDelegateByJwt($request)->id,
                'sender_type' => 'delegate',
                'target_id' => $order->delegate_id,
                'order_id' => $order->id,
            ]);
        } else {
            $chat = $chat_check;
        }

        $chat->messages()->create([
            'sent_from' => 'delegate',
            'content' => $request->message,
            'image'=>$request->image,
        ]);
        $this->PushNotification(getDelegateByJwt($request)->name, $request->message, $order->user->notification_token, 'new_message', $order->id);
        return apiResponse(200, 'تم الإرسال بنجاح', new ChatResource($chat), true);
    }

}
