<?php

namespace App\Http\Controllers\Api\Delegate;

use App\Http\Resources\Orders\OrdersResourceCollection;
use App\Notifications\newOderStatusNotification;
use App\Order;
use App\traits\PushNotificationTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;

class OrdersController extends Controller
{
    use PushNotificationTrait;

   
    public function getOrders(Request $request)
    {
        $v = validator($request->all(), [
            'lat' => 'required',
            'lng' => 'required',
        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }

        $orders = Order::where('delegate_id', null)->where('order_status', '0')
            ->whereRaw("( 6371 * acos ( cos ( radians(" . $request->lat . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $request->lng . ") ) + sin ( radians(" . $request->lat . ") ) * sin( radians( lat ) ) ) <= 50) ")
            ->orderBy('id','desc')
            ->get();
        // return apiResponse(200, 'الطلبات', new OrdersResourceCollection(getDelegateByJwt($request)->orders()->orderBy('id','desc')->get()),true);
        // dd($orders);
        
        //  return apiResponse(200, 'الطلبات', new OrdersResourceCollection(getDelegateByJwt($request)->orders()->orderBy('id','desc')->get()),true);
        
        $order_Items = [];
        $order_list =[];
        foreach($orders as $order)
        {
                
                
            $order_Items['id'] = $order['id'];
            $order_Items['laundry_id'] = $order['laundry_id'];
            $order_Items['delegate_id'] = $order['delegate_id'];
            $order_Items['order_status'] = $order['order_status'];
            $order_Items['reason'] = $order['reason'];
             $order_Items['description'] = $order['description'];
             $order_Items['order_type'] = $order['order_type'];
              $order_Items['city'] = $order['city'];
              $order_Items['delivery'] = $order['delivery'];
              $order_Items['address'] = $order['address'];
                // $order_Items['user'] = new UserResource($order->user);
                //$order_Items['laundry'] = new LaundryDetails($order->laundry);
                $order_Items['user'] = \App\User::where('id',$order->user_id)->select('id','social_token','name','email','img','phone','social_img','jwt','activated')->first();
                
                
                 $order_Items['laundry'] = \App\Laundry::where('id',$order->laundry_id)->select('name','name_en','owner_id','section_id','img','description','description_en','lat','lng','address','address_en','has_offers','phone','email','is_active','expire_date')->first();
                 //$order_Items['delegate'] = $order->delegate ? new DelegateResource($order->delegate) : '';
                //   $order_Items['delegate'] = \App\Delegate::where('id',$order->delegate_id)->select('id','name','email','phone','img')->first();
                  $order_Items['services'] = $order->getOrderServices();
                  //$order_Items['chat'] = new ChatResource($order->chat);
                   $order_Items['created_at'] = $order['created_at'];
       

            $order_list[] = $order_Items;
          

        }
        //   return $order_list;
     
        return apiResponse(200, 'طلباتى',$order_list, true);
    
    }

    public function acceptOrder(Request $request)
    {
        $v = validator($request->all(), [
            'order_id' => 'required|exists:orders,id',
        ], [
            'order_id.required' => 'برجاء إدخال رقم الطلب',
            'order_id.exists' => 'رقم الطلب غير صحيح',

        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $order = Order::find($request->order_id);
        if (!$order->delegate_id) {
            $order->update([
                'delegate_id' => getDelegateByJwt($request)->id,
                'order_status'=>'1'
            ]);
            $order->user->notify(new newOderStatusNotification('تم تعيين طلبك إلى مندوب', $order->id, 'تم تحديث حاله الطلب'));
            $this->PushNotification('تم تحديث حاله الطلب', 'تم تعيين طلبك إلى مندوب', $order->user->notification_token, 'new_order_status', $order->id);
            return apiResponse(200, app()->isLocale('ar')  ? 'تم قبول الطلب بنجاح' : 'order accepted succesffully');
        } else {
            return apiResponse(405,app()->isLocale('ar')  ? 'الطلب غير متاح' : 'order not available');
        }
    }

    public function myOrders(Request $request)
    {
        if ($request->order_id) {
            $order = Order::where('id', $request->order_id)->get();
            if ($order->count()) {
                if (checkOrderOwner(getDelegateByJwt($request)->id, $request->order_id) == true) {
                    return apiResponse(200, 'تفاصيل الطلب', new OrdersResourceCollection($order), true);
                }
                return apiResponse(405, 'حدث خطأ , حاول مره أخرى');
            }
            return apiResponse(404, 'الطلب غير موجود');
        }
        return apiResponse(200, 'طلباتى', new OrdersResourceCollection(getDelegateByJwt($request)->orders()->orderBy('id','desc')->get()), true);
    }

    public function changeStatus(Request $request)
    {
        $v = validator($request->all(), [
            'order_id' => 'required',
            'status' => 'required|in:1,2,3,4,5',
            'reason' => 'required_if:status,5'

        ], [
            'order_id.required' => 'برجاء إدخال رقم الطلب',
            'status.required' => 'برجاء إدخال رقم الحاله',
            'reason.required_if' => 'برجاء إدخال سبب الإلغاء',
        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $order = Order::find($request->order_id);
        if (!$order) {
            return apiResponse(404, 'الطلب غير موجود');
        }
        $user = getDelegateByJwt($request);
        if ($user->id != $order->delegate_id) {
            return apiResponse(405, 'ليس لديك صلاحيه');
        }
        if ($order->order_status == 4 || $order->order_status == 5) {
            return apiResponse(405, 'لا يمكن تغير حاله الطلب');
        }
        if ($request->status == 1) {
            $message = 'المندوب فى الطريق إليك';
        } elseif ($request->status == 2) {
            $message = 'تم تسليم الطلب إلى المغسله';
        } elseif ($request->status == 3) {
            $message = 'تم إستلام الطلب من المغسله';
        } elseif ($request->status == 4) {
            $message = 'تم إكمال الطلب ';
        } elseif ($request->status == 5) {
            $message = 'تم إلغاء الطلب ';
        }
        $order->update(['order_status' => $request->status]);
        $order->user->notify(new newOderStatusNotification($message, $order->id, 'تم تحديث حاله الطلب'));
        $this->PushNotification('تم تحديث حاله الطلب', $message,  $order->user->notification_token, 'new_order_status', $order->id);
        return apiResponse(200, 'تم تغير حاله الطلب بنجاح');
    }
    
       public function fastOrderDetails(Request $request)
        {
              $orders = getUserByJwt($request)->orders->when(Order::find($request->order_id), function ($order) use ($request) {
            return $order->where('id', $request->order_id);
        });
        return apiResponse(200, 'الطلبات', new OrdersResourceCollection($orders), true, $request->per_page);
        }
}
