<?php

namespace App\Http\Controllers\Api\Delegate;

use App\Http\Resources\NotificationParserResourceCollection;
use App\PushNotification;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Delegate\User\DelegateResource;
use App\Http\Resources\NotificationResourceCollection;

class ProfileController extends Controller
{

    public function updateInfo(Request $request)
    {
        $user = getDelegateByJwt($request);
        $user->update($request->all());
        return apiResponse(200, 'تم تعديل البيانات بنجاح', new DelegateResource($user));
    }

    public function updatePassword(Request $request)
    {
        $v = validator($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required|min:6|max:15',
            'new_password_confirmation' => 'required|same:new_password',
        ],
            [
                'old_password.required' => 'برجاء إدخال كلمه المرور القديمه',
                'new_password.required' => 'برجاء إدخال كلمه المرور الجديده',
                'new_password.min' => ' كلمه المرور الجديده لا تقل عن 6 حروف',
                'new_password.max' => ' كلمه المرور الجديده لا تزيد عن 15 حروف',
                'new_password_confirmation.required' => 'برجاء إدخال تأكيد كلمه المرور الجديده',
                'new_password_confirmation.same' => 'تأكيد كلمه المرور الجديده لا تتطابق',
            ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $user = getDelegateByJwt($request);
        $input = $request->all();
        if (Hash::check($request->old_password, $user->password)) {
            $user->update(['password' => $input['new_password']]);
            return apiResponse(200, 'تم تحديث كلمه المرور بنجاح', new DelegateResource($user));
        }
        return apiResponse(205, 'كلمه المرور القديمه غير صحيحه');

    }


    public function updateNotificationToken(Request $request)
    {
        $v = validator($request->all(), [
            'notification_token' => 'required',
        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        getDelegateByJwt($request)->update(['notification_token' => $request->notification_token]);
        return apiResponse(200, 'تم التعديل بنجاح');
    }


    public function getNotifications(Request $request)
    {
        $user = getDelegateByJwt($request);
        $notifications = new NotificationParserResourceCollection($user->notifications);
        $push_notifications = new NotificationResourceCollection(PushNotification::where('type', 'all')->orWhere('type', 'delegates')->get());
        $merged_notification = collect($push_notifications)->merge(collect($notifications));
        return apiResponse(200, 'الإشعارات', $merged_notification, true);
    }
}
