<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatMessages extends Model
{
     protected  $fillable=[

             'chat_id',
             'sent_from',
             'content',
             'image',

         ];
         
           public function setImageAttribute($value)
    {
        if ($value!=null)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/message/image/'),$imageName);
            $this->attributes['image']=$imageName;
        }else{
            return null;
        }
    }
    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/message/image/'.$value);
        } else {
             return null;
        }
    }
}
