<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class Delegate extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'img',
        'identity_doc',
        'jwt',
        'notification_token',
        'device_id',
        'is_active',
    ];

    public function code()
    {
        return $this->morphOne(Code::class, 'receiver');
    }

    public function delegateFees()
    {
        return $this->orders->where('order_status', 4)->where('is_collected', 0)->pluck('order_total')->sum();
    }
    public function completedOrders(){
        return $this->orders()->where('order_status',4)->count();
    }
    public function restDelegateFees()
    {
        foreach ($this->orders->where('order_status', 4)->where('is_collected', 0) as $order) {

            $order->update(['is_collected' => 1]);
        }
    }

    public function getImgAttribute($value)
    {
        return $value ? asset('uploads/' . class_basename($this) . '/' . $value) : "";
    }

    public function setImgAttribute($value)
    {
        if (is_file($value)) {
            if ($value) {
                $name = rand(0000, 9999) . time() . '.' . $value->getClientOriginalExtension();
                $value->move(public_path('uploads/' . class_basename($this)), $name);
                $this->attributes['img'] = $name;
            }
        } else {
            $this->attributes['img'] = $value;
        }
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

//    public function getIdentityDocAttribute($value)
//    {
//        return $value ? asset('uploads/' . class_basename($this) . '/' . $value) : "";
//    }

    public function identityDownload()
    {
        return public_path('uploads/' . class_basename($this) . '/' . $this->identity_doc);
    }

    public function setIdentityDocAttribute($value)
    {
        if (is_file($value)) {
            if ($value) {
                $name = rand(0000, 9999) . time() . '.' . $value->getClientOriginalExtension();
                $value->move(public_path('uploads/' . class_basename($this)), $name);
                $this->attributes['identity_doc'] = $name;
            }
        } else {
            $this->attributes['identity_doc'] = $value;
        }
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'delegate_id');
    }

    public function suggestions()
    {
        return $this->morphMany(Suggestion::class, 'user');
    }
}
