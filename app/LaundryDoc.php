<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LaundryDoc extends Model
{
    use SoftDeletes;
    protected $fillable = [

        'laundry_id',
        'doc',
    ];

    public function getDocAttribute($value)
    {
        return $value ? asset('uploads/' . class_basename($this) . '/' . $value) : "";
    }

    public function setDocAttribute($value)
    {
        if (is_file($value)) {
            if ($value) {
                $name = rand(0000, 9999) . time() . '.' . $value->getClientOriginalExtension();
                $value->move(public_path('uploads/' . class_basename($this)), $name);
                $this->attributes['doc'] = $name;
            }
        } else {
            $this->attributes['doc'] = $value;
        }
    }
}
