<?php

namespace App\traits;
trait PushNotificationTrait
{
    function PushNotification($title, $body, $patch, $type, $order_id)
    {
        if (!empty($patch)) {
            if (!is_array($patch)) {
                $patch = [$patch];
            }
            fcm()
                ->to($patch)// $recipients must an array
                ->data([
                    'title' => $title,
                    'body' => $body,
                    'type' => $type,
                    'order_id' => $order_id
                ])
                ->notification([
                    'title' => $title,
                    'body' => $body,
                    'type' => $type,
                    'order_id' => $order_id
                ])
                ->send();
        }
   
    }

}