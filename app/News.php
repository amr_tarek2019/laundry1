<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['img', 'title', 'description'];

    public function getImgAttribute($value)
    {
        return $value ? asset('uploads/' . class_basename($this) . '/' . $value) : "";
    }


    public function setImgAttribute($value)
    {
        if (is_file($value)) {
            if ($value) {
                $name = rand(0000, 9999) . time() . '.' . $value->getClientOriginalExtension();
                $value->move(public_path('uploads/' . class_basename($this)), $name);
                $this->attributes['img'] = $name;
            }
        } else {
            $this->attributes['img'] = $value;
        }
    }
}
