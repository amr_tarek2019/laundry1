<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
     protected  $fillable=[

             'lat',
             'lng',
             'user_id',
             'address',

         ];
}
