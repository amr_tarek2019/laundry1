<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Laundry extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'expire_date',
    ];
    protected $casts = [
        'section_id' => 'array',
    ];
    protected $appends = [
        'modified_expire_date',
    ];

    public function getModifiedExpireDateAttribute($value)
    {
        return Carbon::parse($this->expire_date)->format('Y-m-d');
    }

    protected $fillable = [

        'name',
        'name_en',
        'owner_id',
        'section_id',
        'img',
        'description',
        'description_en',
        'lat',
        'lng',
        'address',
        'address_en',
        'has_offers',
        'phone',
        'email',
        'is_active',
        'expire_date',
        'is_collected',
        'is_paid',
    ];

    public function docs()
    {
        return $this->hasMany(LaundryDoc::class);
    }

    public function services()
    {
        return $this->hasMany(LaundryServices::class);
    }

    public function getImgAttribute($value)
    {
        return $value ? asset('uploads/' . class_basename($this) . '/' . $value) : "";
    }

    public function setImgAttribute($value)
    {
        if (is_file($value)) {
            if ($value) {
                $name = rand(0000, 9999) . time() . '.' . $value->getClientOriginalExtension();
                $value->move(public_path('uploads/' . class_basename($this)), $name);
                $this->attributes['img'] = $name;
            }
        } else {
            $this->attributes['img'] = $value;
        }
    }

    public function orders()
    {
        return $this->hasMany(Order::class)->withTrashed();
    }

    public function owner()
    {
        return $this->belongsTo(LaundryOwner::class);
    }

    public function rate()
    {
        return $this->hasMany(Rate::class, 'laundry_id');
    }


    public function totalRate()
    {
        $rate = $this->rate->count() ?  $this->rate->sum('rate') / $this->rate->count() : 0;
        return $rate;
    }
}
