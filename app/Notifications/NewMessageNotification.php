<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewMessageNotification extends Notification implements ShouldQueue
{
    use Queueable;
    public $message;
    public $user_name;
    public $user_id;

    /**
     * NewMessageNotification constructor.
     * @param $message
     * @param $user_name
     * @param $user_id
     */
    public function __construct($message, $user_name, $user_id)
    {
        $this->message = $message;
        $this->user_name = $user_name;
        $this->user_id = $user_id;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    public function toDatabase($notifiable)
    {
        return [
            'message' => $this->message,
            'user_name' => $this->user_name,
            'user_id' => $this->user_id,
        ];
    }

}
