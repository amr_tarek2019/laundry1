<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class newOrderNotification extends Notification implements ShouldQueue
{
    use Queueable;
    public $title;
    public $message;
    public $order_id;

    /**
     * newOrderNotification constructor.
     * @param $message
     * @param $order_id
     * @param null $title
     */
    public function __construct($message, $order_id, $title = null)
    {
        $this->title = $title;
        $this->message = $message;
        $this->order_id = $order_id;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * @param $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'message' => $this->message,
            'title' => $this->title,
            'order_id' => $this->order_id,
        ];
    }

}
