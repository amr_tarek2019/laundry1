<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = [
        'rate',
        'user_id',
        'laundry_id',
        'order_id',
    ];
}
