<?php

use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

if (!function_exists('apiResponse')) {
    function apiResponse($status, $msg, $data = null, $force_data = false, $per_page = null)
    {
        if (!$data && !$force_data) {
            return [
                'status' => $status,
                'msg' => $msg,
            ];
        } else {
            return [
                'status' => $status,
                'msg' => $msg,
                'data' => $per_page ? manual_pagination($data, $per_page ?? $per_page) : $data
            ];
        }
    }
};

if (!function_exists('jwtGenerator')) {
    function jwtGenerator()
    {
        return Str::random(60);
    }
}

if (!function_exists('getUserByJwt')) {
    function getUserByJwt($request)
    {
        $token = $request->header('jwt');
        return User::where('jwt', $token)->first();
    }
}

if (!function_exists('getDelegateByJwt')) {
    function getDelegateByJwt($request)
    {
        $token = $request->header('jwt');
        return \App\Delegate::where('jwt', $token)->first();
    }
}

if (!function_exists('add_file')) {
    function add_file($request_file, $model)
    {
        if ($file = $request_file) {
            $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/' . $model), $name);
        }
        return $name;
    }
}

if (!function_exists('update_file')) {
    function update_file($request_file, $checker, $database_name, $model)
    {
        if ($file = $request_file) {
            @unlink(public_path('uploads/' . $model) . '/' . $checker->$database_name);
            $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/' . $model), $name);
        }
        return $name;
    }
}

if (!function_exists('delete_file')) {
    function delete_file($checker, $database_name, $model)
    {
        @unlink(public_path('uploads/' . $model) . '/' . $checker->$database_name);
    }
}

if (!function_exists('generateRandomCode')) {
    function generateRandomCode($digits, $id = null)
    {
        $i = 0; //counter
        $value = $id; //our default pin is the order id .
        while ($i < $digits - intval(strlen($id))) {
            //generate a random number between 0 and 9.
            $value .= mt_rand(0, 9);
            $i++;
        }
        return $value;
    }
}

if (!function_exists('manual_pagination')) {
    function manual_pagination($items, $perPage = 5, $page = null)
    {
        $pageName = 'page';
        $page = $page ?: (Paginator::resolveCurrentPage($pageName) ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator(
            $items->forPage($page, $perPage)->values(),
            $items->count(),
            $perPage,
            $page,
            [
                'path' => Paginator::resolveCurrentPath(),
                'pageName' => $pageName,
            ]
        );
    }
}

if (!function_exists('sidebarActive')) {
    function sidebarActive(array $routes)
    {
        if (in_array(Route::currentRouteName(), $routes)) {
            return 'active';
        }
        return '';
    }
}

if (!function_exists('adminNotification')) {
    function adminNotification()
    {
        return auth('admin')->user()->unreadNotifications->where('type','!=','App\Notifications\NewMessageNotification');
    }
}
if (!function_exists('adminMessagesNotification')) {
    function adminMessagesNotification()
    {
        return auth('admin')->user()->unreadNotifications->where('type','=','App\Notifications\NewMessageNotification');
    }
}

if (!function_exists('laundryNotification')) {
    function laundryNotification()
    {
        return auth('laundry')->user()->unreadNotifications->where('type','!=','App\Notifications\NewMessageNotification');
    }
}
if (!function_exists('laundryMessagesNotification')) {
    function laundryMessagesNotification()
    {
        return auth('laundry')->user()->unreadNotifications->where('type','=','App\Notifications\NewMessageNotification');
    }
}


if (!function_exists('checkOrderOwner')) {
    function checkOrderOwner($delegate_id, $order_id)
    {
        return \App\Order::where('id', $order_id)->where('delegate_id', $delegate_id)->first() ? true : false;
    }
}






