<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class LaundryOwner extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'img',
        'is_active',
        'enabled',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function orders()
    {
        return $this->hasManyThrough(Order::class, Laundry::class, 'owner_id', 'laundry_id');
    }

    public function laundry()
    {
        return $this->hasOne(Laundry::class, 'owner_id')->withDefault();
    }

    public function completedOrders()
    {
        return $this->orders()->where('order_status', 4)->count();
    }

    public function defaultLaundry()
    {
        return $this->laundry()->create([
            'owner_id' => $this->id,
            'section_id' => [1, 2],
            'name' => 'default',
            'name_en' => 'default english',
            'description' => 'default',
            'description_en' => 'default english',
            'lat' => 21.383469, 
            'lng' => 39.861603,
            'address' => 'default',
            'has_offers' => 0,
            'phone' => 00,
            'email' => 'default',
            'is_active' => 0,
            'expire_date' => \Carbon\Carbon::yesterday(),
        ]);
    }

    public function restLaundryFees()
    {
        foreach ($this->orders->where('order_status', 4)->where('is_paid', 0) as $order) {

            $order->update(['is_paid' => 1]);
        }
    }

    public function laundryFees()
    {
        return $this->orders->where('order_status', 4)->where('is_paid', 0)->pluck('order_total')->sum();
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function conversations()
    {
        $laundries_conversations = Conversation::where('user_one_type', 'Laundry')->where('user_one', $this->id)->get()
            ->merge(Conversation::where('user_two_type', 'Laundry')->where('user_two', $this->id)->get());
        return $laundries_conversations;
    }
}
