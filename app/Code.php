<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $fillable = [
        'code',
        'user_id'
    ];


    public function receiver()
    {
        return $this->morphTo();
    }
}
