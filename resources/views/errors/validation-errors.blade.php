@if(!empty($errors->first()))
    <div id="validation">
        <div class="alert alert-danger text-center" style="margin-top: 10px">
            <span>{{ $errors->first() }}</span>
        </div>
    </div>
@endif