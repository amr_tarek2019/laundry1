<!DOCTYPE html>
<html lang="ar"  dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="endless admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, endless admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{asset('uploads/Laundry/28081563262562.png') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('uploads/Laundry/28081563262562.png') }}" type="image/x-icon">
    <title>Laundry Station  - Admin Dashboard</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/css/fontawesome.css') }}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/css/icofont.css') }}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/css/themify.css') }}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/css/flag-icon.css') }}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/css/feather-icon.css') }}">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/css/chartist.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/css/prism.css') }}">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/css/bootstrap.css') }}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/css/style.css') }}">
    <link id="color" rel="stylesheet" href="{{asset('assets/backend/css/light-1.css') }}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/css/responsive.css') }}">
</head>
<body main-theme-layout="rtl">
<!-- Loader starts-->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="whirly-loader"> </div>
    </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper" dir="rtl">
    <!-- Page Header Start-->
@include('backend.dashboard.layouts.navbar')
    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
    @include('backend.dashboard.layouts.sidebar')

        @yield('content')

        <!-- footer start-->
     @include('backend.dashboard.layouts.footer')
    </div>
</div>
<!-- latest jquery-->
<script src="{{asset('assets/backend/js/jquery-3.2.1.min.js') }}"></script>
<!-- Bootstrap js-->

<script src="{{asset('assets/backend/js/bootstrap/popper.min.js') }}"></script>
<script src="{{asset('assets/backend/js/bootstrap/bootstrap.js') }}"></script>
<!-- feather icon js-->
<script src="{{asset('assets/backend/js/icons/feather-icon/feather.min.js') }}"></script>
<script src="{{asset('assets/backend/js/icons/feather-icon/feather-icon.js') }}"></script>
<!-- Sidebar jquery-->
<script src="{{asset('assets/backend/js/sidebar-menu.js') }}"></script>
<script src="{{asset('assets/backend/js/config.js') }}"></script>
<!-- Plugins JS start-->
<script src="{{asset('assets/backend/js/chart/morris-chart/raphael.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/morris-chart/morris.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/morris-chart/prettify.min.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/chartjs/chart.min.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/chartist/chartist.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/knob/knob.min.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/knob/knob-chart.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/flot-chart/excanvas.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/flot-chart/jquery.flot.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/flot-chart/jquery.flot.time.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/flot-chart/jquery.flot.categories.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/flot-chart/jquery.flot.stack.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/flot-chart/jquery.flot.pie.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/flot-chart/jquery.flot.symbol.js') }}"></script>
<script src="{{asset('assets/backend/js/chart/flot-chart/flot-script.js') }}"></script>
<script src="{{asset('assets/backend/js/prism/prism.min.js') }}"></script>
<script src="{{asset('assets/backend/js/clipboard/clipboard.min.js') }}"></script>
<script src="{{asset('assets/backend/js/counter/jquery.waypoints.min.js') }}"></script>
<script src="{{asset('assets/backend/js/counter/jquery.counterup.min.js') }}"></script>
<script src="{{asset('assets/backend/js/counter/counter-custom.js') }}"></script>
<script src="{{asset('assets/backend/js/custom-card/custom-card.js') }}"></script>
<script src="{{asset('assets/backend/js/dashboard/default.js') }}"></script>
<script src="{{asset('assets/backend/js/notify/index.js') }}"></script>
<script src="{{asset('assets/backend/js/typeahead/handlebars.js') }}"></script>
<script src="{{asset('assets/backend/js/typeahead/typeahead.bundle.js') }}"></script>
<script src="{{asset('assets/backend/js/typeahead/typeahead.custom.js') }}"></script>
<script src="{{asset('assets/backend/js/chat-menu.js') }}"></script>
<script src="{{asset('assets/backend/js/height-equal.js') }}"></script>
<script src="{{asset('assets/backend/js/tooltip-init.js') }}"></script>
<script src="{{asset('assets/backend/js/typeahead-search/handlebars.js') }}"></script>
<script src="{{asset('assets/backend/js/typeahead-search/typeahead-custom.js') }}"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{asset('assets/backend/js/script.js') }}"></script>

<script src="{{asset('assets/backend/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/dataTables.buttons.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/jszip.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/buttons.colVis.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/pdfmake.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/vfs_fonts.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/dataTables.autoFill.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/dataTables.select.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/buttons.bootstrap4.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/buttons.html5.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/buttons.print.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/dataTables.responsive.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/responsive.bootstrap4.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/dataTables.keyTable.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/dataTables.colReorder.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/dataTables.rowReorder.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/dataTables.scroller.min.js') }}"></script>
<script src="{{asset('assets/backend/js/datatable/datatable-extension/custom.js') }}"></script>

@yield('scripts')
<!-- Plugin used-->
</body>
</html>
