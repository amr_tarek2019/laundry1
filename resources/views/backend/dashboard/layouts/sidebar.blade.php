<div class="page-sidebar">
    <div class="main-header-left d-none d-lg-block">
        <div class="logo-wrapper" style="margin-right: 45px;"><a href="index.html">
                <img src="{{asset('uploads/Laundry/28081563262562.png') }}" alt="" width="70px" style="margin-right:35px"></a></div>
    </div>
    <div class="sidebar custom-scrollbar">
        <div class="sidebar-user text-center">
            <div><img class="img-60 rounded-circle" src="{{asset('/uploads/User/' . auth()->guard('admin')->user()->img)}}" alt="#">
                <div class="profile-edit"><a href="edit-profile.html" target="_blank"><i data-feather="edit"></i></a></div>
            </div>
            <h6 class="mt-3 f-14">{{Auth::guard('admin')->user()->name}}</h6>
            <p>{{Auth::guard('admin')->user()->email}}</p>
        </div>
        <ul class="sidebar-menu">
            <li><a class="sidebar-header" href="{{route('dashboard')}}"><i data-feather="home"></i><span>لوحة التحكم</span></a>

            </li>

            <li><a class="sidebar-header" href="index.html#"><i data-feather="users"></i><span>المستخدمين</span></a>

            </li>

            <li><a class="sidebar-header" href="index.html#"><i data-feather="users"></i><span>المندوبين</span></a>

            </li>

            <li><a class="sidebar-header" href="index.html#"><i data-feather="users"></i><span>المشرفين</span></a>

            </li>

            <li><a class="sidebar-header" href="index.html#"><i data-feather="users"></i><span>المغاسل</span></a>

            </li>

            <li><a class="sidebar-header" href="index.html#"><i data-feather="map-pin"></i><span>المدن</span></a>

            </li>

            <li><a class="sidebar-header" href="index.html#"><i data-feather="shopping-cart"></i><span>الطلبات</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="general-widget.html"><i class="fa fa-circle"></i>عادية</a></li>
                    <li><a href="chart-widget.html"><i class="fa fa-circle"></i>سريعة</a></li>
                </ul>
            </li>

            <li><a class="sidebar-header" href="index.html#"><i data-feather="mail"></i><span>المقترحات</span></a>

            </li>

            <li><a class="sidebar-header" href="index.html#"><i data-feather="bell"></i><span>الاشعارات</span></a>

            </li>

            <li><a class="sidebar-header" href="index.html#"><i data-feather="settings"></i><span>الاعدادات</span></a>

            </li>

        </ul>
    </div>
</div>
