<div class="br-logo">
    <a target="_blank" href="http://2grand.net/">
        <span>
        <img style="max-width: 90% !important;" src="{{asset('/android.png')}}" class="grand">
        </span>
    </a>
</div><div class="br-sideleft overflow-y-auto">
    <label class="sidebar-label pd-x-15 mg-t-20" style="direction: rtl">القائمه الرئيسيه</label>
    <div class="br-sideleft-menu">
        <a href="{{route('admin.dashboard.index')}}" class="br-menu-link {{sidebarActive(['admin.dashboard.index'])}}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">الرئيسيه</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        @can('show_users')
        <a href="{{route('admin.users.index')}}" class="br-menu-link {{sidebarActive(['admin.users.index','admin.users.user.details'])}}" >
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-people tx-24"></i>
                <span class="menu-item-label">المستخدمين</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        @endcan
        @can('show_delegates')
        <a href="{{route('admin.delegates.index')}}" class="br-menu-link {{sidebarActive(['admin.delegates.index','admin.delegates.user.details'])}}" >
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-people-outline tx-24"></i>
                <span class="menu-item-label">المندوبين</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        @endcan
        @can('show_admins')
        <a href="{{route('admin.admins.index')}}" class="br-menu-link {{sidebarActive(['admin.admins.index','admin.admins.admin.details'])}}" >
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-person tx-24"></i>
                <span class="menu-item-label">المشرفين</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        @endcan
        @can('show_orders')
        <a href="{{route('admin.orders.index')}}" class="br-menu-link {{sidebarActive(['admin.orders.index','admin.orders.cash.back.details'])}}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-cart tx-24"></i>
                <span class="menu-item-label">الطلبات</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        @endcan
                @can('show_laundry_owner')
        <a href="{{route('admin.laundries.index')}}" class="br-menu-link {{sidebarActive(['admin.laundries.index'])}}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-person-outline tx-24"></i>
                <span class="menu-item-label">أصحاب المغسلات</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
                @endcan
        @can('show_suggestions')
        <a href="{{route('admin.suggestions.index')}}" class="br-menu-link {{sidebarActive(['admin.suggestions.index'])}}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-mic tx-24"></i>
                <span class="menu-item-label">المقترحات</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        @endcan
        @can('show_notification')
        <a href="{{route('admin.notifications.index')}}" class="br-menu-link {{sidebarActive(['admin.notifications.index'])}}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-android-notifications tx-24"></i>
                <span class="menu-item-label">الإشعارات</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        @endcan
        @can('show_news')
        <a href="{{route('admin.news.index')}}" class="br-menu-link {{sidebarActive(['admin.news.index'])}}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-network tx-24"></i>
                <span class="menu-item-label">اخبار المغاسل</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        @endcan
        @can('show_testimonials')
        <a href="{{route('admin.testimonials.index')}}" class="br-menu-link {{sidebarActive(['admin.testimonials.index'])}}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-chatboxes-outline tx-24"></i>
                <span class="menu-item-label">اراء العملاء</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        @endcan
                @can('show_cities')
        <a href="{{route('admin.cities.index')}}" class="br-menu-link {{sidebarActive(['admin.cities.index'])}}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-jet tx-24"></i>
                <span class="menu-item-label"> المدن </span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
                @endcan
        @can('edit_settings')
        <a href="{{route('admin.settings.index')}}" class="br-menu-link {{sidebarActive(['admin.settings.index'])}}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-cog tx-24"></i>
                <span class="menu-item-label"> الاعدادات العامه </span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        @endcan


    </div><!-- br-sideleft-menu -->

</div><!-- br-sideleft -->
