<!-- ########## START: HEAD PANEL ########## -->
<div class="br-header">
    <div class="br-header-left">

        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a>
        </div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a>
        </div>

    </div><!-- br-header-left -->
    <a target="_blank" href="http://2grand.net/" style="margin: 0 auto">
        <img style="max-width: 15% !important; " src="{{asset('/android.png')}}" class="grand">
    </a>
    <div class="br-header-right">
        <div class="dropdown">
            <a href="" class="nav-link pd-x-7 pos-relative" data-toggle="dropdown">
                <i class="icon ion-ios-email-outline tx-24"></i>
                <!-- start: if statement -->
                @if ((count(adminMessagesNotification())))
                    <span class="square-8 bg-danger pos-absolute t-15 r-5 rounded-circle"></span>
            @endif
            <!-- end: if statement -->
            </a>
            <div class="dropdown-menu dropdown-menu-header wd-300 pd-0-force notifications">
                <div class="d-flex align-items-center justify-content-between pd-y-10 pd-x-20 bd-b bd-gray-200">
                    <label class="tx-12 tx-info tx-uppercase tx-semibold tx-spacing-2 mg-b-0">الرسائل</label>
                    @if (count(adminMessagesNotification()))
                        <a href="{{route('admin.dashboard.read.notification',['type'=>'message'])}}"><label
                                    class="tx-12 tx-info tx-uppercase tx-semibold tx-spacing-2 mg-b-0">قرأه
                                الكل</label></a>
                    @endif

                </div><!-- d-flex -->
                <div style="overflow:auto; height: 210px">
                    <div class="media-list infinite-scroll">
                    @if ((count(adminMessagesNotification())))
                        @foreach(adminMessagesNotification() as $notification)
                            <!-- loop starts here -->
                                <a href="{{route('admin.messenger',['id'=>$notification->data['user_id'],'notification_id'=>$notification->id])}}"
                                   class="media-list-link read notification">
                                    <div class="media pd-x-20 pd-y-15">
                                        <div class="media-body">
                                            <p class="tx-13 mg-b-0 tx-gray-700"> {{$notification->data['message']}}
                                                من &nbsp;{{$notification->data['user_name']}}</p>
                                            <span class="tx-12">{{$notification->created_at->diffForHumans()}}</span>
                                        </div>
                                    </div><!-- media -->
                                </a>
                                <!-- loop ends here -->
                            @endforeach
                        @else
                            <div class="pd-y-10 tx-center bd-t">
                                <a href="#" class="tx-12"> لا يوجد رسائل جديده</a>
                            </div>
                        @endif
                        {{--                        {{adminNotification()->links()}}--}}
                    </div><!-- media-list -->
                    {{--  @if (adminNotification()->total() > 3)
                          <div class="pd-y-10 tx-center bd-t">
                              <button class="view-more form-control" style="width: 30% ; display: table;margin: 0 auto;">
                                  المزيد
                              </button>
                          </div>
                      @endif--}}
                </div>
            </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
        <div class="dropdown">
            <a href="" class="nav-link pd-x-7 pos-relative" data-toggle="dropdown">
                <i class="icon ion-ios-bell-outline tx-24"></i>
                <!-- start: if statement -->
                @if ((count(adminNotification())))
                    <span class="square-8 bg-danger pos-absolute t-15 r-5 rounded-circle"></span>
            @endif
            <!-- end: if statement -->
            </a>
            <div class="dropdown-menu dropdown-menu-header wd-300 pd-0-force notifications">
                <div class="d-flex align-items-center justify-content-between pd-y-10 pd-x-20 bd-b bd-gray-200">
                    <label class="tx-12 tx-info tx-uppercase tx-semibold tx-spacing-2 mg-b-0">الإشعارات</label>
                    @if (count(adminNotification()))
                        <a href="{{route('admin.dashboard.read.notification')}}"><label
                                    class="tx-12 tx-info tx-uppercase tx-semibold tx-spacing-2 mg-b-0">قرأه
                                الكل</label></a>
                    @endif

                </div><!-- d-flex -->
                <div style="overflow:auto; height: 210px">
                    <div class="media-list infinite-scroll">
                    @if ((count(adminNotification())))
                        @foreach(adminNotification() as $notification)
                            <!-- loop starts here -->
                                <a href="{{route('admin.dashboard.read.notification',['id'=>$notification->id])}}"
                                   class="media-list-link read notification">
                                    <div class="media pd-x-20 pd-y-15">
                                        <div class="media-body">
                                            <p class="tx-13 mg-b-0 tx-gray-700"> {{$notification->data['message']}}
                                                &nbsp; #{{$notification->data['order_id']}}</p>
                                            <span class="tx-12">{{$notification->created_at->diffForHumans()}}</span>
                                        </div>
                                    </div><!-- media -->
                                </a>
                                <!-- loop ends here -->
                            @endforeach
                        @else
                            <div class="pd-y-10 tx-center bd-t">
                                <a href="#" class="tx-12"> لا يوجد إشعارات جديده</a>
                            </div>
                        @endif
                        {{--                        {{adminNotification()->links()}}--}}
                    </div><!-- media-list -->
                    {{--  @if (adminNotification()->total() > 3)
                          <div class="pd-y-10 tx-center bd-t">
                              <button class="view-more form-control" style="width: 30% ; display: table;margin: 0 auto;">
                                  المزيد
                              </button>
                          </div>
                      @endif--}}
                </div>
            </div><!-- dropdown-menu -->
        </div><!-- dropdown -->

        <nav class="nav">
            <div class="dropdown">
                <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name hidden-md-down">
                        {{ auth()->guard('admin')->user()->name }}
                    </span>
                    <img src="{{auth()->guard('admin')->user()->img ? asset('uploads/profile/' . auth()->guard('admin')->user()->img) : 'http://via.placeholder.com/64x64'}}  "
                         class="wd-32 rounded-circle" alt="">
                    <span class="square-10 bg-success"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-200">
                    <ul class="list-unstyled user-profile-nav">
                        <li><a href="{{route('admin.profile.index')}}"><i class="icon ion-ios-person"></i> الصفحه
                                الشخصيه</a>
                        </li>
                        <li><a href="{{ route('admin.auth.logout') }}"><i class="icon ion-power"></i> تسجيل الخروج</a>
                        </li>
                    </ul>
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </nav>

        {{--         <div class="navicon-right">--}}
        {{--            <a id="btnRightMenu" href="" class="pos-relative">--}}
        {{--                <i class="icon ion-ios-chatboxes-outline"></i>--}}
        {{--                <!-- start: if statement -->--}}
        {{--                <span class="square-8 bg-danger pos-absolute t-10 r--5 rounded-circle"></span>--}}
        {{--                <!-- end: if statement -->--}}
        {{--            </a>--}}
        {{--        </div><!-- navicon-right -->--}}
    </div><!-- br-header-right -->
</div><!-- br-header -->
<!-- ########## END: HEAD PANEL ########## -->
