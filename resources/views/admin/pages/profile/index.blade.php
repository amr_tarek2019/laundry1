@extends('admin.layouts.master')

@section('title')

    تفاصيل المشرف

@stop

@section('content')

    <div class="br-profile-page">
        <div class="card shadow-base bd-0 rounded-0 widget-4">
            <div class="card-header ht-75">
                <div class="hidden-xs-down"></div>
                <div class="tx-24 hidden-xs-down"></div>
            </div><!-- card-header -->
            <div class="card-body">
                <div class="card-profile-img">
                    @if ($admin->img)
                        <img src="{{asset('uploads/profile/'.$admin->img) }}" alt="">
                    @endif
                </div><!-- card-profile-img -->
                <h4 class="tx-normal tx-roboto tx-white">{{$admin->name}}</h4>
                <p class="mg-b-25">{{$admin->email}}</p>
            </div><!-- card-body -->
        </div><!-- card -->

        <div class="ht-70 bg-gray-100 pd-x-20 d-flex align-items-center justify-content-center shadow-base">
            <ul class="nav nav-outline active-info align-items-center flex-row" role="tablist">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#mainuserdata" role="tab">
                        بيانات المشرف</a></li>
            </ul>
        </div>
        <div style="padding-top: 15px;">

            @include('errors.custom-messages')
            @include('errors.validation-errors')
        </div>
        <div class="tab-content br-profile-body">
            <div class="tab-pane fade active show" id="mainuserdata">
                <div class="row">
                    <div class="col-lg-12 mg-t-30 mg-lg-t-0">
                        <div class="card pd-20 pd-xs-30 shadow-base bd-0">
                            <h6 class="tx-gray-800 tx-uppercase tx-semibold tx-13 mg-b-25">معلومات التواصل</h6>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">الإسم</label>
                            <p class="tx-info mg-b-25">{{$admin->name}}</p>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">رقم الهاتف</label>
                            <p class="tx-info mg-b-25">{{$admin->phone}}</p>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">البريد
                                الالكترونى</label>
                            <p class="tx-inverse mg-b-25">{{$admin->email}}</p>
                            <div class="row col-sm-12">
                                <div class="col-sm-3 mg-t-20 mg-sm-t-0 ">
                                    <a href="" class="btn btn-info btn-with-icon btn-block" data-toggle="modal"
                                       data-target="#modaldemo">
                                        <div class="ht-40 ">
                            <span class="icon wd-40">
                                <i class="fa fa-pencil"></i></span>
                                            <span class="pd-x-15">تعديل البيانات</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-3 mg-t-20 mg-sm-t-0 ">
                                    <a href="" class="btn btn-danger btn-with-icon btn-block" data-toggle="modal"
                                       data-target="#modaldemopassword">
                                        <div class="ht-40 ">
                            <span class="icon wd-40">
                                <i class="fa fa-pencil"></i></span>
                                            <span class="pd-x-15">تعديل كلمه المرور</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div><!-- card -->
                    </div><!-- col-lg-4 -->

                </div><!-- row -->
            </div><!-- tab-pane -->
        </div><!-- br-pagebody -->

    </div>
    <div id="modaldemo" class="modal fade">
        <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">تعديل البيانات</h6>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="edit" action="{{route('admin.profile.update.info')}}" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="modal-body pd-20">
                        <div class="row mg-b-30">
                            <div class="col-lg-6 mg-b-30 ">
                                <input class="form-control" required placeholder="الإسم" type="text" name="name"
                                       data-parsley-required-message="برجاء إدخال الإسم" value="{{$admin->name}}">
                                @include('errors.validation-messages', ['field' => 'name'])
                            </div><!-- col -->
                            <div class="col-lg-6 mg-b-30 ">
                                <input class="form-control" required placeholder="رقم الهاتف" type="text" name="phone"
                                       data-parsley-required-message="برجاء إدخال رقم الهاتف" value="{{$admin->phone}}">
                                @include('errors.validation-messages', ['field' => 'phone'])
                            </div><!-- col -->
                            <div class="col-lg-6  ">
                                <input class="form-control" required placeholder="البريد الالكترونى" type="email"
                                       name="email"
                                       data-parsley-required-message="برجاء إدخال البريد الالكترونى"
                                       value="{{$admin->email}}">
                                @include('errors.validation-messages', ['field' => 'email'])
                            </div><!-- col -->
                            <div class="col-lg-6 " style="text-align: center">
                                <div class="wrap-custom-file">
                                    <input type="file" name="img" id="image1" accept=".gif, .jpg, .png"
                                    />
                                    <label for="image1" class="file-ok"
                                           style="background-image: url({{asset('uploads/profile/'.$admin->img)}});">
                                        <span>الصوره</span>
                                    </label>
                                </div>
                                @include('errors.validation-messages', ['field' => 'img'])
                            </div><!-- col -->
                        </div>
                    </div><!-- modal-body -->
                    <div class="modal-footer" style="direction: rtl">
                        <button type="submit" class="btn btn-primary tx-size-xs"
                        >حفظ
                        </button>
                        <button type="button" class="btn btn-secondary tx-size-xs"
                                data-dismiss="modal">غلق
                        </button>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    <div id="modaldemopassword" class="modal fade">
        <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">تعديل كلمه المرور</h6>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="edit2" action="{{route('admin.profile.update.password')}}" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="modal-body pd-20">
                        <div class="row ">
                            <div class="col-lg-12 mg-b-30 ">
                                <input class="form-control" required placeholder="كلمه المرور القديمه" type="password"
                                       name="old_password"
                                       data-parsley-required-message="برجاء إدخال كلمه المرور القديمه">
                                @include('errors.validation-messages', ['field' => 'old_password'])
                            </div><!-- col -->
                            <div class="col-lg-6 mg-b-30 ">
                                <input class="form-control" required placeholder="كلمه المرور الجديده" type="password"
                                       name="new_password"
                                       data-parsley-required-message="برجاء إدخال كلمه المرور الجديده">
                                @include('errors.validation-messages', ['field' => 'new_password'])
                            </div><!-- col -->
                            <div class="col-lg-6  ">
                                <input class="form-control" required placeholder="تأكيد كلمه المرور الجديده"
                                       type="password" name="new_password_confirmation"
                                       data-parsley-required-message="برجاء إدخال تأكيد كلمه المرور الجديده">
                                @include('errors.validation-messages', ['field' => 'new_password_confirmation'])
                            </div><!-- col -->

                        </div>
                    </div><!-- modal-body -->
                    <div class="modal-footer" style="direction: rtl">
                        <button type="submit" class="btn btn-primary tx-size-xs"
                        >حفظ
                        </button>
                        <button type="button" class="btn btn-secondary tx-size-xs"
                                data-dismiss="modal">غلق
                        </button>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
@stop

@section('scripts')
    <script>
        $('#edit').parsley();
        $('#edit2').parsley();
    </script>
@stop