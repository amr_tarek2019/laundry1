@extends('admin.layouts.master')

@section('title')

    الإعدادات

@stop
@section('content')
        <div class="br-pageheader pd-y-15 pd-l-20">
            <nav class="breadcrumb pd-0 mg-0 tx-12">
                <a class="breadcrumb-item" href="{{route('admin.dashboard.index')}}">الرئيسيه</a>
                <span class="breadcrumb-item active"> تعديل بيانات الاعدادات العامه </span>
            </nav>
        </div><!-- br-pageheader -->
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30 mg-b-20">
            <h4 class="tx-gray-800 mg-b-5">تعديل بيانات  الاعدادات العامه  </h4>
            <p class="mg-b-0">يمكنك تعديل بيانات الاعدادات العامه من هنا </p>
        </div>


        @include('errors.custom-messages')
        @include('errors.validation-errors')
        <div class="br-pagebody">
            <div class="br-section-wrapper">
                <div class="col-md-2 mg-b-15">
                    <h4 class="tx-gray-800 mg-b-5">ادخل البيانات  </h4>

                </div>
                <div class="table-wrapper">
                    <form id="edit" action="{{route('admin.settings.update')}}" method="post" enctype="multipart/form-data">
                         @csrf
                        @method('PATCH')
                        <div class="row mg-b-30" >
                            <div class="col-lg-6 mg-b-30">
                                <label for=""> عن التطبيق</label>
                                <textarea class="form-control" required placeholder="عن التطبيق " type="text" name="about" rows="10"
                                          data-parsley-required-message="برجاء إدخال عن التطبيق"
                                >{{$settings->about}}</textarea>
                            </div><!-- col -->
                            <div class="col-lg-6 mg-b-30">
                                <label for=""> about app</label>
                                <textarea class="form-control" required placeholder="about app" type="text" name="about_en" rows="10"
                                          data-parsley-required-message="please enter about app"
                                >{{$settings->about_en}}</textarea>
                            </div><!-- col -->
                            <div class="col-lg-6 mg-b-30">
                                <label for="">الشروط و الاحكام</label>
                                <textarea rows="10" class="form-control" required placeholder="الشروط و الاحكام " type="text" name="terms"
                                          data-parsley-required-message="برجاء إدخال الشروط و الاحكام"
                                >{{$settings->terms}}</textarea>
                            </div><!-- col -->
                            <div class="col-lg-6 mg-b-30">
                                <label for="">terms and condition</label>
                                <textarea rows="10" class="form-control" required placeholder="terms and condition " type="text" name="terms_en"
                                          data-parsley-required-message="please enter terms and condition"
                                >{{$settings->terms_en}}</textarea>
                            </div><!-- col -->
                        </div>


                        <div class="form-layout-footer">
                            <button class="btn btn-info">تعديل</button>
                        </div>

                        
                        
                    </form>
                </div><!-- table-wrapper -->

                
            </div><!-- br-section-wrapper -->
        </div><!-- br-pagebody -->

@endsection

@section('scripts')
    <script>
        $('#edit').parsley();

    </script>
@stop
