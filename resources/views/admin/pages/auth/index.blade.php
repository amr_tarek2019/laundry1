<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{ asset('css/admin.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <title>تسجيل الدخول</title>

</head>

<body>
<div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">
    <form action="{{route('admin.auth.login')}}" method="post" id="login" style="direction: rtl"
          class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
        @csrf
        <div class="signin-logo tx-center tx-28 tx-bold tx-inverse"><span class="tx-normal">[</span> Laundry <span
                    class="tx-normal">]</span></div>
        <div class="tx-center mg-b-60">Easy Dry & Cleaning Services</div>
        <div class="form-group">
            <input type="text" name="email" class="form-control" placeholder="إسم المستخدم أو البريد الإلكترونى " required
                   data-parsley-required-message="برجاء إدخال إسم المستخدم أو البريد الإلكترونى">
            @include('errors.validation-messages', ['field' => 'email'])
        </div><!-- form-group -->
        <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="كلمه المرور" required
                   data-parsley-required-message="برجاء إدخال كلمه المرور">
            @include('errors.validation-messages', ['field' => 'password'])
        </div><!-- form-group -->
        <button type="submit" class="btn btn-info btn-block">تسجيل الدخول</button>
        @include('errors.validation-errors')
        <div class="mg-t-60 tx-center">
            <a target="_blank" href="http://2grand.net/"> <img src="{{asset('/android.png')}}" class="grand"> </a>

        </div>
    </form><!-- login-wrapper -->
</div><!-- d-flex -->
<script type="text/javascript" src="{{asset('js/admin.js.js')}}"></script>
<script>
    $('#login').parsley();
</script>
<script>
    setTimeout(fade_out, 5000);

    function fade_out() {
        $("#validation").fadeOut().empty();
    }
</script>
</body>
</html>
