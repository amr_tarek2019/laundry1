@extends('admin.layouts.master')

@section('title')
    المشرفين
@stop


@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{route('admin.dashboard.index')}}">الرئيسيه</a>
            <span class="breadcrumb-item active">المشرفين</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">جدول المشرفين</h4>
        <p class="mg-b-0">عرض كل المشرفين </p>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            @can('add_admins')
                <div class="col-md-6">
                    <div class="col-md-5 mg-b-15">
                        <a href="" class="btn btn-success btn-with-icon btn-block" data-toggle="modal"
                           data-target="#modaldemo">
                            <div class="ht-40 justify-content-between">
                            <span class="icon wd-40">
                                <i class="fa fa-plus"></i></span>
                                <span class="pd-x-15">اضافه مشرف</span>
                            </div>
                        </a>
                    </div>
                </div>
            @endcan
            @include('errors.custom-messages')
            @include('errors.validation-errors')
            <div class="col-md-6"></div>
            <div class="table-wrapper">
                <table id="datatable1" class="display  nowrap" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="wd-15p all"> الإسم</th>
                        <th class="wd-15p all">البريد الإلكترونى</th>
                        <th class="wd-15p all">رقم الهاتف</th>
                        @can('edit_admins')
                            <th class="wd-15p all">الصلاحيات</th>
                        @endcan
                        @canany(['edit_admins','delete_admins'])
                            <th class="wd-15p all">اللإجراء</th>
                        @endcanany
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($admins as  $admin)
                        <tr id="{{$admin->id}}">
                            <td>{{$admin->name}}</td>
                            <td>{{$admin->email}}</td>
                            <td>{{$admin->phone}}</td>
                            @can('edit_admins')
                                <td>
                                    <a href="{{route('admin.admins.admin.details',$admin->id)}}"
                                       class="btn btn-info btn-icon mg-r-5 mg-b-10">
                                        <div><i class="fa fa-arrow-left"></i></div>
                                    </a>
                                </td>
                            @endcan
                            @canany(['edit_admins','delete_admins'])
                            <td>
                                @can('edit_admins')
                                    <a href="#" data-toggle="modal"
                                       data-target="#modaldemooo{{$admin->id}}"
                                       class="btn btn-success btn-icon mg-r-5 mg-b-10">
                                        <div><i class="fa fa-pencil"></i></div>
                                    </a>
                                @endcan
                                @can('delete_admins')
                                    <a href="#"
                                       onclick="showDeletConfirmationModal('{{route('admin.admins.delete', $admin->id)}}'  , {{$admin->id}})"
                                       class="btn btn-danger btn-icon mg-r-5 mg-b-10">
                                        <div><i class="fa fa-trash-o"></i></div>
                                    </a>
                                @endcan
                            </td>
                                @endcan
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->

        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->
    @can('add_admins')
        <div id="modaldemo" class="modal fade">
            <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">اضافه مشرف</h6>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="addAdmin" action="{{route('admin.admins.add')}}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body pd-20">
                            <div class="row mg-b-30">
                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <input class="form-control" required placeholder="الإسم" type="text"
                                           value="{{old('name')}}" name="name"
                                           data-parsley-required-message="برجاء إدخال الإسم">
                                    @include('errors.validation-messages', ['field' => 'name'])
                                </div><!-- col-4 -->

                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <input class="form-control" required placeholder="البريد الإلكترونى"
                                           value="{{old('email')}}" type="text"
                                           name="email" data-parsley-type="email"
                                           data-parsley-required-message="برجاء إدخال البريد الإلكترونى"
                                           data-parsley-type-message="برجاء إدخال بريد الإلكترونى صحيح"
                                    >
                                    @include('errors.validation-messages', ['field' => 'name'])
                                </div>
                            </div>
                            <div class="row mg-b-30">
                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <input class="form-control" required placeholder="رقم الهاتف"
                                           value="{{old('phone')}}"
                                           type="text" name="phone"
                                           data-parsley-required-message="برجاء إدخال رقم الهاتف">
                                    @include('errors.validation-messages', ['field' => 'phone'])
                                </div><!-- col-4 -->

                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <input class="form-control" required placeholder="كلمه المرور" type="password"
                                           name="password"
                                           data-parsley-required-message="برجاء إدخال كلمه المرور">
                                    @include('errors.validation-messages', ['field' => 'password'])
                                </div>
                            </div>
                            <!-- modal-body -->
                            <div class="modal-footer" style="direction: rtl">
                                <button type="submit" class="btn btn-primary tx-size-xs"
                                >حفظ
                                </button>
                                <button type="button" class="btn btn-secondary tx-size-xs"
                                        data-dismiss="modal">غلق
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- modal-dialog -->
        </div><!-- modal -->
    @endcan
    @foreach($admins as  $admin)
        <div id="modaldemooo{{$admin->id}}" class="modal fade">
            <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">تعديل بنك</h6>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="editAdmin" action="{{route('admin.admins.edit',['id'=>$admin->id])}}" method="post"
                          enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf
                        <div class="modal-body pd-20">
                            <div class="row mg-b-30">
                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <input class="form-control" required placeholder="الإسم" type="text"
                                           value="{{$admin->name}}" name="name"
                                           data-parsley-required-message="برجاء إدخال الإسم">
                                    @include('errors.validation-messages', ['field' => 'name'])
                                </div><!-- col-4 -->

                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <input class="form-control" required placeholder="البريد الإلكترونى"
                                           value="{{$admin->email}}" type="text"
                                           name="email" data-parsley-type="email"
                                           data-parsley-required-message="برجاء إدخال البريد الإلكترونى"
                                           data-parsley-type-message="برجاء إدخال بريد الإلكترونى صحيح"
                                    >
                                    @include('errors.validation-messages', ['field' => 'name'])
                                </div>
                            </div>
                            <div class="row mg-b-30">
                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <input class="form-control" required placeholder="رقم الهاتف"
                                           value="{{$admin->phone}}"
                                           type="text" name="phone"
                                           data-parsley-required-message="برجاء إدخال رقم الهاتف">
                                    @include('errors.validation-messages', ['field' => 'phone'])
                                </div><!-- col-4 -->

                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <input class="form-control"  placeholder="كلمه المرور" type="password"
                                           name="password"
                                           >
                                    @include('errors.validation-messages', ['field' => 'password'])
                                </div>
                            </div>
                            <!-- modal-body -->
                            <div class="modal-footer" style="direction: rtl">
                                <button type="submit" class="btn btn-primary tx-size-xs"
                                >حفظ
                                </button>
                                <button type="button" class="btn btn-secondary tx-size-xs"
                                        data-dismiss="modal">غلق
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- modal-dialog -->
        </div><!-- modal -->

    @endforeach
@endsection
@section('scripts')
    <script>
        $('#addAdmin').parsley();
        $('#editAdmin').parsley();
    </script>
@stop