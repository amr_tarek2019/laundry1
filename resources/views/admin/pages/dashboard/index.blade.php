@extends('admin.layouts.master')

@section('title')

    الرئيسيه

@stop

@section('content')
    <div class="br-pagebody mg-t-5 pd-x-30">
        <div class="row row-sm">
            <div class="col-sm-6 col-xl-3">
                <div class="bg-teal rounded overflow-hidden">
                    <div class="pd-25 d-flex align-items-center">
                        <i class="ion ion-ios-people tx-60 lh-0 tx-white op-7"></i>
                        <div class="mg-l-20">
                            <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">
                                المستخدمين</p>
                            <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{$users}}</p>
                        </div>
                    </div>
                </div>
            </div><!-- col-3 -->

            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="bg-primary rounded overflow-hidden">
                    <div class="pd-25 d-flex align-items-center">
                        <i class="ion ion-bag tx-60 lh-0 tx-white op-7"></i>
                        <div class="mg-l-20">
                            <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">الطلبات
                            </p>
                            <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{$order_count}}</p>
                        </div>
                    </div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="bg-br-primary rounded overflow-hidden">
                    <div class="pd-25 d-flex align-items-center">
                        <i class="ion ion-ios-person-outline tx-60 lh-0 tx-white op-7"></i>
                        <div class="mg-l-20">
                            <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">
                                المغاسل</p>
                            <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{$laundries_count}}</p>
                        </div>
                    </div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="bg-br-primary rounded overflow-hidden">
                    <div class="pd-25 d-flex align-items-center">
                        <i class="ion ion-ios-people-outline tx-60 lh-0 tx-white op-7"></i>
                        <div class="mg-l-20">
                            <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">
                                المندوبين</p>
                            <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{$delegates_count}}</p>
                        </div>
                    </div>
                </div>
            </div><!-- col-3 -->
        </div><!-- row -->

        <div class="row row-sm mg-t-20">
            <div class="col-xl-6">
                <div>
                    <h6 class="tx-13 tx-uppercase tx-inverse tx-semibold tx-spacing-1">إحصائيات المستخدمين</h6>
                </div>
                <div class="bd pd-t-30 pd-b-20 pd-x-20">
                    <canvas id="userschart" height="200"></canvas>
                </div>
            </div><!-- col-6 -->


            <div class="col-xl-6">
                <div>
                    <h6 class="tx-13 tx-uppercase tx-inverse tx-semibold tx-spacing-1">إحصائيات الطلبات</h6>
                </div>
                <div class="bd pd-t-30 pd-b-20 pd-x-20">
                    <canvas id="orderschart" height="200"></canvas>
                </div>
            </div><!-- col-6 -->
        </div><!-- row -->

    </div><!-- br-pagebody -->
    @can('show_orders')
        <div class="col-12">
            <div class="card bd-0 shadow-base pd-30 mg-t-20">
                <div class="d-flex align-items-center justify-content-between mg-b-30">
                    <div>
                        <h6 class="tx-13 tx-uppercase tx-inverse tx-semibold tx-spacing-1">اخر 10 طلبات </h6>
                    </div>
                </div><!-- d-flex -->

                <div class="table-wrapper">
                    <table class="table display responsive nowrap">
                        <thead>
                        <tr>
                            <th class="wd-15p all">رقم الطلب</th>
                            <th class="wd-15p all print">تاريخ الطلب</th>
                            <th class="wd-15p all ">حاله الطلب</th>
                            <th class="wd-15p all print">المدينه</th>
                            <th class="wd-15p all print">الإجمالى</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as  $order)
                            <tr id="{{$order->id}}">
                                <td>
                                    <a href="{{route('admin.orders.index')}}"> {{$order->id}} </a>
                                </td>
                                <td>{{$order->created_at}}</td>
                                <td>
                                    @if ($order->order_status == 0)
                                        طلب جديد
                                    @elseif ($order->order_status == 1)
                                        إلى الطريق للمغسله
                                    @elseif ($order->order_status == 2)
                                        تم التسليم للمغسله
                                    @elseif ($order->order_status == 3)
                                        إلى الطريق للعميل
                                    @elseif ($order->order_status == 4)
                                        مكتمل
                                    @elseif ($order->order_status == 5)
                                        ملغى
                                    @endif
                                </td>
                                <td>{{$order->city }}</td>
                                <td>{{$order->totalPrice()}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-wrapper -->

            </div><!-- card -->
        </div>
    @endcan
    @can('show_suggestions')
        <div class="col-12">
            <div class="card bd-0 shadow-base pd-30 mg-t-20">
                <div class="d-flex align-items-center justify-content-between mg-b-30">
                    <div>
                        <h6 class="tx-13 tx-uppercase tx-inverse tx-semibold tx-spacing-1">اخر 5 رسائل </h6>
                    </div>
                </div><!-- d-flex -->

                <div class="table-wrapper">
                    <table class="table display responsive nowrap">
                        <thead>
                        <tr>
                            <th class="wd-15p all">المستخدم</th>
                            <th class="wd-15p all">العنوان</th>
                            <th class="wd-15p all">التفاصيل</th>
                            @can('delete_suggestions')
                                <th class="wd-15p all">الإجراء</th>
                            @endcan
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($suggestions as  $suggestion)
                            <tr id="{{$suggestion->id}}">
                                <td>
                                    <a href="{{route('admin.users.user.details',$suggestion->user->id)}}">{{$suggestion->user->phone}} </a>
                                </td>
                                <td>{{$suggestion->title}}</td>
                                <td><a href=""
                                       class="btn btn-info tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium"
                                       data-toggle="modal" data-target="#modaldemooozz{{$suggestion->id}}">عرض
                                    </a></td>
                                @can('delete_suggestions')
                                    <td>
                                        <a href="#"
                                           onclick="showDeletConfirmationModal('{{route('admin.suggestions.suggestion.delete', $suggestion->id)}}'  , {{$suggestion->id}})"
                                           class="btn btn-danger btn-icon mg-r-5 mg-b-10">
                                            <div><i class="fa fa-trash-o"></i></div>
                                        </a>
                                    </td>
                                @endcan
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-wrapper -->
            @foreach($suggestions as  $suggestion)
                <!-- LARGE MODAL -->
                    <div id="modaldemooozz{{$suggestion->id}}" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                            <div class="modal-content tx-size-sm">
                                <div class="modal-header pd-x-20">
                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">عرض التفاصيل</h6>
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body pd-20">
                                    <input class="form-control" placeholder="{{$suggestion->details}}"
                                           type="text"
                                           readonly>
                                </div><!-- modal-body -->
                                <div class="modal-footer" style="direction: rtl">
                                    <button type="button" class="btn btn-secondary tx-size-xs"
                                            data-dismiss="modal">غلق
                                    </button>
                                </div>
                            </div>
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                @endforeach
            </div><!-- card -->
        </div>
    @endcan

@stop

@section('scripts')
    <script src="{{asset('cpanel/lib/chart.js/Chart.js')}}"></script>
    <script>
        /** PIE CHART **/
        var randomScalingFactor = function () {
            return Math.round(Math.random() * 100);
        };

        var datapie = {
            labels: [
                'أخر 7 أيام',

            ],
            datasets: [{
                data: [
                    {{$users_last_7_days }},


                ],
                backgroundColor: [
                    '#1D2939',
                    '#F57E2E',

                ]
            }]
        };

        var optionpie = {
            responsive: true,
            legend: {
                display: false,
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        };

        // For a doughnut chart
        // For a pie chart
        var ctx7 = document.getElementById('userschart');
        var myPieChart7 = new Chart(ctx7, {
            type: 'pie',
            data: datapie,
            options: optionpie
        });
    </script>
    <script>
        /** PIE CHART **/
        var randomScalingFactor = function () {
            return Math.round(Math.random() * 100);
        };

        var datapie = {
            labels: [
                'أخر 7 أيام ',
            ],
            datasets: [{
                data: [
                    {{$orders_last_7_days }},

                ],
                backgroundColor: [
                    '#1D2939',
                    '#F57E2E',

                ]
            }]
        };

        var optionpie = {
            responsive: true,
            legend: {
                display: false,
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        };

        // For a doughnut chart
        // For a pie chart
        var ctx7 = document.getElementById('orderschart');
        var myPieChart7 = new Chart(ctx7, {
            type: 'pie',
            data: datapie,
            options: optionpie
        });
    </script>
@stop