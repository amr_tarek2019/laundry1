@extends('admin.layouts.master')

@section('title')

    المندوبين

@stop

@section('content')

    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{route('admin.dashboard.index')}}">الرئيسيه</a>
            {{--                <a class="breadcrumb-item" href="{{route('user.showindex')}}">الفئات</a>--}}
            <span class="breadcrumb-item active">المندوبين</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">جدول المندوبين </h4>
        <p class="mg-b-0">عرض كل المندوبين </p>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            @can('add_delegates')
                <div class="col-md-4">
                    <div class="col-md-5 mg-b-15">
                        <a href="" class="btn btn-success btn-with-icon btn-block" data-toggle="modal"
                           data-target="#modaldemo">
                            <div class="ht-40 justify-content-between">
                            <span class="icon wd-40">
                                <i class="fa fa-plus"></i></span>
                                <span class="pd-x-15">اضافه </span>
                            </div>
                        </a>
                    </div>
                </div>
            @endcan
            @include('errors.custom-messages')
            @include('errors.validation-errors')
            <div class="table-wrapper">
                <table id="datatable1" class=" display  nowrap" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="wd-15p all">الاسم</th>
                        <th class="wd-15p all">الهاتف</th>
                        <th class="wd-15p all">البريد الالكترونى</th>
                        <th class="wd-15p all">الحاله</th>
                        <th class="wd-15p all">الطلبات المكتمله</th>
                        <th class="wd-15p all">مستحقات التطبيق</th>
                        @can('edit_delegates')
                            <th class="wd-15p all">تصفير المستحقات</th>
                        @endcan
                        <th class="wd-15p all">إثبات الهويه</th>
                        @canany(['edit_delegates','delete_delegates'])
                            <th class="wd-20p all">المزيد</th>
                        @endcan
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as  $user)
                        <tr id="{{$user->id}}">
                            <td>{{$user->name}}</td>
                            <td>{{$user->phone}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->is_active ? 'نشط' : 'معلق'}}</td>
                            <td>{{$user->completedOrders()}} </td>

                            <td>{{$user->delegateFees()}} </td>
                            @can('edit_delegates')
                                <td>
                                    <form action="{{route('admin.delegates.fees.reset' , $user->id)}}" method="post">
                                        @csrf
                                        <button type="submit" style=" width: 40px"
                                                class="btn btn-outline-success btn-icon mg-r-5 mg-b-10">
                                            <i class="ion-ios-refresh-empty" style="font-size: 25px ;"></i>
                                        </button>
                                    </form>
                                </td>
                            @endcan
                            <td>
                                <form action="{{route('admin.delegates.identity.download' , $user->id)}}" method="post">
                                    @csrf
                                    <button type="submit" style="padding: 9px;"
                                            class="btn btn-warning btn-icon mg-r-5 mg-b-10">
                                        <i class="fa fa-cloud-download"></i>
                                    </button>
                                </form>
                            </td>
                            @canany(['edit_delegates','delete_delegates'])
                                <td>
                                    @can('edit_delegates')
                                        <a href="" data-toggle="modal"
                                           data-target="#modaldemoo{{$user->id}}"
                                           class="btn btn-info btn-icon mg-r-5 mg-b-10">
                                            <div><i class="fa fa-edit"></i></div>
                                        </a>
                                    @endcan
                                    @can('show_delegates')
                                        <a href="{{route('admin.delegates.user.details' , $user->id)}}"
                                           class="btn btn-success btn-icon mg-r-5 mg-b-10">
                                            <div><i class="fa fa-arrow-left"></i></div>
                                        </a>
                                    @endcan
                                    @can('delete_delegates')
                                        <a href="#"
                                           onclick="showDeletConfirmationModal('{{route('admin.delegates.user.delete', $user->id)}}'  , {{$user->id}})"
                                           class="btn btn-danger btn-icon mg-r-5 mg-b-10">
                                            <div><i class="fa fa-trash-o"></i></div>
                                        </a>
                                    @endcan
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->


        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->
    @can('add_delegates')
        <div id="modaldemo" class="modal fade">
            <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">اضافه مندوب</h6>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="adddelegate" action="{{route('admin.delegates.add.delegate')}}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body pd-20">
                            <div class="row mg-b-30">
                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <input class="form-control" required placeholder="الإسم" type="text" name="name"
                                           data-parsley-required-message="برجاء إدخال الإسم">
                                    @include('errors.validation-messages', ['field' => 'name'])
                                </div><!-- col-4 -->
                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <input class="form-control" required placeholder="البريد الإلكترونى" type="email"
                                           name="email"
                                           data-parsley-required-message="برجاء إدخال البريد الإلكترونى"
                                           data-parsley-type-message="برجاء إدخال بريد الإلكترونى صحيح">
                                    @include('errors.validation-messages', ['field' => 'email'])
                                </div><!-- col-4 -->
                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <input class="form-control" required placeholder="الهاتف" type="number" name="phone"
                                           data-parsley-required-message="برجاء إدخال الهاتف">
                                    @include('errors.validation-messages', ['field' => 'phone'])
                                </div><!-- col-4 -->
                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <input class="form-control" required placeholder="كلمه المرور" type="password"
                                           name="password"
                                           data-parsley-required-message="برجاء إدخال كلمه المرور">
                                    @include('errors.validation-messages', ['field' => 'password'])
                                </div><!-- col-4 -->
                                <div class="col-lg-6 mg-b-30 " style="text-align: center">
                                    <label class="custom-file">
                                        <input type="file" id="file" name="identity_doc" class="custom-file-input"
                                               required
                                               data-parsley-required-message="برجاء إختيار إثبات الهويه">
                                        <span class="custom-file-control">إثبات الهويه</span>
                                    </label>
                                    @include('errors.validation-messages', ['field' => 'identity_doc'])
                                </div><!-- col -->
                            </div>
                        </div><!-- modal-body -->
                        <div class="modal-footer" style="direction: rtl">
                            <button type="submit" class="btn btn-primary tx-size-xs"
                            >حفظ
                            </button>
                            <button type="button" class="btn btn-secondary tx-size-xs"
                                    data-dismiss="modal">غلق
                            </button>
                        </div>
                    </form>
                </div>
            </div><!-- modal-dialog -->
        </div><!-- modal -->
    @endcan
    @can('edit_delegates')
        @foreach($users as $user)
            <div id="modaldemoo{{$user->id}}" class="modal fade">
                <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                    <div class="modal-content tx-size-sm">
                        <div class="modal-header pd-x-20">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">تعديل مندوب</h6>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="editdelegate" action="{{route('admin.delegates.edit.delegate',['id'=>$user->id])}}"
                              method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body pd-20">
                                <div class="row mg-b-30">
                                    <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                        <input class="form-control" required placeholder="الإسم" type="text" name="name"
                                               value="{{$user->name}}"
                                               data-parsley-required-message="برجاء إدخال الإسم">
                                        @include('errors.validation-messages', ['field' => 'name'])
                                    </div><!-- col-4 -->
                                    <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                        <input class="form-control" required placeholder="البريد الإلكترونى"
                                               type="email"
                                               name="email" value="{{$user->email}}"
                                               data-parsley-required-message="برجاء إدخال البريد الإلكترونى"
                                               data-parsley-type-message="برجاء إدخال بريد الإلكترونى صحيح">
                                        @include('errors.validation-messages', ['field' => 'email'])
                                    </div><!-- col-4 -->
                                    <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                        <input class="form-control" required placeholder="الهاتف" type="number"
                                               name="phone"
                                               data-parsley-required-message="برجاء إدخال الهاتف"
                                               value="{{$user->phone}}">
                                        @include('errors.validation-messages', ['field' => 'phone'])
                                    </div><!-- col-4 -->
                                    <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                        <input class="form-control" placeholder="كلمه المرور" type="password"
                                               name="password"
                                        >
                                        @include('errors.validation-messages', ['field' => 'password'])
                                    </div><!-- col-4 -->
                                    <div class="col-lg-6 mg-b-30 " style="text-align: center">
                                        <label class="custom-file">
                                            <input type="file" id="file" name="identity_doc" class="custom-file-input"
                                            >
                                            <span class="custom-file-control">إثبات الهويه</span>
                                        </label>
                                        @include('errors.validation-messages', ['field' => 'identity_doc'])
                                    </div><!-- col -->
                                </div>
                            </div><!-- modal-body -->
                            <div class="modal-footer" style="direction: rtl">
                                <button type="submit" class="btn btn-primary tx-size-xs"
                                >حفظ
                                </button>
                                <button type="button" class="btn btn-secondary tx-size-xs"
                                        data-dismiss="modal">غلق
                                </button>
                            </div>
                        </form>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->
        @endforeach
    @endcan
@stop

@section('scripts')
    <script>
        $('#adddelegate').parsley();
        $('#editdelegate').parsley();
    </script>
@stop