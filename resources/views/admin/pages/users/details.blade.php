@extends('admin.layouts.master')

@section('title')

    تفاصيل المستخدم

@stop
@section('styles')
    <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" media="all" rel="stylesheet"
          type="text/css"/>
    <script type="text/javascript" src="{{asset('cpanel/js/map.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrFDCzH3QzNG94R8W1poCbt3oIVTyxbEI&callback=initMap"
            async defer></script>
    <link href="{{ asset('cpanel/css/bootstrap4-toggle.css')}}" media="all" rel="stylesheet" type="text/css"/>
@stop
@section('content')

    <div class="br-profile-page">
        <div class="card shadow-base bd-0 rounded-0 widget-4">
            <div class="card-header ht-75">
                <div class="hidden-xs-down"></div>
                <div class="tx-24 hidden-xs-down"></div>
            </div><!-- card-header -->
            <div class="card-body">
                <div class="card-profile-img">
                    @if ($user->img)
                        <img src="{{asset('uploads/profile/') .$user->img}}" alt="">
                    @endif
                </div><!-- card-profile-img -->
                <h4 class="tx-normal tx-roboto tx-white">{{$user->name}}</h4>
                <p class="mg-b-25">{{$user->email}}</p>
            </div><!-- card-body -->
        </div><!-- card -->

        <div class="ht-70 bg-gray-100 pd-x-20 d-flex align-items-center justify-content-center shadow-base">
            <ul class="nav nav-outline active-info align-items-center flex-row" role="tablist">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#mainuserdata" role="tab">
                        بيانات المستخدم</a></li>
                @can('show_suggestions')
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#suggestions"
                                            role="tab">الاقتراحات</a>
                    </li>
                @endcan
                @can('show_orders')
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#orders" role="tab">الطلبات</a>
                    </li>
                @endcan
            </ul>
        </div>

        <div class="tab-content br-profile-body">
            <div class="tab-pane fade active show" id="mainuserdata">
                <div class="row">
                    <div class="col-lg-12 mg-t-30 mg-lg-t-0">
                        <div class="card pd-20 pd-xs-30 shadow-base bd-0">
                            <h6 class="tx-gray-800 tx-uppercase tx-semibold tx-13 mg-b-25">معلومات التواصل</h6>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">الإسم</label>
                            <p class="tx-info mg-b-25">{{$user->name}}</p>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">رقم الهاتف</label>
                            <p class="tx-info mg-b-25">{{$user->phone}}</p>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">البريد
                                الالكترونى</label>
                            <p class="tx-inverse mg-b-25">{{$user->email}}</p>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">النوع</label>
                            <p class="tx-info mg-b-25">{{$user->type == 0 ? 'مستخدم' : 'بائع'}}</p>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">الحاله</label>
                            <div class="col-sm-2" style="padding: 0 !important;">
                                <input id="status" type="checkbox" uid="{{$user->id}}" class="role" data-toggle="toggle"
                                       data-on="مفعل"
                                       name="is_active"
                                       data-off="معلق" data-onstyle="success"
                                       data-offstyle="danger"
                                       @if ($user->is_active)
                                       checked
                                        @endif
                                >
                            </div>
                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2"
                                   style="padding-top: 15px">العنوان</label>
                            <div style="height: 200px" id="map"></div>
                        </div><!-- card -->
                    </div><!-- col-lg-4 -->

                </div><!-- row -->
            </div><!-- tab-pane -->
            @can('show_suggestions')
                <div class="tab-pane fade  show" id="suggestions">
                    <div class="br-pagebody">
                        <div class="br-section-wrapper">
                            <div class="table-wrapper">
                                <table id="datatable1" class=" display  nowrap" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th class="wd-15p all">العنوان</th>
                                        <th class="wd-15p all">التفاصيل</th>
                                        {{--                                        @can('delete_suggestions')--}}
                                        <th class="wd-15p all">الإجراء</th>
                                        {{--                                        @endcan--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (count($user->suggestions))
                                        @foreach($user->suggestions as  $suggestion)
                                            <tr id="{{$suggestion->id}}">
                                                <td>{{$suggestion->title}}</td>
                                                <td><a href=""
                                                       class="btn btn-info tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium"
                                                       data-toggle="modal"
                                                       data-target="#modaldemooozz{{$suggestion->id}}">عرض
                                                    </a></td>
                                                {{--                                                @can('delete_suggestions')--}}
                                                <td>
                                                    <a href="#"
                                                       onclick="showDeletConfirmationModal('{{route('admin.users.suggestion.delete', $suggestion->id)}}'  , {{$user->id}})"
                                                       class="btn btn-danger btn-icon mg-r-5 mg-b-10">
                                                        <div><i class="fa fa-trash-o"></i></div>
                                                    </a>
                                                </td>
                                                {{--                                                @endcan--}}
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- table-wrapper -->
                        @if (count($user->suggestions))
                            @foreach($user->suggestions as  $suggestion)
                                <!-- LARGE MODAL -->
                                    <div id="modaldemooozz{{$suggestion->id}}" class="modal fade">
                                        <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                                            <div class="modal-content tx-size-sm">
                                                <div class="modal-header pd-x-20">
                                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">عرض
                                                        التفاصيل</h6>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body pd-20">
                                                    <input class="form-control" placeholder="{{$suggestion->details}}"
                                                           type="text"
                                                           readonly>
                                                </div><!-- modal-body -->
                                                <div class="modal-footer" style="direction: rtl">
                                                    <button type="button" class="btn btn-secondary tx-size-xs"
                                                            data-dismiss="modal">غلق
                                                    </button>
                                                </div>
                                            </div>
                                        </div><!-- modal-dialog -->
                                    </div><!-- modal -->
                                @endforeach
                            @endif
                        </div><!-- br-section-wrapper -->
                    </div><!-- br-pagebody -->
                </div><!-- tab-pane -->
            @endcan
            @can('show_orders')
                <div class="tab-pane fade  show" id="orders">
                    <div class="br-pagebody">
                        <div class="br-section-wrapper">
                            <div class="table-wrapper">
                                <p id="date_filter" class="col-sm-12"
                                   style="display: flex ; justify-content: space-between">
                                <span id="date-label-from" class="date-label">من : <input name="min" id="min"
                                                                                          type="text"> </span>
                                    <span id="date-label-to" class="date-label">إلى :<input name="max" id="max"
                                                                                            type="text"> </span>
                                </p>
                                <table id="orders_table" class=" display  nowrap" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th class="wd-15p all print">رقم الطلب</th>
                                        <th class="wd-15p all print">تاريخ الطلب</th>
                                        <th class="wd-15p all " style="text-align: center ;">التفاصيل</th>
                                        <th class="wd-15p all">المستخدم</th>
                                        <th class="wd-15p all print">المستخدم</th>
                                        <th class="wd-15p all">المندوب</th>
                                        <th class="wd-15p all print">المندوب</th>
                                        <th class="wd-15p all ">حاله الطلب</th>
                                        <th class="wd-15p all ">سبب الإلغاء</th>
                                        <th class="wd-15p all print">المدينه</th>
                                        <th class="wd-15p all print">الشحن</th>
                                        <th class="wd-15p all print">الإجمالى</th>
                                        <th class="wd-15p all">تفاصيل الفاتوره</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (count($user->orders))
                                        @foreach($user->orders as  $order)
                                            <tr id="{{$order->id}}">
                                                <td>
                                                    <a href="{{route('admin.users.user.details',$order->user->id)}}"> {{$order->id}} </a>
                                                </td>
                                                <td>{{$order->created_at}}</td>
                                                <td>
                                                    <a href=""
                                                       class="btn btn-info tx-11 tx-uppercase pd-y-11 pd-x-15 tx-mont tx-medium"
                                                       data-toggle="modal" data-target="#modaldemooo{{$order->id}}">تفاصيل
                                                        الطلب</a>
                                                </td>
                                                <td>
                                                    <a href=""
                                                       class="btn btn-info tx-11 tx-uppercase pd-y-11 pd-x-15 tx-mont tx-medium"
                                                       data-toggle="modal" data-target="#modaldemozz{{$order->id}}">تفاصيل
                                                        المستخدم</a>
                                                </td>
                                                <td>
                                                    <b>الإسم : </b> {{$order->user->name}} | <b> الهاتف
                                                        :</b> {{$order->user->phone}}
                                                </td>
                                                <td>
                                                    @if ($order->delegate_id)
                                                        <a href=""
                                                           class="btn btn-info tx-11 tx-uppercase pd-y-11 pd-x-15 tx-mont tx-medium"
                                                           data-toggle="modal" data-target="#modaldemo{{$order->id}}">تفاصيل
                                                            المندوب</a>
                                                    @else
                                                        لا يوجد
                                                    @endif

                                                </td>
                                                <td>
                                                    <b>الإسم : </b> {{$order->delegate->name}} | <b>الهاتف
                                                        : </b> {{$order->delegate->phone}}
                                                </td>

                                                <td>
                                                    @if ($order->order_status == 0)
                                                        طلب جديد
                                                    @elseif ($order->order_status == 1)
                                                        إلى الطريق للمغسله
                                                    @elseif ($order->order_status == 2)
                                                        تم التسليم للمغسله
                                                    @elseif ($order->order_status == 3)
                                                        إلى الطريق للعميل
                                                    @elseif ($order->order_status == 4)
                                                        مكتمل
                                                    @elseif ($order->order_status == 5)
                                                        ملغى
                                                    @endif
                                                </td>
                                                <td>{{$order->order_status == 5 ? $order->reason : 'غير ملغى'}}</td>
                                                <td>{{$order->city }}</td>
                                                <td>{{$order->delivery}}</td>
                                                <td>{{$order->totalPrice()}}</td>
                                                <td>
                                                    <form action="{{route('admin.orders.invoice.download' , $order->id)}}"
                                                          method="post">
                                                        @csrf
                                                        <button type="submit" style="padding: 12px;"
                                                                class="btn btn-warning btn-icon mg-r-5 mg-b-10">
                                                            <i class="fa fa-cloud-download"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- table-wrapper -->
                        @if (count($user->orders))
                            @foreach($user->orders as  $order)
                                <!-- LARGE MODAL -->
                                    <div id="modaldemooo{{$order->id}}" class="modal fade">
                                        <div class="modal-dialog modal-lg" role="document"
                                             style="direction: rtl ; width: 600px">
                                            <div class="modal-content tx-size-sm">
                                                <div class="modal-header pd-x-20">
                                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">
                                                        الخدمات</h6>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body pd-20">
                                                    <table style="width: 100%" class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>الإسم</th>
                                                            <th>النوع</th>
                                                            <th>الكميه</th>
                                                            <th>التكلفه</th>
                                                        </tr>
                                                        </thead>
                                                        @foreach($order->getOrderServices() as $service)
                                                            <tbody>
                                                            <tr>
                                                                <td>{{$service['service_name']}}</td>
                                                                <td>{{$service['service_type']}}</td>
                                                                <td>{{$service['service_count']}}</td>
                                                                <td>{{$service['service_price']}}</td>
                                                            </tr>
                                                            </tbody>
                                                        @endforeach
                                                    </table>
                                                </div><!-- modal-body -->
                                                <div class="modal-footer" style="direction: rtl">
                                                    <button type="button" class="btn btn-secondary tx-size-xs"
                                                            data-dismiss="modal">غلق
                                                    </button>
                                                </div>
                                            </div>
                                        </div><!-- modal-dialog -->
                                    </div><!-- modal -->
                                    <!-- LARGE MODAL -->
                                    <div id="modaldemozz{{$order->id}}" class="modal fade">
                                        <div class="modal-dialog modal-lg" role="document"
                                             style="direction: rtl ; width: 600px">
                                            <div class="modal-content tx-size-sm">
                                                <div class="modal-header pd-x-20">
                                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">
                                                        المستخدم</h6>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body pd-20">
                                                    <table style="width: 100%" class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>الإسم</th>
                                                            <th>البريد الإلكترونى</th>
                                                            <th>الهاتف</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>{{$order->user->name}}</td>
                                                            <td>{{$order->user->email}}</td>
                                                            <td>{{$order->user->phone}}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div><!-- modal-body -->
                                                <div class="modal-footer" style="direction: rtl">
                                                    <button type="button" class="btn btn-secondary tx-size-xs"
                                                            data-dismiss="modal">غلق
                                                    </button>
                                                </div>
                                            </div>
                                        </div><!-- modal-dialog -->
                                    </div><!-- modal -->
                                @if ($order->delegate_id)
                                    <!-- LARGE MODAL -->
                                        <div id="modaldemo{{$order->id}}" class="modal fade">
                                            <div class="modal-dialog modal-lg" role="document"
                                                 style="direction: rtl ; width: 600px">
                                                <div class="modal-content tx-size-sm">
                                                    <div class="modal-header pd-x-20">
                                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">
                                                            المندوب</h6>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body pd-20">
                                                        <table style="width: 100%" class="table">
                                                            <thead>
                                                            <tr>
                                                                <th>الإسم</th>
                                                                <th>البريد الإلكترونى</th>
                                                                <th>الهاتف</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>{{$order->delegate->name}}</td>
                                                                <td>{{$order->delegate->email}}</td>
                                                                <td>{{$order->delegate->phone}}</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div><!-- modal-body -->
                                                    <div class="modal-footer" style="direction: rtl">
                                                        <button type="button" class="btn btn-secondary tx-size-xs"
                                                                data-dismiss="modal">غلق
                                                        </button>
                                                    </div>
                                                </div>
                                            </div><!-- modal-dialog -->
                                        </div><!-- modal -->
                                        <!-- LARGE MODAL -->
                                    @endif
                                @endforeach
                            @endif
                        </div><!-- br-section-wrapper -->
                    </div><!-- br-pagebody -->
                </div><!-- tab-pane -->
            @endcan
        </div><!-- br-pagebody -->


    </div>
@stop
@section('scripts')
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

    <script type="text/javascript" src="{{asset('cpanel/js/orders.js')}}"></script>

    <script>
        $(document).ready(function () {
            initMap('{{$user->lat}}', '{{$user->lng}}');
        });
    </script>
    <script type="text/javascript" src="{{asset('cpanel/js/bootstrap4-toggle.js')}}"></script>

    @can('edit_users')
        <script>
            $(document).on('change', '#status', function () {
                var status;
                if ($(this).is(':checked')) {
                    status = 1;
                } else {
                    status = 0;
                }
                var id = $(this).attr("uid");
                var token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('admin.users.toggle.status') }}",
                    type: "PATCH",
                    data: {status: status, id: id, _token: token},
                });
            })
        </script>
    @endcan
@stop