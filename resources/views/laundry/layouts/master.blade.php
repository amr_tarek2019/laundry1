<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
    <!-- compiled css -->
    <link href="{{ asset('css/admin.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <meta name="csrf-token" content="{{@csrf_token()}}">
    @yield('styles')

</head>

<body>

<!-- ########## START: LEFT PANEL ########## -->
@include('laundry.layouts.navigation')
<!-- ########## END: LEFT PANEL ########## -->

<!-- ########## START: HEAD PANEL ########## -->
<div id="notifications">
    @include('laundry.layouts.header')
</div>
<!-- ########## END: HEAD PANEL ########## -->


<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel" style="direction: rtl">
    @yield('content')
    @include('laundry.layouts.footer')
</div><!-- br-mainpanel -->
@include('laundry.pages.modals.modals')
<!-- ########## END: MAIN PANEL ########## -->
<script type="text/javascript" src="{{asset('js/admin.js')}}"></script>

<script>

    setTimeout(fade_out, 5000);

    function fade_out() {
        $("#validation").fadeOut().empty();
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
@yield('scripts')
<script>
    // Enable pusher logging - don't include this in production
    /*Pusher.logToConsole = true;*/
    var pusher = new Pusher("{{env('PUSHER_APP_KEY')}}", {
        cluster: 'eu',
        forceTLS: true
    });
    var channel2 = pusher.subscribe('new-message');
    channel2.bind('new-message', (e) => {
        $.ajax({
            url: '{{route('laundry.dashboard.new.notification')}}',
            type: 'get',
            data: {_toke: '{{csrf_token()}}'},
            success: function (response) {
                $('#notifications').html(response.view);
            },
            error: function () {
                alert('error');
            }
        });
    });
    var channel = pusher.subscribe('new-order');
    channel.bind('new-order', (e) => {
        $.ajax({
            url: '{{route('laundry.dashboard.new.notification')}}',
            type: 'get',
            data: {_toke: '{{csrf_token()}}'},
            success: function (response) {
                $('#notifications').html(response.view);
            },
            error: function () {
                alert('error');
            }
        });
    });
</script>
{{--<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.js"></script>

<script type="text/javascript">
    $('ul.pagination').hide();
    $('.infinite-scroll').infiniteScroll({
        // options
        path: '.pagination li.active + li a',
        append: '.notification',
        history: false,
        checkLastPage: true,
        button: '.view-more',
// load pages on button click
        scrollThreshold: false,
// disable loading on scroll

    });
    $('.dropdown-menu').click(function (e) {
        e.stopPropagation();
    });
</script>--}}

</body>
</html>
