<!DOCTYPE html>
<html dir="rtl" lang="ar">
<head>
    <meta charset="utf-8">
    <title>{{ $invoice['name'] }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        h1, h2, h3, h4, p, span, div {
            font-family: DejaVu Sans;
        }

        .style {
            text-align: right;
        }
    </style>
</head>
<body class="container">
<div style="clear:both; position:relative;">
    {{--            <div style="position:absolute; left:0pt; width:250pt;">--}}
    {{--                <img class="img-rounded" height="{{ $invoice['logo_height'] }}" src="{{ $invoice['logo'] }}">--}}
    {{--            </div>--}}
    <div style="margin-left:300pt;">
        <b>التاريخ : </b> {{ $invoice['date'] }} <br/>
        @if ($invoice['number'] )
            <b>فاتوره : </b>  # {{ $invoice['number'] }}
        @endif
        <br/>
    </div>
</div>
<br/>
<h2>{{ $invoice['name'] }} </h2>
<div class="col-md-12">
    <div class="row">
        <div class="col-md-4">
            <div>
                <h4>بيانات المندوب :</h4>
                <div class="panel panel-default">
                    <div class="panel-body">
                        {!! is_null($invoice['delegate_detail'])  ? 'لا يوجد تفاصيل' : '' !!}
                        @if (!is_null($invoice['delegate_detail']))
                            <b>الإسم : </b> {{ $invoice['delegate_detail']->name }}<br/>
                            <b>البريد الإلكترونى : </b> {{ $invoice['delegate_detail']->email }}<br/>
                            <b>الهاتف : </b> {{ $invoice['delegate_detail']->phone }}<br/>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4 ">
            <div>
                <h4>بيانات العميل :</h4>
                <div class="panel panel-default">
                    <div class="panel-body">
                        {!! is_null($invoice['user_detail'])  ? '<i>لا يوجد تفاصيل</i><br />' : '' !!}
                        <b>الإسم : </b> {{ $invoice['user_detail']->name }}<br/>
                        <b>البريد الإلكترونى : </b> {{ $invoice['user_detail']->email }}<br/>
                        <b>الهاتف : </b> {{ $invoice['user_detail']->phone }}<br/>
                        <b>عنوان الطلب : </b> {{ $invoice['address'] }}<br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<h4>المنتجات : </h4>
<table class="table table-bordered">
    <thead>
    <tr>
        <th class="style">#</th>
        <th class="style">الخدمه</th>
        <th class="style">النوع</th>
        <th class="style">السعر</th>
        <th class="style">الكميه</th>
        <th class="style">الإجمالى</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($invoice['items'] as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item['service_name'] }}</td>
            <td>{{ $item['service_type'] }}</td>
            <td>{{ $item['service_price'] }}</td>
            <td>{{ $item['service_count'] }}</td>
            <td>{{ $item['service_total'] }}</td>

        </tr>
    @endforeach
    </tbody>
</table>
<div style="clear:both; position:relative;">
    <div style="margin-left: 300pt;">
        <h4>الإجمالى :</h4>
        <table class="table table-bordered">
            <tbody>
            <tr>
                <td><b>الخدمات :</b></td>
                <td>{{ $invoice['servicePrice'] }} </td>
            </tr>
            <tr>
                <td>
                    <b>
                        التوصيل :
                    </b>
                </td>
                <td>{{$invoice['delivery']}}</td>
            </tr>
            <tr>
                <td><b>الإجمالى :</b></td>
                <td><b>{{ $invoice['total'] }}</b></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
