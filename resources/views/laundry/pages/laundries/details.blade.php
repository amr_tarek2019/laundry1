@extends('laundry.layouts.master')

@section('title')

تفاصيل المستخدم

@stop
@section('styles')
<link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" media="all" rel="stylesheet"
    type="text/css" />
<script type="text/javascript" src="{{asset('cpanel/js/map.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrFDCzH3QzNG94R8W1poCbt3oIVTyxbEI&callback=initMap" async
    defer></script>
<link href="{{ asset('cpanel/css/bootstrap4-toggle.css')}}" media="all" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<style>
    .select2-container {
        width: 100% !important;
        ;
    }

    .select2-selection__choice {
        color: black !important;
        ;
    }

    .select2-selection--multiple {
        float: right !important;
    }
</style>

@stop
@section('content')

<div class="br-profile-page">
    <div class="card shadow-base bd-0 rounded-0 widget-4">
        <div class="card-header ht-75">
            <div class="hidden-xs-down"></div>
            <div class="tx-24 hidden-xs-down"></div>
        </div><!-- card-header -->
        <div class="card-body">
            <div class="card-profile-img">
                @if ($user->img)
                <img src="{{asset('uploads/profile/') .$user->img}}" alt="">
                @endif
            </div><!-- card-profile-img -->
            <h4 class="tx-normal tx-roboto tx-white">{{$user->name}}</h4>
            <p class="mg-b-25">{{$user->email}}</p>
        </div><!-- card-body -->
    </div><!-- card -->

    <div class="ht-70 bg-gray-100 pd-x-20 d-flex align-items-center justify-content-center shadow-base">
        <ul class="nav nav-outline active-info align-items-center flex-row" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#mainuserdata" role="tab">
                    بيانات المستخدم</a></li>
            {{--                @can('show_laundry')--}}
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#suggestions" role="tab">المغسله</a>
            </li>
            {{--                @endcan--}}
            {{--                --}}{{--                @can('show_orders')--}}
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#orders" role="tab">الطلبات</a>
            </li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#services" role="tab">خدمات المغسله</a>
            </li>
            {{--                --}}{{--                @endcan--}}
        </ul>
    </div>

    <div class="tab-content br-profile-body">
        <div class="tab-pane fade active show" id="mainuserdata">
            <div class="row">
                <div class="col-lg-12 mg-t-30 mg-lg-t-0">
                    <div class="card pd-20 pd-xs-30 shadow-base bd-0">
                        <h6 class="tx-gray-800 tx-uppercase tx-semibold tx-13 mg-b-25">معلومات التواصل</h6>

                        <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">الإسم</label>
                        <p class="tx-info mg-b-25">{{$user->name}}</p>

                        <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">رقم الهاتف</label>
                        <p class="tx-info mg-b-25">{{$user->phone}}</p>

                        <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">البريد
                            الالكترونى</label>
                        <p class="tx-inverse mg-b-25">{{$user->email}}</p>

                        <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">النوع</label>
                        <p class="tx-info mg-b-25">{{$user->type == 0 ? 'مستخدم' : 'بائع'}}</p>

                        <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">الحاله</label>
                        <div class="col-sm-2" style="padding: 0 !important;">
                            <input id="status" type="checkbox" uid="{{$user->id}}" class="role" data-toggle="toggle"
                                data-on="مفعل" name="is_active" data-off="معلق" data-onstyle="success"
                                data-offstyle="danger" @if ($user->is_active)
                            checked
                            @endif
                            >
                        </div>
                        <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2"
                            style="padding-top: 15px">العنوان</label>
                        <div style="height: 200px" id="map"></div>
                    </div><!-- card -->
                </div><!-- col-lg-4 -->

            </div><!-- row -->
        </div><!-- tab-pane -->
        {{--            @can('show_laundry')--}}
        <div class="tab-pane fade  show" id="suggestions">
            <div class="br-pagebody">
                <div class="br-section-wrapper">
                    <div class="table-wrapper">
                        <table id="datatable1" class=" display  nowrap" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="wd-15p all">الأقسام</th>
                                    <th class="wd-15p all">الإسم</th>
                                    <th class="wd-15p all">الوصف</th>
                                    <th class="wd-15p all">العنوان</th>
                                    <th class="wd-15p all">الهاتف</th>
                                    <th class="wd-15p all">البريد الإلكترونى</th>
                                    <th class="wd-15p all">الحاله</th>
                                    <th class="wd-15p all">تاريخ الإنتهاء</th>
                                    {{--                                        @can('edit_laundry')--}}
                                    <th class="wd-15p all">الإجراء</th>
                                    {{--                                        @endcan--}}
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="{{$user->laundry->id}}">
                                    <td>
                                        @foreach($user->laundry->section_id as $section)
                                        <span class="badge badge-pill badge-primary" style="padding: 5px">
                                            @if($section == 1)
                                            رجال
                                            @elseif($section == 2)
                                            سيدات
                                            @elseif($section == 3)
                                            سجاد
                                            @elseif($section == 4)
                                            أغطيه
                                            @endif
                                        </span>
                                        @endforeach

                                    </td>
                                    <td>
                                        {{$user->laundry->name}}
                                    </td>
                                    <td><a href=""
                                            class="btn btn-info tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium"
                                            data-toggle="modal" data-target="#modaldemooozz{{$user->laundry->id}}">عرض
                                        </a></td>
                                    <td>
                                        {{$user->laundry->address}}
                                    </td>
                                    <td>
                                        {{$user->laundry->phone}}
                                    </td>
                                    <td>
                                        {{$user->laundry->email}}
                                    </td>
                                    <td>
                                        {{$user->laundry->is_active ? 'نشط' : 'معلق'}}
                                    </td>
                                    <td>
                                        {{$user->laundry->expire_date->toDateString()}}
                                    </td>
                                    {{--                                        @can('edit_laundry')--}}
                                    <td>
                                        <a href="" data-toggle="modal" data-target="#modaldemoo"
                                            class="btn btn-info btn-icon mg-r-5 mg-b-10">
                                            <div><i class="fa fa-edit"></i></div>
                                        </a>
                                    </td>
                                    {{--                                        @endcan--}}
                                </tr>
                            </tbody>
                        </table>
                    </div><!-- table-wrapper -->

                    {{--                            @can('edit_laundry')--}}
                    <div id="modaldemoo" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                            <div class="modal-content tx-size-sm">
                                <div class="modal-header pd-x-20">
                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">تعديل
                                        مغسله</h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form id="editdelegate"
                                    action="{{route('laundry.laundries.edit.laundry',['id'=>$user->laundry->id])}}"
                                    method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-body pd-20">
                                        <div class="row mg-b-30">
                                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                <input class="form-control" required placeholder="الإسم" type="text"
                                                    name="name" value="{{$user->name}}"
                                                    data-parsley-required-message="برجاء إدخال الإسم">
                                                @include('errors.validation-messages', ['field' => 'name'])
                                            </div><!-- col-4 -->

                                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                <input class="form-control" required placeholder="name_en" type="text"
                                                    name="name_en" value="{{$user->laundry->name_en}}"
                                                    data-parsley-required-message="please enter the english name">
                                                @include('errors.validation-messages', ['field' => 'name_en'])
                                            </div><!-- col-4 -->
                                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                <input class="form-control" required placeholder="البريد الإلكترونى"
                                                    type="email" name="email" value="{{$user->email}}"
                                                    data-parsley-required-message="برجاء إدخال البريد الإلكترونى"
                                                    data-parsley-type-message="برجاء إدخال بريد الإلكترونى صحيح">
                                                @include('errors.validation-messages', ['field' => 'email'])
                                            </div><!-- col-4 -->
                                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                <input class="form-control" required placeholder="الهاتف" type="number"
                                                    name="phone" data-parsley-required-message="برجاء إدخال الهاتف"
                                                    value="{{$user->phone}}">
                                                @include('errors.validation-messages', ['field' => 'phone'])
                                            </div><!-- col-4 -->
                                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                <input class="form-control" placeholder="كلمه المرور" type="password"
                                                    name="password">
                                                @include('errors.validation-messages', ['field' => 'password'])
                                            </div><!-- col-4 -->
                                            <div class="col-lg-6  mg-t-10">
                                                <label class="custom-file">
                                                    <input type="file" id="file" name="img" class="custom-file-input">
                                                    <span class="custom-file-control">صوره المغسله</span>
                                                </label>
                                                @include('errors.validation-messages', ['field' => 'img'])
                                            </div><!-- col -->
                                            <div class="col-lg-12">
                                                <select id="sections" multiple="multiple" name="section_id[]"
                                                    class="form-control select2" required data-placeholder="الاقسام"
                                                    data-parsley-required-message="برجاء إختيار قسم واحد على الأقل">
                                                    <option @if(in_array(1,$user->laundry->section_id)) selected
                                                        @endif value="1">رجال
                                                    </option>
                                                    <option @if(in_array(2,$user->laundry->section_id)) selected
                                                        @endif value="2">سيدات
                                                    </option>
                                                    <option @if(in_array(3,$user->laundry->section_id)) selected
                                                        @endif value="3">سجاء
                                                    </option>
                                                    <option @if(in_array(4,$user->laundry->section_id)) selected
                                                        @endif value="4">أغطيه
                                                    </option>
                                                </select>
                                                @include('errors.validation-messages', ['field' => 'email'])
                                            </div><!-- col-4 -->

                                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                <label for="">خط الطول</label>
                                                <input class="form-control" required placeholder="خط الطول" type="text"
                                                    name="lat" data-parsley-required-message="برجاء إدخال خط الطول"
                                                    value="{{$user->laundry->lat}}">
                                                @include('errors.validation-messages', ['field' => 'lat'])
                                            </div><!-- col-4 -->
                                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                <label for="">خط العرض</label>
                                                <input class="form-control" required placeholder="خط العرض" type="text"
                                                    name="lng" data-parsley-required-message="برجاء إدخال خط العرض"
                                                    value="{{$user->laundry->lng}}">
                                                @include('errors.validation-messages', ['field' => 'lng'])
                                            </div><!-- col-4 -->
                                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                <label for="">العنوان</label>
                                                <input class="form-control" required placeholder="العنوان" type="text"
                                                    name="address" data-parsley-required-message="برجاء إدخال العنوان"
                                                    value="{{$user->laundry->address}}">
                                                @include('errors.validation-messages', ['field' => 'address'])
                                            </div><!-- col-4 -->
                                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                <label for="">address</label>
                                                <input class="form-control" required placeholder="address" type="text"
                                                    name="address_en"
                                                    data-parsley-required-message="please enter the address"
                                                    value="{{$user->laundry->address_en}}">
                                                @include('errors.validation-messages', ['field' => 'address_en'])
                                            </div><!-- col-4 -->
                                            <div class="col-lg-6 mg-t-10">
                                                <div class="input-group">
                                                    <textarea required name="description" id="" cols="100" rows="10"
                                                        data-parsley-required-message="برجاء إدخال الوصف">{{$user->laundry->description}}</textarea>
                                                </div>
                                                @include('errors.validation-messages', ['field' => 'description'])
                                            </div>
                                            <div class="col-lg-6 mg-t-10">
                                                <div class="input-group">

                                                    <textarea required name="description_en" id="" cols="50" rows="10"
                                                        data-parsley-required-message="please enter description">{{$user->laundry->description_en}}</textarea>
                                                </div>
                                                @include('errors.validation-messages', ['field' => 'description_en'])
                                            </div>
                                            <div class="col-lg-12 mg-t-10">
                                                <div class="input-group">
                                                    <label for="asdasd">has offers ?</label>
                                                    <input type="checkbox"
                                                        {{$user->laundry->has_offer ? 'checked' : ''}} name="has_offer"
                                                        class="form-control" id="asdasd">
                                                </div>
                                                @include('errors.validation-messages', ['field' => 'description_en'])
                                            </div>
                                        </div>
                                    </div><!-- modal-body -->
                                    <div class="modal-footer" style="direction: rtl">
                                        <button type="submit" class="btn btn-primary tx-size-xs">حفظ
                                        </button>
                                        <button type="button" class="btn btn-secondary tx-size-xs"
                                            data-dismiss="modal">غلق
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                    {{--                        @endcan--}}
                    <!-- LARGE MODAL -->
                    <div id="modaldemooozz{{$user->laundry->id}}" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                            <div class="modal-content tx-size-sm">
                                <div class="modal-header pd-x-20">
                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">عرض
                                        الوصف</h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body pd-20">
                                    <input class="form-control" placeholder="{{$user->laundry->description}}"
                                        type="text" readonly>
                                </div><!-- modal-body -->
                                <div class="modal-footer" style="direction: rtl">
                                    <button type="button" class="btn btn-secondary tx-size-xs" data-dismiss="modal">غلق
                                    </button>
                                </div>
                            </div>
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                </div><!-- br-section-wrapper -->
            </div><!-- br-pagebody -->
        </div><!-- tab-pane -->
        {{--            @endcan--}}
        {{--            @can('show_orders')--}}
        <div class="tab-pane fade  show" id="orders">
            <div class="br-pagebody">
                <div class="br-section-wrapper">
                    <div class="table-wrapper">
                        <p id="date_filter" class="col-sm-12" style="display: flex ; justify-content: space-between">
                            <span id="date-label-from" class="date-label">من : <input name="min" id="min" type="text">
                            </span>
                            <span id="date-label-to" class="date-label">إلى :<input name="max" id="max" type="text">
                            </span>
                        </p>
                        <table id="orders_table" class=" display  nowrap" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="wd-15p all print">رقم الطلب</th>
                                    <th class="wd-15p all print">تاريخ الطلب</th>
                                    <th class="wd-15p all " style="text-align: center ;">التفاصيل</th>
                                    <th class="wd-15p all">المستخدم</th>
                                    <th class="wd-15p all print">المستخدم</th>
                                    <th class="wd-15p all">المندوب</th>
                                    <th class="wd-15p all print">المندوب</th>
                                    <th class="wd-15p all ">حاله الطلب</th>
                                    <th class="wd-15p all ">سبب الإلغاء</th>
                                    <th class="wd-15p all print">المدينه</th>
                                    <th class="wd-15p all print">الشحن</th>
                                    <th class="wd-15p all print">الإجمالى</th>
                                    <th class="wd-15p all">تفاصيل الفاتوره</th>

                                </tr>
                            </thead>
                            <tbody>
                                @if (count($user->orders))
                                @foreach($user->orders as $order)
                                <tr id="{{$order->id}}">
                                    <td>
                                        <a href="{{route('admin.users.user.details',$order->user->id)}}"> {{$order->id}}
                                        </a>
                                    </td>
                                    <td>{{$order->created_at}}</td>
                                    <td>
                                        <a href=""
                                            class="btn btn-info tx-11 tx-uppercase pd-y-11 pd-x-15 tx-mont tx-medium"
                                            data-toggle="modal" data-target="#modaldemooo{{$order->id}}">تفاصيل
                                            الطلب</a>
                                    </td>
                                    <td>
                                        <a href=""
                                            class="btn btn-info tx-11 tx-uppercase pd-y-11 pd-x-15 tx-mont tx-medium"
                                            data-toggle="modal" data-target="#modaldemozz{{$order->id}}">تفاصيل
                                            المستخدم</a>
                                    </td>
                                    <td>
                                        <b>الإسم : </b> {{$order->user->name}} | <b> الهاتف
                                            :</b> {{$order->user->phone}}
                                    </td>
                                    <td>
                                        @if ($order->delegate_id)
                                        <a href=""
                                            class="btn btn-info tx-11 tx-uppercase pd-y-11 pd-x-15 tx-mont tx-medium"
                                            data-toggle="modal" data-target="#modaldemo{{$order->id}}">تفاصيل
                                            المندوب</a>
                                        @else
                                        لا يوجد
                                        @endif

                                    </td>
                                    <td>
                                        <b>الإسم : </b> {{$order->delegate->name}} | <b>الهاتف
                                            : </b> {{$order->delegate->phone}}
                                    </td>

                                    <td>
                                        @if ($order->order_status == 0)
                                        طلب جديد
                                        @elseif ($order->order_status == 1)
                                        إلى الطريق للمغسله
                                        @elseif ($order->order_status == 2)
                                        تم التسليم للمغسله
                                        @elseif ($order->order_status == 3)
                                        إلى الطريق للعميل
                                        @elseif ($order->order_status == 4)
                                        مكتمل
                                        @elseif ($order->order_status == 5)
                                        ملغى
                                        @endif
                                    </td>
                                    <td>{{$order->order_status == 5 ? $order->reason : 'غير ملغى'}}</td>
                                    <td>{{$order->city }}</td>
                                    <td>{{$order->delivery}}</td>
                                    <td>{{$order->totalPrice()}}</td>
                                    <td>
                                        <form action="{{route('laundry.laundries.invoice.download' , $order->id)}}"
                                            method="post">
                                            @csrf
                                            <button type="submit" style="padding: 12px;"
                                                class="btn btn-warning btn-icon mg-r-5 mg-b-10">
                                                <i class="fa fa-cloud-download"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div><!-- table-wrapper -->
                    @if (count($user->orders))
                    @foreach($user->orders as $order)
                    <!-- LARGE MODAL -->
                    <div id="modaldemooo{{$order->id}}" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document" style="direction: rtl ; width: 600px">
                            <div class="modal-content tx-size-sm">
                                <div class="modal-header pd-x-20">
                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">
                                        الخدمات</h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body pd-20">
                                    <table style="width: 100%" class="table">
                                        <thead>
                                            <tr>
                                                <th>الإسم</th>
                                                <th>النوع</th>
                                                <th>الكميه</th>
                                                <th>التكلفه</th>
                                            </tr>
                                        </thead>
                                        @foreach($order->getOrderServices() as $service)
                                        <tbody>
                                            <tr>
                                                <td>{{$service['service_name']}}</td>
                                                <td>{{$service['service_type']}}</td>
                                                <td>{{$service['service_count']}}</td>
                                                <td>{{$service['service_price']}}</td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div><!-- modal-body -->
                                <div class="modal-footer" style="direction: rtl">
                                    <button type="button" class="btn btn-secondary tx-size-xs" data-dismiss="modal">غلق
                                    </button>
                                </div>
                            </div>
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                    <!-- LARGE MODAL -->
                    <div id="modaldemozz{{$order->id}}" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document" style="direction: rtl ; width: 600px">
                            <div class="modal-content tx-size-sm">
                                <div class="modal-header pd-x-20">
                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">
                                        المستخدم</h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body pd-20">
                                    <table style="width: 100%" class="table">
                                        <thead>
                                            <tr>
                                                <th>الإسم</th>
                                                <th>البريد الإلكترونى</th>
                                                <th>الهاتف</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{$order->user->name}}</td>
                                                <td>{{$order->user->email}}</td>
                                                <td>{{$order->user->phone}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div><!-- modal-body -->
                                <div class="modal-footer" style="direction: rtl">
                                    <button type="button" class="btn btn-secondary tx-size-xs" data-dismiss="modal">غلق
                                    </button>
                                </div>
                            </div>
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                    @if ($order->delegate_id)
                    <!-- LARGE MODAL -->
                    <div id="modaldemo{{$order->id}}" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document" style="direction: rtl ; width: 600px">
                            <div class="modal-content tx-size-sm">
                                <div class="modal-header pd-x-20">
                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">
                                        المندوب</h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body pd-20">
                                    <table style="width: 100%" class="table">
                                        <thead>
                                            <tr>
                                                <th>الإسم</th>
                                                <th>البريد الإلكترونى</th>
                                                <th>الهاتف</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{$order->delegate->name}}</td>
                                                <td>{{$order->delegate->email}}</td>
                                                <td>{{$order->delegate->phone}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div><!-- modal-body -->
                                <div class="modal-footer" style="direction: rtl">
                                    <button type="button" class="btn btn-secondary tx-size-xs" data-dismiss="modal">غلق
                                    </button>
                                </div>
                            </div>
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                    <!-- LARGE MODAL -->
                    @endif
                    @endforeach
                    @endif
                </div><!-- br-section-wrapper -->
            </div><!-- br-pagebody -->
        </div><!-- tab-pane -->
        {{--            @endcan--}}

        <div class="tab-pane fade  show" id="services">
            <div class="col-md-4">
                <div class="col-md-5 mg-b-15">
                    <a href="" class="btn btn-success btn-with-icon btn-block" data-toggle="modal"
                        data-target="#modaldemozxcfeqwr">
                        <div class="ht-40 justify-content-between">
                            <span class="icon wd-40">
                                <i class="fa fa-plus"></i></span>
                            <span class="pd-x-15">اضافه </span>
                        </div>
                    </a>
                </div>
            </div>
            <div id="modaldemozxcfeqwr" class="modal fade">
                <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                    <div class="modal-content tx-size-sm">
                        <div class="modal-header pd-x-20">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">اضافه خدمه</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="adddelegate"
                            action="{{route('laundry.laundries.add.service',['id'=>$user->laundry->id])}}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body pd-20">
                                <div class="row mg-b-30">
                                    <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                        <input class="form-control" required placeholder="الإسم" type="text" name="name"
                                            data-parsley-required-message="برجاء إدخال الإسم">
                                        @include('errors.validation-messages', ['field' => 'name'])
                                    </div><!-- col-4 -->
                                    <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                        <input class="form-control" required placeholder="name" type="text"
                                            name="name_en" data-parsley-required-message="please enter the name">
                                        @include('errors.validation-messages', ['field' => 'name_en'])
                                    </div><!-- col-4 -->
                                    <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                        <input class="form-control" placeholder="غسيل" type="number" name="wash">
                                    </div><!-- col-4 -->
                                    <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                        <input class="form-control" placeholder="كى" type="number" name="ironing">
                                    </div><!-- col-4 -->
                                    <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                        <input class="form-control" placeholder="غسيل و كى" type="number"
                                            name="wash_ironing">
                                    </div><!-- col-4 -->
                                    <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                        <select id="sections_edit" name="section_id" class="form-control select2"
                                            required data-placeholder="الاقسام"
                                            data-parsley-required-message="برجاء إختيار قسم ">
                                            <option @if(1==old('section_id')) selected @endif value="1">رجال
                                            </option>
                                            <option @if(2==old('section_id')) selected @endif value="2">سيدات
                                            </option>
                                            <option @if(3==old('section_id')) selected @endif value="3">سجاء
                                            </option>
                                            <option @if(4==old('section_id')) selected @endif value="4">أغطيه
                                            </option>
                                        </select>
                                        @include('errors.validation-messages', ['field' => 'section_id'])
                                    </div><!-- col-4 -->
                                    <div class="col-lg-6 mg-b-30 " style="text-align: center">
                                        <label class="custom-file">
                                            <input type="file" id="file" name="img" class="custom-file-input" required
                                                data-parsley-required-message="برجاء إختيار صوره الخدمه">
                                            <span class="custom-file-control">صوره الخدمه</span>
                                        </label>
                                        @include('errors.validation-messages', ['field' => 'img'])
                                    </div><!-- col -->
                                </div>
                            </div><!-- modal-body -->
                            <div class="modal-footer" style="direction: rtl">
                                <button type="submit" class="btn btn-primary tx-size-xs">حفظ
                                </button>
                                <button type="button" class="btn btn-secondary tx-size-xs" data-dismiss="modal">غلق
                                </button>
                            </div>
                        </form>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->
            <div class="br-pagebody">
                <div class="br-section-wrapper">
                    <div class="table-wrapper">
                        <table id="datatable1" class=" display  nowrap" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="wd-15p all">الاسم بالعربى</th>
                                    <th class="wd-15p all">الاسم بالإنجليزى</th>
                                    <th class="wd-15p all">القسم </th>
                                    <th class="wd-15p all">غسيل</th>
                                    <th class="wd-15p all">كى</th>
                                    <th class="wd-15p all">غسيل و كى</th>
                                    <th class="wd-15p all">الصوره</th>

                                    <th class="wd-20p all">المزيد</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($user->laundry->services as $service)
                                <tr id="{{$service->id}}">
                                    <td>{{$service->name}}</td>
                                    <td>{{$service->name_en}}</td>
                                    <td><span class="badge badge-pill badge-primary" style="padding: 5px">
                                            @if($service->section_id == 1)
                                            رجال
                                            @elseif($service->section_id == 2)
                                            سيدات
                                            @elseif($service->section_id == 3)
                                            سجاد
                                            @elseif($service->section_id == 4)
                                            أغطيه
                                            @endif
                                        </span></td>
                                    <td>{{$service->wash}}</td>
                                    <td>{{$service->ironing}}</td>
                                    <td>{{$service->wash_ironing}}</td>
                                    <td><img width="70%" src="{{$service->img}}" alt="" /></td>
                                    <td>

                                        <a href="" data-toggle="modal" data-target="#modaldemoozzaa{{$service->id}}"
                                            class="btn btn-info btn-icon mg-r-5 mg-b-10">
                                            <div><i class="fa fa-edit"></i></div>
                                        </a>

                                        <a href="#"
                                            onclick="showDeletConfirmationModal('{{route('laundry.laundries.service.delete', $service->id)}}'  , {{$service->id}})"
                                            class="btn btn-danger btn-icon mg-r-5 mg-b-10">
                                            <div><i class="fa fa-trash-o"></i></div>
                                        </a>

                                    </td>

                                </tr>
                                <div id="modaldemoozzaa{{$service->id}}" class="modal fade">
                                    <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">تعديل
                                                    خدمه</h6>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form id="adddelegate"
                                                action="{{route('admin.laundries.edit.service',['id'=>$service->id])}}"
                                                method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="modal-body pd-20">
                                                    <div class="row mg-b-30">
                                                        <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                            <input class="form-control" required placeholder="الإسم"
                                                                type="text" name="name" value="{{$service->name}}"
                                                                data-parsley-required-message="برجاء إدخال الإسم">
                                                            @include('errors.validation-messages', ['field' => 'name'])
                                                        </div><!-- col-4 -->
                                                        <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                            <input class="form-control" required placeholder="name"
                                                                type="text" name="name_en" value="{{$service->name_en}}"
                                                                data-parsley-required-message="please enter the name">
                                                            @include('errors.validation-messages', ['field' =>
                                                            'name_en'])
                                                        </div><!-- col-4 -->
                                                        <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                            <input class="form-control" placeholder="غسيل"
                                                                value="{{$service->wash}}" type="number" name="wash">
                                                        </div><!-- col-4 -->
                                                        <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                            <input class="form-control" placeholder="كى"
                                                                value="{{$service->ironing}}" type="number"
                                                                name="ironing">
                                                        </div><!-- col-4 -->
                                                        <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                            <input class="form-control" placeholder="غسيل و كى"
                                                                value="{{$service->wash_ironing}}" type="number"
                                                                name="wash_ironing">
                                                        </div><!-- col-4 -->
                                                        <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                            <select id="sections_edit" name="section_id"
                                                                class="form-control select2" required
                                                                data-placeholder="الاقسام"
                                                                data-parsley-required-message="برجاء إختيار قسم ">
                                                                <option @if(1==$service->section_id) selected
                                                                    @endif value="1">رجال
                                                                </option>
                                                                <option @if(2==$service->section_id) selected
                                                                    @endif value="2">سيدات
                                                                </option>
                                                                <option @if(3==$service->section_id) selected
                                                                    @endif value="3">سجاء
                                                                </option>
                                                                <option @if(4==$service->section_id) selected
                                                                    @endif value="4">أغطيه
                                                                </option>
                                                            </select>
                                                            @include('errors.validation-messages', ['field' =>
                                                            'section_id'])
                                                        </div><!-- col-4 -->
                                                        <div class="col-lg-6 mg-b-30 " style="text-align: center">
                                                            <label class="custom-file">
                                                                <input type="file" id="file" name="img"
                                                                    class="custom-file-input">
                                                                <span class="custom-file-control">صوره الخدمه</span>
                                                            </label>
                                                            @include('errors.validation-messages', ['field' => 'img'])
                                                        </div><!-- col -->
                                                    </div>
                                                </div><!-- modal-body -->
                                                <div class="modal-footer" style="direction: rtl">
                                                    <button type="submit" class="btn btn-primary tx-size-xs">حفظ
                                                    </button>
                                                    <button type="button" class="btn btn-secondary tx-size-xs"
                                                        data-dismiss="modal">غلق
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div><!-- modal-dialog -->
                                </div><!-- modal -->
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- table-wrapper -->

                    @can('edit_laundry')
                    <div id="modaldemoo" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                            <div class="modal-content tx-size-sm">
                                <div class="modal-header pd-x-20">
                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">تعديل
                                        مغسله</h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form id="editdelegate"
                                    action="{{route('laundry.laundries.edit.laundry',['id'=>$user->laundry->id])}}"
                                    method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-body pd-20">
                                        <div class="row mg-b-30">
                                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                <input class="form-control" required placeholder="الإسم" type="text"
                                                    name="name" value="{{$user->laundry->name}}"
                                                    data-parsley-required-message="برجاء إدخال الإسم">
                                                @include('errors.validation-messages', ['field' => 'name'])
                                            </div><!-- col-4 -->
                                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                <input class="form-control" required placeholder="البريد الإلكترونى"
                                                    type="email" name="email" value="{{$user->laundry->email}}"
                                                    data-parsley-required-message="برجاء إدخال البريد الإلكترونى"
                                                    data-parsley-type-message="برجاء إدخال بريد الإلكترونى صحيح">
                                                @include('errors.validation-messages', ['field' => 'email'])
                                            </div><!-- col-4 -->
                                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                <input class="form-control" required placeholder="الهاتف" type="number"
                                                    name="phone" data-parsley-required-message="برجاء إدخال الهاتف"
                                                    value="{{$user->laundry->phone}}">
                                                @include('errors.validation-messages', ['field' => 'phone'])
                                            </div><!-- col-4 -->
                                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                                <input class="form-control" placeholder="كلمه المرور" type="password"
                                                    name="password">
                                                @include('errors.validation-messages', ['field' => 'password'])
                                            </div><!-- col-4 -->
                                            <div class="col-lg-6">
                                                <select id="sections" multiple="multiple" name="section_id[]"
                                                    class="form-control select2" required data-placeholder="الاقسام"
                                                    data-parsley-required-message="برجاء إختيار قسم واحد على الأقل">
                                                    <option @if(in_array(1,$user->laundry->section_id)) selected
                                                        @endif value="1">رجال
                                                    </option>
                                                    <option @if(in_array(2,$user->laundry->section_id)) selected
                                                        @endif value="2">سيدات
                                                    </option>
                                                    <option @if(in_array(3,$user->laundry->section_id)) selected
                                                        @endif value="3">سجاء
                                                    </option>
                                                    <option @if(in_array(4,$user->laundry->section_id)) selected
                                                        @endif value="4">أغطيه
                                                    </option>
                                                </select>
                                                @include('errors.validation-messages', ['field' => 'email'])
                                            </div><!-- col-4 -->
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i
                                                            class="icon ion-calendar tx-16 lh-0 op-6"></i></span>
                                                    <input required type="date" name="expire_date" class="form-control"
                                                        value="{{$user->laundry->modified_expire_date}}"
                                                        data-parsley-required-message="برجاء إختيار تاريخ الانتهاء">
                                                </div>
                                                @include('errors.validation-messages', ['field' => 'expire_date'])
                                            </div>
                                        </div>
                                    </div><!-- modal-body -->
                                    <div class="modal-footer" style="direction: rtl">
                                        <button type="submit" class="btn btn-primary tx-size-xs">حفظ
                                        </button>
                                        <button type="button" class="btn btn-secondary tx-size-xs"
                                            data-dismiss="modal">غلق
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                    @endcan
                    <!-- LARGE MODAL -->
                    <div id="modaldemooozz{{$user->laundry->id}}" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                            <div class="modal-content tx-size-sm">
                                <div class="modal-header pd-x-20">
                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">عرض
                                        الوصف</h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body pd-20">
                                    <input class="form-control" placeholder="{{$user->laundry->description}}"
                                        type="text" readonly>
                                </div><!-- modal-body -->
                                <div class="modal-footer" style="direction: rtl">
                                    <button type="button" class="btn btn-secondary tx-size-xs" data-dismiss="modal">غلق
                                    </button>
                                </div>
                            </div>
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                </div><!-- br-section-wrapper -->
            </div><!-- br-pagebody -->
        </div><!-- tab-pane -->
    </div><!-- br-pagebody -->


</div>
@stop
@section('scripts')
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

<script type="text/javascript" src="{{asset('cpanel/js/orders.js')}}"></script>

<script>
    $(document).ready(function () {
        initMap('{{$user->laundry->lat}}', '{{$user->laundry->lng}}');
    });

</script>
<script type="text/javascript" src="{{asset('cpanel/js/bootstrap4-toggle.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>



<script>
    $('#sections').select2({
        dropdownParent: $('#modaldemoo'),
        width: '100%'
    });

</script>
{{--    @can('edit_laundry_owner')--}}
{{--   <script>
            $(document).on('change', '#status', function () {
                var status;
                if ($(this).is(':checked')) {
                    status = 1;
                } else {
                    status = 0;
                }
                var id = $(this).attr("uid");
                var token = "{{ csrf_token() }}";
$.ajax({
url: "{{ route('admin.users.toggle.status') }}",
type: "PATCH",
data: {status: status, id: id, _token: token},
});
})
</script>--}}
{{--    @endcan--}}
@stop