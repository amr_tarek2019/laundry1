<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>laundry station</title>

    <!-- Stylesheets -->


    <link href="{{asset('site/css/all.css')}}" rel="stylesheet">
    <link href="{{asset('site/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('site/css/responsive.css')}}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" id="jssDefault" href="{{asset('site/css/custom/theme-2.css')}}" />
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">


</head>

<!-- page wrapper -->

<body class="boxed_wrapper">


    <!-- .preloader -->
    <div class="preloader"></div>
    <!-- /.preloader -->

    <!-- main header area -->
    <header class="main-header">

        <!-- header lower/fixed-header -->
        <div class="theme_menu stricky header-lower">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="logo-box">
                            <a href="{{route('site.home.index')}}"></a>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-12 col-xs-12">
                        <div class="menu-bar">
                            <nav class="main-menu">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                                        data-target=".navbar-collapse">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="navbar-collapse collapse clearfix">
                                    <ul class="navigation clearfix">
                                        <li class="current"><a href="{{route('site.home.index')}}">الرئيسية</a></li>
                                        <li><a href="{{ route('site.home.about') }}">عن لاندرى </a>

                                        </li>
                                        <li><a href="{{ route('site.laundries.index') }}">المغاسل </a>
                                        {{-- <li><a href="{{ route('site.faq.index') }}">الاسئله الشائعه </a> --}}

                                        <li><a href="{{ route('site.news.index') }}">الأخبار</a>
                                        {{-- <li><a href="{{ route('site.home.terms') }}">الشروط والاحكام</a> --}}

                                        </li>
                                        <li><a href="{{ route('site.home.contact.get') }}">تواصل معنا</a></li>
                                    </ul>

                                    <!-- mobile menu -->
                                    <ul class="mobile-menu clearfix">

                                        <li><a href="{{route('site.home.index')}}">الرئيسية</a></li>
                                        <li><a href="{{ route('site.home.about') }}">عن لاندرى </a>

                                        </li>
                                        <li><a href="{{ route('site.laundries.index') }}">المغاسل </a>
                                        {{-- <li><a href="{{ route('site.faq.index') }}">الاسئله الشائعه </a> --}}
                                        <li><a href="{{ route('site.news.index') }}">الأخبار</a>
                                        {{-- <li><a href="{{ route('site.home.terms') }}">الشروط والاحكام</a> --}}
                                        </li>
                                        <li><a href="{{ route('site.home.contact.get') }}">تواصل معنا</a></li>
                                    </ul>
                                </div>

                            </nav>
                            <div class="cart-box ">
                                <a href="{{ route('site.home.orders.get') }}"><i class="fas fa-dumpster" aria-hidden="true">


                                    </i>طلباتك</a>
                            </div>
                            <div class="cart-box cart-box2">
                                <a
                                    href="{{ auth('web')->user() ? route('site.home.profile.get') : route('site.auth.index') }}"><i
                                        class="fas fa-user-tie" aria-hidden="true">
                                    </i>حسابي</a>
                            </div>
                            @auth('web')
                            <div class="cart-box cart-box2">
                                <a href="{{ route('site.auth.logout') }}"><i class="fas fa-sign-out-alt"
                                        aria-hidden="true">
                                    </i></a>
                            </div>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end header lower/fixed-header -->

    </header>
    <!-- end main header area -->



    @include('errors.validation-errors')
    @include('errors.custom-messages')
    @yield('content')


    <!-- main footer area -->
    <footer class="footer-area" style="background-image: url({{asset('site/images/home/footer.png')}});">
        <div class="subscribe-section">
            <div class="container">
                <div class="subscribe-content">
                    {{-- <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-12 footer-column">
                            <div class="top-title">
                                <h3>تابع الجديد من اخبارنا</h3>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-12 footer-column">
                            <div class="footr-form rtl">
                                <form action="#" method="Post">
                                    <div class="form-group">
                                        <input type="text" name="name" placeholder="الاسم" required="">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" placeholder="البريد الالكتروني" required="">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn-one">تابعنا</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
        <div class="container">
            <div class="main-footer">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12 footer-column">
                        <div class="logo-wideget footer-wideget">
                            <div class="footer-logo">
                                <a href="index.html"></a>
                            </div>
                            <div class="text">
                                <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز
                                    على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام
                                    طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ</p>
                            </div>
                            <ul class="footer-social text-right">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    {{-- <div class="col-md-2 col-sm-6 col-xs-12 footer-column">
                        <div class="service-wideget footer-wideget">
                            <div class="footer-title">
                                <h3>خدماتنا</h3>
                            </div>
                            <ul class="list">
                                <li><a href="dry-clean.html">غسيل</a></li>
                                <li><a href="dry-clean.html">كي</a></li>
                                <li><a href="dry-clean.html">غسيل و كي </a></li>
                                <li><a href="dry-clean.html">التوصيل للمنازل</a></li>
                                <li><a href="dry-clean.html">خدمات أخرى</a></li>
                            </ul>
                        </div>
                    </div> --}}
                    <div class="col-md-3 col-sm-6 col-xs-12 footer-column">
                        <div class="link-wideget footer-wideget">
                            <div class="footer-title">
                                <h3>روابط سريعة</h3>
                            </div>
                            <ul class="list">
                                <li><a href="{{ route('site.home.about') }}">عن لاندرى</a></li>
                                <li><a href="{{ route('site.home.contact.get') }}">اتصل بنا</a></li>
                                <li><a href="{{ route('site.laundries.index') }}">مغاسل</a></li>
                                <li><a href="{{ route('site.home.terms') }}">الشروط و الأحكام </a></li>
                                <li><a href="{{ route('site.faq.index') }}">الأسئلة الشائعة</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 footer-column">
                        <div class="contact-wideget footer-wideget">
                            <div class="footer-title">
                                <h3>اتصل بنا</h3>
                            </div>
                            <div class="widget-content">
                                <div class="text">
                                    <p>خدمة عملائنا 24 ساعة على مدار الأسبوع</p>
                                </div>
                                <ul class="contact-info">
                                    <li><i class="icon fa fa-map-marker"></i>184 Main Rd E, St Albans VIC <br />3021,
                                        Australia</li>
                                    <li><i class="icon fa fa-phone"></i><a href="tel:+22 657 325 413">+22 657 325
                                            413</a></li>
                                    <li><i class="icon fa fa-envelope"></i><a
                                            href="mailto:info@sharpes.com">info@sharpes.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="footer-bottom centred">
                <div class="copyright"> جميع الحقوق محفوظة لشركة © <a href="http://2grand.net/" class="grand"
                        target="_blank">جراند رواد برمجة تطبيقات الجوال</a> </div>
            </div>
        </div>
    </footer>
    <!-- main footer area -->



    <!--End bodywrapper-->


    <!--Scroll to top-->
    <div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-angle-up"></span>
    </div>










    <!--jquery js -->

    <script src="{{asset('site/js/jquery-2.1.4.js')}}"></script>
    <script src="{{asset('site/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('site/js/owl.js')}}"></script>
    <script src="{{asset('site/js/wow.js')}}"></script>
    <script src="{{asset('site/js/validation.js')}}"></script>
    <script src="{{asset('site/js/jquery-ui.js')}}"></script>
    <script src="{{asset('site/js/jquery.fancybox.pack.js')}}"></script>
    <script src="{{asset('site/js/SmoothScroll.js')}}"></script>
    <script src="{{asset('site/js/jQuery.style.switcher.min.js')}}"></script>
    <script src="{{asset('site/js/html5lightbox/html5lightbox.js')}}"></script>
    <script src="{{asset('site/js/script.js')}}"></script>
    <script src="{{asset('site/js/isotope.js')}}"></script>

    <script>
        $(document).ready(function () {
            let v = $('#validation');
            if (v) {
                setTimeout(() => {
                    v.hide();
                }, 3000);
            }
        });

    </script>

    @yield('scripts')


    <!-- End of .page_wrapper -->
</body>


</html>