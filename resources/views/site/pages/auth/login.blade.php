@extends('site.layouts.master')
@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{asset('site/images/about/page-title2.png')}});">
    <div class="container">
        <div class="content-box">
            <div class="title">
                <h1>Login</h1>
            </div>
            <ul class="bread-crumb rtl">
                <li><a href="{{ route('site.home.index') }}">الرئيسية</a></li>
                <li>تسجيل الدخول</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- contact section -->
<section class="contact-section">

    <div class="contact-form-area">
        <div class="container">
            <div class="title-top centred">تسجيل الدخول</div>
            <div class="sec-title centred">
                <h2>من فضلك قم بتسجيل الدخول الى حسابك </h2>
            </div>
            <form id="contact-form" name="contact_form" class="default-form" action="{{ route('site.auth.login') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                        <input type="number" required name="phone" value="" placeholder="رقم الهاتف">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                        <input type="password" required name="password" value="" placeholder="كلمة المرور">
                    </div>

                </div>



                <div class="contact-btn centred"><button type="submit" class="btn-one"
                        data-loading-text="Please wait..."> تسجيل الدخول</button></div>



                <div class="option-block col-centered">
                    <div class="radio-block">
                        <div class="checkbox">
                            <label>
                                <a href="{{ route('site.auth.forget.get') }}">نسيت كلمة المرور ؟</a>
                            </label>
                        </div>
                    </div>
                </div>
{{-- 
                <div class="option-block col-centered">
                    <div class="radio-block">
                        <div class="checkbox">
                            <label>

                                <span>أو تسجيل الدخول بالفيس بوك </span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                    <a href="#" class="fa fa-facebook fa-facebook2 col-centered"></a>
                </div> --}}
                <div class="option-block col-centered">
                    <div class="radio-block">
                        <div class="checkbox">
                            <label>

                                <span>ليس لديك حساب <a href="{{ route('site.auth.register.get') }}">إنشاء حساب جديد
                                        ؟</a></span>

                            </label>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- contact section end -->
@endsection