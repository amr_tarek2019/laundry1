@extends('site.layouts.master')
@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{asset('site/images/about/page-title2.png')}});">
    <div class="container">
        <div class="content-box">
            <div class="title">
                <h1>Login</h1>
            </div>
            <ul class="bread-crumb rtl">
                <li><a href="{{ route('site.home.index') }}">الرئيسية</a></li>
                <li>تسجيل الدخول</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->
<!-- contact section -->
<section class="contact-section">

    <div class="contact-form-area">
        <div class="container">
            <div class="title-top centred"> تأكيد حسابك </div>
            <div class="sec-title centred">
                <h2>برجاء كتابة كود التأكيد المرسل على الهاتف </h2>
            </div>
            <form id="contact-form" name="contact_form" class="default-form"
                action="{{ route('site.auth.code.post', ['phone'=>$phone,'forget'=>$forget]) }}" method="post">
                @csrf
                <div class="i-am-centered">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-3 code-border">
                            <input class="code-border" type="text" maxlength="1" name="code[]" value="" placeholder=""
                                required="" aria-required="true">
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 code-border">
                            <input class="code-border" type="text" maxlength="1" name="code[]" value="" placeholder=""
                                required="" aria-required="true">
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 code-border">
                            <input class="code-border" type="text" maxlength="1" name="code[]" value="" placeholder=""
                                required="" aria-required="true">
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 code-border">
                            <input class="code-border" type="text" maxlength="1" name="code[]" value="" placeholder=""
                                required="" aria-required="true">
                        </div>
                    </div>
                </div>
                <div class="contact-btn centred"><button type="submit" class="btn-one"
                        data-loading-text="Please wait..."> تأكيد</button></div>
            </form>
            <div class="option-block col-centered">
                <div class="radio-block">
                    <div class="checkbox">
                        <label>
                            <a href="{{ route('site.auth.code.resend',['phone'=>$phone]) }}">إعاده إرسال الكود ؟</a>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- contact section end -->
@endsection