@extends('site.layouts.master')

@section('content')

<!-- page title -->
<section class="page-title centred" style="background-image: url({{ 'site/images/about/page-title.png'}});">
    <div class="container">
        <div class="content-box">
            <div class="title">
                <h1>بيانات التوصيل و الدفع</h1>
            </div>
            <ul class="bread-crumb rtl">
                <li><a href="{{ route('site.home.index') }}">الرئيسية</a></li>
                <li>بيانات التوصيل و الدفع</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->
<!-- checkout section -->
<section class="checkout-section checkout-page rtl">
    <div class="container">
        <form action="{{ route('site.home.checkout.post') }}" method="post" class="billing-form">
            @csrf
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 checkout-column">
                    <div class="billing-info">
                        <div class="title">
                            <h4 class="white">بيانات التوصيل</h4>
                        </div>
                        <div class="row">
                            <div class="field-input col-md-12 col-sm-12 col-xs-12">
                                <div class="swappy-radios" role="radiogroup" aria-labelledby="swappy-radios-label">
                                    <h4 id="swappy-radios-label">برجاء اختيار عنوان التوصيل</h4>
                                    @foreach ($user->addresses as $key=>$item)
                                    <label>
                                        <input name="address" value="{{$item->id}}" type="radio" name="options" {{ $key == 0 ? 'checked' : '' }} />
                                        <span class="radio"></span>
                                        <h4> <span>{{ $item->address }}</span></h4>
                                    </label>
                                    @endforeach
                                </div>
                            </div>
                            {{-- <div class=" col-md-12 col-sm-12 col-xs-12">
                                <button type="button" class="btn btn-primary center" data-toggle="modal"
                                    data-target="#exampleModalCenter">
                                    أضف عنوان أخر
                                </button>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 checkout-column">
                    <div class="billing-info">
                        <div class="title">
                            <h4 class="white">بيانات الدفع</h4>
                        </div>
                        <div class="row">
                            <div class="field-input col-md-12 col-sm-12 col-xs-12">
                                <div class="swappy-radios" role="radiogroup" aria-labelledby="swappy-radios-label">
                                    <h4 id="swappy-radios-label">برجاء اختيار طريقة الدفع</h4>
                                    <label>
                                        <input value="cash" type="radio" name="payment_type" checked />
                                        <span class="radio"></span>
                                        <h4> <span>الدفع عند الإستلام</span></h4>
                                    </label>
                                    <label>
                                        <input value="visa" disabled type="radio" name="payment_type" />
                                        <span class="radio"></span>
                                        <h4> <span>دفع أونلاين</span></h4>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 checkout-column">
                    <div class="billing-info">
                        <div class="title">
                            <h4 class="white">بيانات الشحن</h4>
                        </div>
                        <div class="row">
                            <div class="field-input col-md-6 col-sm-6 col-xs-6">
                                <div class="swappy-radios" role="radiogroup" aria-labelledby="swappy-radios-label">
                                    <h4 id="swappy-radios-label">برجاء اختيار المدينه</h4>
                                        <select class="form-control" name="city" required>
                                            @foreach ($cities as $city)
                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px">
                    <button style="margin-top: 20px" type="submit" class="btn-one btn-one center" data-toggle="modal"
                        data-target="#exampleModalCenter2">
                        استمرار </button>
                </div>  
        </form>
    </div>
    </div>
   
</section>
<!-- checkout section end -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title rtl" id="exampleModalLongTitle"> مغسلة الإخلاص و الأمل</h5>

            </div>
            <div class="modal-body">
                <div id="oneordermap"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">إغلاق</button>

            </div>
        </div>
    </div>
</div>
@stop