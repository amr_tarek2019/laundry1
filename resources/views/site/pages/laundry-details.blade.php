@extends('site.layouts.master')
@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{ asset('site/images/about/page-title3.png') }});">
    <div class="container">
        <div class="content-box">
            <div class="title">
                <h1>مغسلة الإخلاص و الأمل</h1>
            </div>
            <ul class="bread-crumb rtl">
                <li><a href="{{ route('site.home.index') }}">الرئيسية</a></li>
                <li>مغسلة الإخلاص و الأمل</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->

<section class="team-details about-section rtl" style="padding-bottom: 10px">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8 col-xs-12 column">
                <div class="img-box">
                    <figure><img src="{{ $laundry->img }}" alt=""></figure>
                </div>
            </div>
            <div class="col-md-7 col-sm-8 col-xs-12 column">
                <div class="team-details-content">
                    <div class="content-style-one">
                        <div class="title">{{ $laundry->name }}</div>
                        <!-- Button trigger modal -->


                        <div class="text">
                            <p>
                                {{ $laundry->description }}
                            </p>
                        </div>
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#exampleModalCenter">
                            موقعنا على الخريطة
                        </button>
                        <ul class="contact-info">
                            <li><i class="fa fa-phone"><a href="#">{{ $laundry->phone }}</a></i></li>
                            <li><i class="fa fa-envelope"></i><a href="#">{{ $laundry->email }}</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- single shop -->

<section class="service-section" style="padding-top: 50px">
    <div class="container">
        <div class="service-title centred">
            <div class="title-top">منتجاتنا </div>
            <div class="sec-title">
                <h2>تمتع بكل المزايا و الخدمات معنا</h2>
            </div>
        </div>
        <ul class="post-filter list-inline centred rtl">
            <li data-filter=".wash">
                <span>غسيل</span>
            </li>
            <li data-filter=".iron">
                <span>كي</span>
            </li>
            </li>
            <li data-filter=".wash_iron">
                <span> غسيل و كي </span>
            </li>

        </ul>

        <div class="row masonary-layout filter-layout" id="here">
            @if ($laundry->services)
            @foreach ($laundry->services as $item)
            <div
                class="col-md-6 col-sm-6 col-xs-12  @if($item->wash) wash @endif  @if($item->ironing) iron  @endif @if($item->wash_ironing) wash_iron @endif ">
                <article class="single-column single-column2">
                    <div class="item rtl">
                        <figure class="img-box">
                            <img src="{{ $item->img}}" alt="">
                            <figcaption class="default-overlay-outer">
                                <div class="inner">
                                    <div class="content-layer">
                                        <a href="#" data-toggle="modal" data-target="#exampleModalCenter{{ $item->id }}"
                                            class="btn-one">اضف الى قائمة الطلبات</a>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                        <div class="lower-content">
                            <ul class="counter">
                                <li>
                                    <h5><a href="#">{{ $item->name }}</a></h5>
                                </li>
                            </ul>
                            <div class="price">السعر <span>: {{ $item->wash }} ريال سعودى</span></div>
                        </div>
                    </div>
                </article>
            </div>
            @endforeach
            @endif
        </div>
        <a href="#here" id="checkout" class="btn-one btn-one center">
            الإجمالى <span id="totalPrice">0</span> ريال سعودي
        </a>

    </div>
</section>
@foreach ($laundry->services as $item)
<div class="modal fade" id="exampleModalCenter{{ $item->id }}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="margin-top: 15%; !important">
        <div class="modal-content">
            <div class="modal-header modal-header2">
                <h5 class="modal-title modal-title2 rtl" id="exampleModalLongTitle"> {{ $laundry->name }}</h5>
            </div>
            <div class="modal-body" style="direction:rtl">
                <div class="field-input col-md-12 col-sm-12 col-xs-12">
                    <div class="swappy-radios" role="radiogroup" aria-labelledby="swappy-radios-label">
                        <h4 id="swappy-radios-label">برجاء اختيار نوع الخدمة</h4>
                        @if($item->wash)
                        <div class="row col-md-12">
                            <div class="col-md-6">
                                <h4>كي : <span class="colored"> {{ $item->wash }} ريال سعودي </span></h4>
                            </div>
                            <div class="qty mt-5 col-md-6 ">
                                <span Cid="service_{{ $item->id }}" Cprice="{{ $item->wash }}" Ctype="wash"
                                    class="plus bg-dark">+</span>
                                <input type="number" Ctype="wash" id="service_{{ $item->id }}" Cid="{{ $item->id }}"
                                    class="count" name="qty" value="0" disabled="">
                                <span Cid="service_{{ $item->id }}" Cprice="{{ $item->wash }}" Ctype="wash"
                                    class="minus bg-dark">-</span>
                            </div>
                        </div>
                        @endif
                        @if($item->ironing)
                        <div class="row col-md-12">
                            <div class="col-md-6">
                                <h4>غسيل : <span class="colored"> {{ $item->ironing }} ريال سعودي </span></h4>
                            </div>
                            <div class="qty mt-5 col-md-6">
                                <span Cid="service_{{ $item->id }}" Cprice="{{ $item->ironing }}" Ctype="ironing"
                                    class="plus bg-dark">+</span>
                                <input type="number" id="service_{{ $item->id }}" Cid="{{ $item->id }}" Ctype="ironing"
                                    class="count" name="qty" value="0" disabled="">
                                <span Cid="service_{{ $item->id }}" Cprice="{{ $item->ironing }}" Ctype="ironing"
                                    class="minus bg-dark">-</span>
                            </div>
                        </div>
                        @endif
                        @if($item->wash_ironing)
                        <div class="row col-md-12">
                            <div class="col-md-6">
                                <h4> كي وغسيل : <span class="colored"> {{ $item->wash_ironing }} ريال سعودي </span></h4>
                            </div>
                            <div class="qty mt-5 col-md-6 ">
                                <span Cid="service_{{ $item->id }}" Cprice="{{ $item->wash_ironing }}"
                                    Ctype="wash_ironing" class="plus bg-dark">+</span>
                                <input type="number" id="service_{{ $item->id }}" Cid="{{ $item->id }}"
                                    Ctype="wash_ironing" class="count" name="qty" value="0" disabled="">
                                <span Cid="service_{{ $item->id }}" Ctype="wash_ironing"
                                    Cprice="{{ $item->wash_ironing }}" class="minus bg-dark">-</span>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border-top: none !important">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">إغلاق</button>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection

@section('scripts')
<script>
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })

</script>
<script>
    $('#checkout').click(function (e) {
        e.preventDefault();
        let total = parseInt($('#totalPrice').html());
        if (total > 0) {
            let cart = [];
            $.each($('.count'), function (indexInArray, valueOfElement) {
                let type = $(this).attr('Ctype');
                let id = $(this).attr('Cid');
                let quantity = $(this).val();
                if (quantity > 0) {
                    cart.push({service_id:id,service_type:type,service_quantity:quantity});
                }
            });
            $.ajax({
                type: "post",
                url: "{{ route("site.home.checkout.get") }}",
                data: {_token:'{{ csrf_token() }}', cart,total},
                success: function (response) {
                    if(response.status == 1){
                        $(window).unbind('beforeunload');
                        window.location.href = '{{ route('site.home.checkout.confirmation') }}'
                    }else{
                        alert('Error Occured , please Try Again Later');
                    }
                }
            });
        }
    })

</script>
<script>
    var marker = null;

    function initMap() {
        var map = new google.maps.Map(document.getElementById('oneordermap'), {
            zoom: 7,
            center: {
                lat: {{$laundry ->lat}},
                lng: {{$laundry->lng}}
            }
        });
        var MaekerPos = new google.maps.LatLng({{$laundry ->lat}}, {{$laundry->lng}});
        marker = new google.maps.Marker({
            position: MaekerPos,
            map:map
        });
    }

</script>
<script async
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&libraries=places&callback=initMap">
</script>
<script>
    $(window).bind("beforeunload", function (event) {
        if (parseInt($('#totalPrice').html()) > 0) {
            return "سوف يتم حذف سله الشراء عند المغادره";
        }
    });

</script>
<script>
    $(document).ready(function () {
        $('.count').prop('disabled', true);
        $(document).on('click', '.plus', function () {
            let total = $('#totalPrice');
            let cid = $(this).attr('Cid');
            let type = $(this).attr('Ctype');
            let price = $(this).attr('Cprice');
            let counters = $('.count');
            $.each(counters, function (indexInArray, valueOfElement) {
                if ($(this).attr('id') == cid && $(this).attr('Ctype') == type)
                    $(this).val(parseInt($(this).val()) + 1);
            });
            total.html(parseInt(total.html()) + parseInt(price));
        });
        $(document).on('click', '.minus', function () {
            let total = $('#totalPrice');
            let cid = $(this).attr('Cid');
            let type = $(this).attr('Ctype');
            let price = $(this).attr('Cprice');
            let counters = $('.count');
            $.each(counters, function (indexInArray, valueOfElement) {
                if ($(this).attr('id') == cid && $(this).attr('Ctype') == type)
                    if ($(this).val() > 0) {
                        $(this).val(parseInt($(this).val()) - 1);
                    }
            });
            total.html(parseInt(total.html()) - parseInt(price));
            if (total.html() < 0) {
                total.html(0);
            }

        });
    });

</script>


@endsection