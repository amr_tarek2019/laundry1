@extends('site.layouts.master')

@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{ asset('site/images/about/page-title2.png') }});">
    <div class="container">
        <div class="content-box">
            <div class="title">
                <h1>حسابي</h1>
            </div>
            <ul class="bread-crumb rtl">
                <li><a href="{{ route('site.home.index') }}">الرئيسية</a></li>
                <li> حسابي</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- contact section -->
<section class="contact-section">

    <div class="contact-form-area">
        <div class="container">

            <div class="title-top centred">حسابك الشخصي</div>
            <div class="avatar-upload">
                <div class="avatar-preview " style="text-align: center">
                    
                        <img src="{{ $user->img }}" alt="" srcset="" style="max-width: 25%">
                 
                </div>
            </div>
            <form id="contact-form" name="contact_form" class="default-form"
                action="{{ route('site.home.profile.post', $user->phone) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                        <input type="text" required name="name" value="{{ $user->name }}" placeholder=" الأسم">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                        <input type="email" required name="email" value="{{ $user->email }}"
                            placeholder="البريد الإلكتروني">
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                        <input type="password" name="password" value="" placeholder="كلمة المرور">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                        <input type="phone" required name="phone" value="{{ $user->phone }}" placeholder="الهاتف ">
                    </div>

                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                    <div>
                        <input type='file' name="img"  accept=".png, .jpg, .jpeg" />
                        <label for="imageUpload">الصوره الشخصيه</label>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                    <div class="row">
                        <div style="width: 100%"><iframe width="100%" height="300"
                                src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=1%20Grafton%20Street%2C%20Dublin%2C%20Ireland+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed"
                                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a
                                    href="https://www.maps.ie/map-my-route/">Draw map route</a></iframe></div><br />
                    </div>
                </div>
               
                <div class="contact-btn centred"><button type="submit" class="btn-one"
                        data-loading-text="Please wait...">تعديل حسابك</button></div>
            </form>
        </div>
    </div>
</section>
<!-- contact section end -->
@stop