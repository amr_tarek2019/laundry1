@extends('site.layouts.master')

@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{ asset('site/images/about/page-title.png') }});">
    <div class="container">
        <div class="content-box">
            <div class="title">
                <h1> تفاصيل الطلب</h1>
            </div>
            <ul class="bread-crumb rtl">
                <li><a href="{{ route('site.home.index') }}">طللباتي</a></li>
                <li>تفاصيل الطلب </li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- cart sectio -->
<section class="cart-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 table-column">
                <div class="table-outer rtl">
                    <table class="cart-table">
                        <thead class="cart-header">
                            <tr>
                                <th class="table-title">تفاصيل الطلب</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($order->getOrderServices() as $item)
                            <tr>
                                <td colspan="2" class="prod-column">
                                    <div class="column-box">
                                        <div class="prod-thumb">
                                            <a href="#"><img src="{{ $item['img'] }}" alt=""></a>
                                        </div>
                                        <div class="prod-title">
                                            {{ $item['service_name'] }} <span> {{ $item['service_type'] }}</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="qty">
                                    <p>x {{ $item['service_count'] }}</p>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="sub-total">{{ $item['service_total'] }} ريال سعودي</td>


                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <ol class="progtrckr" data-progtrckr-steps="5">
                    <li class="progtrckr-{{ $order->order_status == 1 ? 'done':'todo' }}"><span class="case1">طريقي إلي المغسلة</span></li>
                    <li class="progtrckr-{{ $order->order_status == 2 ? 'done':'todo' }}"><span class="case1">استملت المغسلة</span></li>
                    <li class="progtrckr-{{ $order->order_status == 3 ? 'done':'todo' }}"><span class="case1">استلم المندوب </span></li>
                    <li class="progtrckr-{{ $order->order_status == 4 ? 'done':'todo' }}"><span class="case1">تم التسليم </span></li>


                </ol>



                <div class="cleer-fix"></div>

            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 column">
                <div class="cart-totals rtl">
                    <h4 class="white">المجموع الكلي </h4>
                    <div class="text">رسوم التوصيل<span>{{ $order->delivery }} ريال سعودي</span></div>
                    <div class="text order">المجموع النهائي<span>{{ $order->totalPrice() }} ريال سعودى</span></div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- cart section end -->
@endsection