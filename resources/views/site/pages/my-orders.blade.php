@extends('site.layouts.master')

@section('content')

<!-- page title -->
<section class="page-title centred" style="background-image: url({{ asset('site/images/about/page-title.png') }});">
    <div class="container">
        <div class="content-box">
            <div class="title">
                <h1>طلباتي</h1>
            </div>
            <ul class="bread-crumb rtl">
                <li><a href="{{ route('site.home.index') }}">الرئيسية</a></li>
                <li>طلباتي</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- testimonial -->
<section class="testimonial tesimonial-page">
    <div class="container">
        <div class="testimonial-title centred">
            <div class="title-top">طلباتي</div>
            <div class="sec-title">
                <h2>تعرف على طلباتك و حالة كل طلب</h2>
            </div>
        </div>
        <div class="row">
            @foreach ($user->orders as $order)
            <div class="col-md-6 col-sm-6 col-xs-12 testimonial-column">
                <div class="single-item rtl ">

                    <div class="catagories catagories2">
                        <div class="text2"> اسم المغسلة : </div>
                        <div class="list catagories2">
                            <p href="#"> {{ $order->laundry? $order->laundry->name : "" }} </p>
                        </div>
                    </div>
                    <div class="catagories catagories2">
                        <div class="text2"> رقم الطلب : </div>
                        <div class="list catagories2">
                            <p href="#"> {{ $order->id}} </p>

                        </div>
                    </div>
                    <div class="catagories catagories2">
                        <div class="text2"> حالة الطلب : </div>
                        <div class="list catagories2">
                            <p href="#"> @if ($order->order_status == 0)
                                طلب جديد
                                @elseif ($order->order_status == 1)
                                إلى الطريق للمغسله
                                @elseif ($order->order_status == 2)
                                تم التسليم للمغسله
                                @elseif ($order->order_status == 3)
                                إلى الطريق للعميل
                                @elseif ($order->order_status == 4)
                                مكتمل
                                @elseif ($order->order_status == 5)
                                ملغى
                                @endif </p>

                        </div>
                    </div>
                    <div class="catagories catagories2">
                        <div class="text2"> تكلفة الطلب : </div>
                        <div class="list catagories2">
                            <p href="#"> {{$order->totalPrice()}} ريال سعودي </p>

                        </div>
                    </div>
                    <div class="catagories catagories2">
                        <div class="text2"> تاريخ الطلب : </div>


                        <div class="list catagories2">
                            <p href="#">{{$order->created_at}} </p>

                        </div>

                    </div>

                    <a href="{{ route('site.home.order.details',$order->id) }}" class="btn-one center">


                        تفاصيل الطلب</a>

                </div>
            </div>
            @endforeach
        </div>

    </div>
</section>
<!-- testimonial end -->
@stop