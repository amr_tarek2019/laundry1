@extends('site.layouts.master')

@section('content')

<!-- Main slider -->
    <section class="page-title centred" style="background-image: url({{ asset('site/images/about/page-title.png') }});">
        <div class="container">
            <div class="content-box">
                <div class="title">
                    <h1>الأخبار</h1>
                </div>
                <ul class="bread-crumb rtl">
                    <li><a href="{{ route('site.home.index') }}">الرئيسية</a></li>
                    <li>الأخبار</li>
                </ul>
            </div>
        </div>
    </section>
    <!-- main-slider end -->
    <!-- news section -->
    <section class="news-section">
        <div class="container">
            <div class="news-title centred">
                <div class="title-top">أخبار المغسلة</div>
                <div class="sec-title">
                    <h2>أخر الأخبار عن لاندرى ستيشن</h2>
                </div>
            </div>
            <div class="row news-section2">
                @foreach ($news as $item)
                <div class="col-md-6 col-sm-6 col-xs-12 news-column">
                    <div class="single-item text-right">
                        <div class="img-box"><a href="{{ route('site.news.details',$item->id) }}">{{ $item->title }}">
                                <figure><img src="{{ $item->img }}" alt=""></figure>
                            </a></div>
                        <div class="lower-content">
                            <h3><a href="{{ route('site.news.details',$item->id) }}">{{ $item->title }}</a></h3>
                        </div>
                    </div>
                </div>
                @endforeach
        </div>
    </section>
    <!-- news section end -->
<!-- news section end -->
@endsection