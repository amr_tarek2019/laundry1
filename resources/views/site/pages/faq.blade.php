@extends('site.layouts.master')

@section('content')

<!-- page title -->
<section class="page-title centred" style="background-image: url({{ asset('site/images/about/page-title.png') }});">
    <div class="container">
        <div class="content-box">
            <div class="title">
                <h1>الأسئلة الشائعة</h1>
            </div>
            <ul class="bread-crumb rtl">
                <li><a href="{{ route('site.home.index', ['id'=>1]) }}">الرئيسية</a></li>
                <li>الأسئلة الشائعة</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- faq section -->
<section class="faq-section">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-12 col-xs-12 faq-column">
                <div class="faq-content rtl">
                    <div class="faq-title">الاسئلة الشائعة</div>
                    <div class="accordion-box">
                        <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                            <div class="acc-btn">
                                <div class="acc-title">
                                    <h4>كيف يمكنني تحميل تطبيق لاندرى ستيشن ؟</h4>
                                </div>
                                <div class="toggle-icon">
                                    <i class="plus fa fa-plus"></i><i class="minus fa fa-minus"></i>
                                </div>
                            </div>
                            <div class="acc-content">
                                <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز
                                    على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام
                                    طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ

                                </p>
                            </div>
                        </div>
                        <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                            <div class="acc-btn active">
                                <div class="acc-title">
                                    <h4>كيف يمكنني تحميل تطبيق لاندرى ستيشن ؟</h4>
                                </div>
                                <div class="toggle-icon">
                                    <i class="plus fa fa-plus"></i><i class="minus fa fa-minus"></i>
                                </div>
                            </div>
                            <div class="acc-content collapsed">
                                <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز
                                    على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام
                                    طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ

                                </p>
                            </div>
                        </div>
                        <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                            <div class="acc-btn">
                                <div class="acc-title">
                                    <h4>كيف يمكنني تحميل تطبيق لاندرى ستيشن ؟</h4>
                                </div>
                                <div class="toggle-icon">
                                    <i class="plus fa fa-plus"></i><i class="minus fa fa-minus"></i>
                                </div>
                            </div>
                            <div class="acc-content">
                                <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز
                                    على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام
                                    طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ

                                </p>
                            </div>
                        </div>
                        <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                            <div class="acc-btn">
                                <div class="acc-title">
                                    <h4>كيف يمكنني تحميل تطبيق لاندرى ستيشن ؟</h4>
                                </div>
                                <div class="toggle-icon">
                                    <i class="plus fa fa-plus"></i><i class="minus fa fa-minus"></i>
                                </div>
                            </div>
                            <div class="acc-content">
                                <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز
                                    على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام
                                    طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ

                                </p>
                            </div>
                        </div>
                        <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                            <div class="acc-btn">
                                <div class="acc-title">
                                    <h4>كيف يمكنني تحميل تطبيق لاندرى ستيشن ؟</h4>
                                </div>
                                <div class="toggle-icon">
                                    <i class="plus fa fa-plus"></i><i class="minus fa fa-minus"></i>
                                </div>
                            </div>
                            <div class="acc-content">
                                <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز
                                    على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام
                                    طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ

                                </p>
                            </div>
                        </div>
                        <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                            <div class="acc-btn">
                                <div class="acc-title">
                                    <h4>كيف يمكنني تحميل تطبيق لاندرى ستيشن ؟</h4>
                                </div>
                                <div class="toggle-icon">
                                    <i class="plus fa fa-plus"></i><i class="minus fa fa-minus"></i>
                                </div>
                            </div>
                            <div class="acc-content">
                                <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز
                                    على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام
                                    طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ

                                </p>
                            </div>
                        </div>
                        <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                            <div class="acc-btn">
                                <div class="acc-title">
                                    <h4>كيف يمكنني تحميل تطبيق لاندرى ستيشن ؟</h4>
                                </div>
                                <div class="toggle-icon">
                                    <i class="plus fa fa-plus"></i><i class="minus fa fa-minus"></i>
                                </div>
                            </div>
                            <div class="acc-content">
                                <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز
                                    على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام
                                    طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-1 faq-column">
                <div class="faq-form-area rtl">
                    <h3> أسألنا عن ما تريد ؟</h3>
                    <form action="#" method="post">
                        <div class="form-group">
                            <input type="text" name="name" placeholder="الأسم" placeholder="">
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" placeholder="البريد الإلكتروني" required="">
                        </div>
                        <div class="form-group">
                            <textarea placeholder="الرسالة" name="message" required=""></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn-one">إرسال الرسالة</button>
                        </div>
                    </form>
                </div>
            </div> --}}
        </div>
    </div>
</section>
<!-- faq section end -->
@stop