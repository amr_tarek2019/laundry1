<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

//Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
//
//    // auth routes
//
//    Route::group(['as' => 'auth.', 'prefix' => 'auth'], function () {
//
//        Route::get('', ['uses' => 'AuthController@index', 'as' => 'index']);
//        Route::post('login', ['uses' => 'AuthController@login', 'as' => 'login']);
//        Route::get('logout', ['uses' => 'AuthController@logout', 'as' => 'logout']);
//    });
//
//    Route::group(['middleware' => 'Admin'], function () { // adding the admin middleware to all the routes except the auth
//        Route::prefix('messenger')->group(function () {
//            Route::get('t/{id}', 'MessageController@laravelMessenger')->name('messenger');
//            Route::post('send', 'MessageController@store')->name('message.store');
//            Route::get('threads', 'MessageController@loadThreads')->name('threads');
//            Route::get('more/messages', 'MessageController@moreMessages')->name('more.messages');
//            Route::delete('delete/{id}', 'MessageController@destroy')->name('delete');
//            // AJAX requests.
//            Route::prefix('ajax')->group(function () {
//                Route::post('make-seen', 'MessageController@makeSeen')->name('make-seen');
//            });
//        });
//        // dashboard routes
//
//        Route::group(['as' => 'dashboard.'], function () {
//
//            Route::get('', ['uses' => 'DashboardController@index', 'as' => 'index']);
//            Route::get('read-notification', ['uses' => 'DashboardController@readNotification', 'as' => 'read.notification']);
//            Route::get('new-notification', ['uses' => 'DashboardController@newNotification', 'as' => 'new.notification']);
//        });
//
//        // profile routes
//
//        Route::group(['as' => 'profile.', 'prefix' => 'profile'], function () {
//
//            Route::get('', ['uses' => 'ProfileController@index', 'as' => 'index']);
//            Route::patch('update-information', ['uses' => 'ProfileController@updateInfo', 'as' => 'update.info']);
//            Route::patch('update-password', ['uses' => 'ProfileController@updatePassword', 'as' => 'update.password']);
//        });
//
//        // admins routes
//
//        Route::group(['as' => 'admins.', 'prefix' => 'admins'], function () {
//
//            Route::get('', ['uses' => 'AdminsController@index', 'as' => 'index']);
//            Route::get('admin/{id}/details', ['uses' => 'AdminsController@adminDetails', 'as' => 'admin.details']);
//            Route::post('add', ['uses' => 'AdminsController@add', 'as' => 'add']);
//            Route::delete('delete/{id}', ['uses' => 'AdminsController@delete', 'as' => 'delete']);
//            Route::patch('edit', ['uses' => 'AdminsController@edit', 'as' => 'edit']);
//            Route::patch('toggle-permissions', ['uses' => 'AdminsController@togglePermissions', 'as' => 'toggle.permissions']);
//        });
//
//        // users routes
//
//        Route::group(['as' => 'users.', 'prefix' => 'users'], function () {
//
//            Route::get('', ['uses' => 'UsersController@index', 'as' => 'index']);
//            Route::get('user/{id}/details', ['uses' => 'UsersController@userDetails', 'as' => 'user.details']);
//            Route::patch('toggle-status', ['uses' => 'UsersController@toggleStatus', 'as' => 'toggle.status']);
//            Route::delete('delete/{id}', ['uses' => 'UsersController@deleteUser', 'as' => 'user.delete']);
//            Route::delete('suggestion/{id}', ['uses' => 'UsersController@deleteSuggestion', 'as' => 'suggestion.delete']);
//        });
//
//        // delegates routes
//
//        Route::group(['as' => 'delegates.', 'prefix' => 'delegates'], function () {
//
//            Route::get('', ['uses' => 'DelegatesController@index', 'as' => 'index']);
//            Route::get('user/{id}/details', ['uses' => 'DelegatesController@userDetails', 'as' => 'user.details']);
//            Route::post('add', ['uses' => 'DelegatesController@addDelegate', 'as' => 'add.delegate']);
//            Route::post('edit', ['uses' => 'DelegatesController@editDelegate', 'as' => 'edit.delegate']);
//            Route::patch('toggle-status', ['uses' => 'DelegatesController@toggleStatus', 'as' => 'toggle.status']);
//            Route::delete('delete/{id}', ['uses' => 'DelegatesController@deleteUser', 'as' => 'user.delete']);
//            Route::delete('suggestion/{id}', ['uses' => 'DelegatesController@deleteSuggestion', 'as' => 'suggestion.delete']);
//            Route::post('identity/{id}/download', ['uses' => 'DelegatesController@identityDownload', 'as' => 'identity.download']);
//            Route::post('fees/{id}/reset', ['uses' => 'DelegatesController@feesReset', 'as' => 'fees.reset']);
//        });
//
//
//        // news routes
//
//        Route::group(['as' => 'news.', 'prefix' => 'news'], function () {
//
//            Route::get('', ['uses' => 'NewsController@index', 'as' => 'index']);
//            Route::post('add', ['uses' => 'NewsController@add', 'as' => 'add.news']);
//            Route::post('edit', ['uses' => 'NewsController@edit', 'as' => 'edit.news']);
//            Route::delete('delete/{id}', ['uses' => 'NewsController@delete', 'as' => 'news.delete']);
//        });
//        // testimonials routes
//
//        Route::group(['as' => 'testimonials.', 'prefix' => 'testimonials'], function () {
//
//            Route::get('', ['uses' => 'TestimonialsController@index', 'as' => 'index']);
//            Route::post('add', ['uses' => 'TestimonialsController@add', 'as' => 'add.testimonials']);
//            Route::post('edit', ['uses' => 'TestimonialsController@edit', 'as' => 'edit.testimonials']);
//            Route::delete('delete/{id}', ['uses' => 'TestimonialsController@delete', 'as' => 'testimonials.delete']);
//        });
//
//
//        // orders routes
//
//        Route::group(['as' => 'orders.', 'prefix' => 'orders'], function () {
//
//            Route::get('', ['uses' => 'OrdersController@index', 'as' => 'index']);
//            Route::get('accept/{order}', ['uses' => 'OrdersController@acceptOrder', 'as' => 'accept']);
//            Route::get('cash-back/{order}/details', ['uses' => 'OrdersController@cashBackDetails', 'as' => 'cash.back.details']);
//            Route::patch('cash-back-response', ['uses' => 'OrdersController@cashBackResponse', 'as' => 'cash.back.response']);
//            Route::post('invoice/{order}/download', ['uses' => 'OrdersController@downloadInvoiceDetails', 'as' => 'invoice.download']);
//        });
//
//
//
//
//        //fast orders routes
//
//        Route::group(['as' => 'fast.', 'prefix' => 'fast'], function () {
//
//            Route::get('', ['uses' => 'FastOrdersController@index', 'as' => 'index']);
//            Route::get('accept/fast/{order}', ['uses' => 'FastOrdersController@acceptOrder', 'as' => 'accept']);
//            Route::get('cash-back/fast/{order}/details', ['uses' => 'FastOrdersController@cashBackDetails', 'as' => 'cash.back.details']);
//            Route::patch('cash-back-response', ['uses' => 'FastOrdersController@cashBackResponse', 'as' => 'cash.back.response']);
//            Route::post('invoice/fast/{order}/download', ['uses' => 'FastOrdersController@downloadInvoiceDetails', 'as' => 'invoice.download']);
//        });
//
//
//
//
//        // suggestions routes
//
//        Route::group(['as' => 'suggestions.', 'prefix' => 'suggestions'], function () {
//
//            Route::get('', ['uses' => 'SuggestionsController@index', 'as' => 'index']);
//            Route::delete('suggestion/{id}', ['uses' => 'SuggestionsController@deleteSuggestion', 'as' => 'suggestion.delete']);
//        });
//
//        // notifications routes
//
//        Route::group(['as' => 'notifications.', 'prefix' => 'notifications'], function () {
//
//            Route::get('', ['uses' => 'NotificationsController@index', 'as' => 'index']);
//            Route::post('send-notification', ['uses' => 'NotificationsController@sendNotification', 'as' => 'send']);
//        });
//
//
//        // settings routes
//
//        Route::group(['as' => 'settings.', 'prefix' => 'settings'], function () {
//
//            Route::get('', ['uses' => 'SettingsController@index', 'as' => 'index']);
//            Route::patch('update', ['uses' => 'SettingsController@update', 'as' => 'update']);
//        });
//
//
//        // laundries routes
//
//        Route::group(['as' => 'laundries.', 'prefix' => 'laundries'], function () {
//
//            Route::get('', ['uses' => 'LaundriesController@index', 'as' => 'index']);
//            Route::get('user/{id}/details', ['uses' => 'LaundriesController@userDetails', 'as' => 'user.details']);
//            Route::post('add-owner', ['uses' => 'LaundriesController@add', 'as' => 'add']);
//            Route::post('edit-owner', ['uses' => 'LaundriesController@edit', 'as' => 'edit']);
//            Route::patch('toggle-status', ['uses' => 'LaundriesController@toggleStatus', 'as' => 'toggle.status']);
//            Route::patch('laundry-toggle-status', ['uses' => 'LaundriesController@laundryToggleStatus', 'as' => 'laundry.toggle.status']);
//            Route::delete('delete-owner/{id}', ['uses' => 'LaundriesController@delete', 'as' => 'user.delete']);
//            Route::post('edit-laundry/{id}', ['uses' => 'LaundriesController@editLaundry', 'as' => 'edit.laundry']);
//            Route::post('fees/{id}/reset', ['uses' => 'LaundriesController@feesReset', 'as' => 'fees.reset']);
//            Route::post('add-service', ['uses' => 'LaundriesController@addService', 'as' => 'add.service']);
//            Route::post('edit-service', ['uses' => 'LaundriesController@editService', 'as' => 'edit.service']);
//            Route::delete('delete-service/{id}', ['uses' => 'LaundriesController@deleteService', 'as' => 'service.delete']);
//        });
//
//        // notifications routes
//
//        Route::group(['as' => 'cities.', 'prefix' => 'cities'], function () {
//
//            Route::get('', ['uses' => 'CitiesController@index', 'as' => 'index']);
//            Route::post('add', ['uses' => 'CitiesController@add', 'as' => 'add']);
//            Route::post('edit/{id}', ['uses' => 'CitiesController@edit', 'as' => 'edit']);
//            Route::delete('delete/{id}', ['uses' => 'CitiesController@delete', 'as' => 'delete']);
//        });
//    });
//});
//
//
///**
// * Laundry Owners Routes
// **/
//
//Route::group(['namespace' => 'Laundry', 'prefix' => 'laundry', 'as' => 'laundry.'], function () {
//
//    // auth routes
//
//    Route::group(['as' => 'auth.', 'prefix' => 'auth'], function () {
//
//        Route::get('', ['uses' => 'AuthController@index', 'as' => 'index']);
//        Route::post('login', ['uses' => 'AuthController@login', 'as' => 'login']);
//        Route::get('logout', ['uses' => 'AuthController@logout', 'as' => 'logout']);
//    });
//
//    Route::group(['middleware' => 'Laundry'], function () { // adding the admin middleware to all the routes except the auth
//
//        Route::prefix('messenger')->group(function () {
//            Route::get('t/{id}', 'MessageController@laravelMessenger')->name('messenger');
//            Route::post('send', 'MessageController@store')->name('message.store');
//            Route::get('threads', 'MessageController@loadThreads')->name('threads');
//            Route::get('more/messages', 'MessageController@moreMessages')->name('more.messages');
//            Route::delete('delete/{id}', 'MessageController@destroy')->name('delete');
//            // AJAX requests.
//            Route::prefix('ajax')->group(function () {
//                Route::post('make-seen', 'MessageController@makeSeen')->name('make-seen');
//            });
//        });
//        // dashboard routes
//
//        Route::group(['as' => 'dashboard.'], function () {
//
//            Route::get('', ['uses' => 'DashboardController@index', 'as' => 'index']);
//            Route::get('read-notification', ['uses' => 'DashboardController@readNotification', 'as' => 'read.notification']);
//            Route::get('new-notification', ['uses' => 'DashboardController@newNotification', 'as' => 'new.notification']);
//        });
//
//        // profile routes
//
//        Route::group(['as' => 'profile.', 'prefix' => 'profile'], function () {
//
//            Route::get('', ['uses' => 'ProfileController@index', 'as' => 'index']);
//            Route::patch('update-information', ['uses' => 'ProfileController@updateInfo', 'as' => 'update.info']);
//            Route::patch('update-password', ['uses' => 'ProfileController@updatePassword', 'as' => 'update.password']);
//        });
//
//
//        // laundries routes
//
//        Route::group(['as' => 'laundries.', 'prefix' => 'laundries'], function () {
//
//            Route::get('', ['uses' => 'LaundriesController@index', 'as' => 'index']);
//            Route::get('user/{id}/details', ['uses' => 'LaundriesController@userDetails', 'as' => 'user.details']);
//            Route::post('add-owner', ['uses' => 'LaundriesController@add', 'as' => 'add']);
//            Route::post('edit-owner', ['uses' => 'LaundriesController@edit', 'as' => 'edit']);
//            Route::patch('toggle-status', ['uses' => 'LaundriesController@toggleStatus', 'as' => 'toggle.status']);
//            Route::delete('delete-owner/{id}', ['uses' => 'LaundriesController@delete', 'as' => 'user.delete']);
//            Route::post('edit-laundry/{id}', ['uses' => 'LaundriesController@editLaundry', 'as' => 'edit.laundry']);
//            Route::post('fees/{id}/reset', ['uses' => 'LaundriesController@feesReset', 'as' => 'fees.reset']);
//            Route::post('invoice/{order}/download', ['uses' => 'LaundriesController@downloadInvoiceDetails', 'as' => 'invoice.download']);
//            Route::post('add-service', ['uses' => 'LaundriesController@addService', 'as' => 'add.service']);
//            Route::post('edit-service', ['uses' => 'LaundriesController@editService', 'as' => 'edit.service']);
//            Route::delete('delete-service/{id}', ['uses' => 'LaundriesController@deleteService', 'as' => 'service.delete']);
//        });
//    });
//});






Route::group(['prefix'=>'login','namespace'=>'Backend\Authentication'], function (){
    Route::group(['prefix'=>'admin'],function () {
        Route::get('', 'AuthenticationController@index')->name('login');
        Route::post('auth', 'AuthenticationController@login')->name('user.login');
    });

});

Route::group(['prefix'=>'admin',['middleware' => 'Admin'],'namespace'=>'Backend\Dashboard'], function () {

    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'DashboardController@index')->name('dashboard');
        Route::post('', 'DashboardController@logout')->name('logout');
    });


        Route::group(['prefix' => 'users'], function () {

            Route::get('','UsersController@index')->name('users');
            Route::get('user/{id}/details','UsersController@userDetails')->name('users.details');
            Route::post('toggle-status','UsersController@toggleStatus')->name('users.status');
            Route::delete('delete/{id}','UsersController@deleteUser')->name('users.delete');
        });


});

