<?php
Route::group(['namespace' => 'Site', 'as' => 'site.'], function () {

    // home routes
    Route::group(['as' => 'home.'], function () {
        Route::get('', ['uses' => 'HomeController@index', 'as' => 'index']);
        Route::get('about', ['uses' => 'HomeController@about', 'as' => 'about']);
        Route::get('terms', ['uses' => 'HomeController@getTerms', 'as' => 'terms']);
        Route::group(['middleware' => 'is_authenticated'], function () {
            Route::get('contact', ['uses' => 'HomeController@getContact', 'as' => 'contact.get']);
            Route::post('contact', ['uses' => 'HomeController@postContact', 'as' => 'contact.post']);
            Route::get('profile', ['uses' => 'HomeController@getProfile', 'as' => 'profile.get']);
            Route::put('profile/{user}', ['uses' => 'HomeController@postProfile', 'as' => 'profile.post']);
            Route::post('get-check-out', ['uses' => 'HomeController@getCheckout', 'as' => 'checkout.get']);
            Route::get('get-check-confirmation', ['uses' => 'HomeController@getCheckoutConfirmation', 'as' => 'checkout.confirmation']);
            Route::post('post-check-confirmation', ['uses' => 'HomeController@postCheckout', 'as' => 'checkout.post']);
            Route::get('my-orders', ['uses' => 'HomeController@myOrders', 'as' => 'orders.get']);
            Route::get('order/{order}/details', ['uses' => 'HomeController@orderDetails', 'as' => 'order.details']);
        });
    });

    // laundry routes
    Route::group(['as' => 'laundries.','prefix'=> 'laundries'], function () {
        Route::get('', ['uses' => 'LaundryController@index', 'as' => 'index']);
        Route::group(['middleware' => 'is_authenticated'], function () {
        Route::get('{laundry}/details', ['uses' => 'LaundryController@details', 'as' => 'details']);
        });
    });

    // news routes
    Route::group(['as' => 'news.', 'prefix' => 'news'], function () {
        Route::get('', ['uses' => 'HomeController@getNews', 'as' => 'index']);
        Route::get('{news}/details', ['uses' => 'HomeController@getNewsDetails', 'as' => 'details']);
    });

   // news routes
    Route::group(['as' => 'faq.', 'prefix' => 'faq'], function () {
        Route::get('', ['uses' => 'HomeController@getFaq', 'as' => 'index']);
    });

    // auth routes
    Route::group(['as' => 'auth.', 'prefix' => 'auth', 'middleware' => 'site_log_check'], function () {
        Route::get('', ['uses' => 'AuthController@index', 'as' => 'index']);
        Route::post('', ['uses' => 'AuthController@login', 'as' => 'login']);
        Route::get('register', ['uses' => 'AuthController@getRegister', 'as' => 'register.get']);
        Route::post('register', ['uses' => 'AuthController@postRegister', 'as' => 'register.post']);
        Route::get('code', ['uses' => 'AuthController@getCode', 'as' => 'code.get']);
        Route::post('code', ['uses' => 'AuthController@postCode', 'as' => 'code.post']);
        Route::get('code-resend', ['uses' => 'AuthController@resendCode', 'as' => 'code.resend']);
        Route::get('forget', ['uses' => 'AuthController@getForget', 'as' => 'forget.get']);
        Route::post('forget', ['uses' => 'AuthController@postForget', 'as' => 'forget.post']);
        Route::get('password', ['uses' => 'AuthController@getPassword', 'as' => 'password.get']);
        Route::post('password', ['uses' => 'AuthController@postPassword', 'as' => 'password.post']);
        Route::get('logout', ['uses' => 'AuthController@logout', 'as' => 'logout']);
    });
});
