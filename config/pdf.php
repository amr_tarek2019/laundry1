<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => 'laundry',
	'subject'               => 'laundry',
	'keywords'              => '',
	'creator'               => 'laundry',
	'display_mode'          => 'fullpage',
	'tempDir'               => base_path('../temp/')
];
