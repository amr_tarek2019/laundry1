function showDeletConfirmationModal(url, id) {
    $('#deleteconfirmationmodal').modal('show');
    $('#deleteconfirmationbutton').on('click', function () {
        $('#deleteconfirmationmodal').modal('hide');
        let data = {
            "_token": $('meta[name=csrf-token]').attr('content'),
            url
        };
        console.log(url, data);
        $.ajax({
            url,
            method: "DELETE",
            data,
            success: function () {
                $('#' + id).hide();
                displaycustmodal('success', " تم المسح", " تم مسح العنصر بنجاح");
            }
        });
    })
}
$(document).ready(function () {
    'use strict';
    if ($('.select2') != null) {
        $('.select2').select2({
            minimumResultsForSearch: Infinity
        });
    }
    $('#datatable1').DataTable({
        bLengthChange: false,
        responsive: true,
        "scrollX": true,
        language: {
            searchPlaceholder: ' ابحث هنا ...',
            sSearch: '',
        },
    });
});
function displaycustmodal(flag, title, message) {
    if (flag == "success") {
        $('#modaltitle').addClass('tx-success');
        $('#modalbtn').addClass('tx-success');
    } else {
        $('#modaltitle').addClass('tx-danger');
        $('#modalbtn').addClass('tx-danger');
    }
    $('#modaltitle').html(title);
    $('#modalmessage').html(message);
    $('#custommodal').modal('show');

}



$('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
    $($.fn.dataTable.tables(true)).DataTable()
        .columns.adjust();
});

$('input[type="file"]').each(function(){

    var $file = $(this),
        $label = $file.next('label'),
        $labelText = $label.find('span'),
        labelDefault = $labelText.text();

    $file.on('change', function(event){
        var fileName = $file.val().split( '\\' ).pop(),
            tmppath = URL.createObjectURL(event.target.files[0]);
        if( fileName ){
            $label
                .addClass('file-ok')
                .css('background-image', 'url(' + tmppath + ')');
            $labelText.text(fileName);
        }else{
            $label.removeClass('file-ok');
            $labelText.text(labelDefault);
        }
    });

});




