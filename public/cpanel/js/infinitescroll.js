    $('ul.pagination').hide();
$('.infinite-scroll').infiniteScroll({
    // options
    path: '.pagination li.active + li a',
    append: '.notification',
    history: false,
    checkLastPage: true,
    button: '.view-more',
// load pages on button click
    scrollThreshold: false,
// disable loading on scroll

});
$('.dropdown-menu').click(function (e) {
    e.stopPropagation();
});
