
var map;
function initMap($lat,$lng) {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat:  parseFloat($lat), lng: parseFloat($lng)},
        zoom: 8,


    });
    var marker = new google.maps.Marker({
        position: {lat: parseFloat($lat), lng: parseFloat($lng)},
        map: map,
    });
}